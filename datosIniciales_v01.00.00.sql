-- -----------------------------------------------------
-- Schema CAMBIAR SEGUN EL ENTORNO
-- -----------------------------------------------------
USE `ARADB` ;

/*CREATE TAGS*/
/*OJO SI CAMBIAMOS O METEMOS TENEMOS QUE CAMBIAR TAMBIEN EL CLASIFICADOR*/
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (1, 'Politica', 'Todas las noticias sobre politica', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (2, 'Arte y Cultura', 'Todas las noticias sobre arte y cultura', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (3, 'Ciencia', 'Todas las noticias sobre ciencia', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (4, 'Deportes', 'Todas las noticias sobre deportes', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (5, 'Economía', 'Todas las noticias sobre ecopomía', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (6, 'Educación', 'Todas las noticias sobre educación', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (7, 'Salud', 'Todas las noticias sobre salud', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (8, 'Sociedad', 'Todas las noticias sobre sociedad', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (9, 'Tecnologia', 'Todas las noticias sobre tecnología', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (10, 'Curiosidades', 'Todas las noticias sobre curiosidades', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (11, 'Galicia', 'Todas las noticias sobre galicia', '1');
INSERT INTO `ARADB`.`BDL_TAG` (`idTag`, `value`, `description`, `isVisible`) VALUES (12, 'Otras', 'Otras noticias sin clasificar', '1');
    
/*NUEVO HOST*/ 
 /*LVG PORTADA*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`) VALUES ('1','http://www.lavozdegalicia.es/', 'LVG Portada');
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '1',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '1',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/pontevedra/cuntis/index.htm', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/lavozdelaescuela/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/album/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/video/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/.*directo.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/pontevedra/cuntis/[0-9]*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/tiempo/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/firmas/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/encuesta/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/noticia/opinion/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/([^/]*/)+20(0[0-9]|1[0-4])/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/temas/.*', '', '1',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/barbanza/[^.]*$', '', '1',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '1',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '1',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '1',2);


/*NUEVO HOST*/ 
 /*LVG BARBANZA*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (2,'http://www.lavozdegalicia.es/barbanza/', 'LVG Barbanza', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '2',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '2',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/lavozdelaescuela/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/album/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/video/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/.*directo.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/tiempo/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/firmas/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/encuesta/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/noticia/opinion/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/([^/]*/)+20(0[0-9]|1[0-4])/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/temas/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/barbanza/[^.]*$', '', '2',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '2',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '2',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '2',2);


/*NUEVO HOST*/ 
 /*LVG SANTIAGO*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (3,'http://www.lavozdegalicia.es/santiago/', 'LVG Santiago', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '3',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '3',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/lavozdelaescuela/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/album/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/video/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/.*directo.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/tiempo/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/firmas/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/encuesta/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/noticia/opinion/.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/([^/]*/)+20(0[0-9]|1[0-4])/.*', '', '3',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/temas/.*', '', '3',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '3',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '3',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '3',2);


/*NUEVO HOST*/ 
 /*LVG CUNTIS*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (4,'http://www.lavozdegalicia.es/temas/cuntis/', 'LVG Cuntis', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '4',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '4',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/pontevedra/cuntis/index.htm', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/lavozdelaescuela/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/album/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/video/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/.*directo.*', '', '2',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/pontevedra/cuntis/[0-9]*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/tiempo/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/firmas/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/encuesta/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/noticia/opinion/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/([^/]*/)+20(0[0-9]|1[0-4])/.*', '', '4',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.lavozdegalicia.es/temas/.*', '', '4',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '4',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '4',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '4',2);



/*NUEVO HOST*/ 
 /*ECG SANTIAGO*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (5,'http://www.elcorreogallego.es/santiago/', 'ECG Santiago', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '5',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '5',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.elcorreogallego.es/rss/.*', '', '5',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.elcorreogallego.es/santiago/potencialidad-de-santiago(|/)', '', '5',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '5',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '5',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '5',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/elCorreoGallego.gif.*', '', '5',2);



/*NUEVO HOST*/ 
 /*DA Ulla-Umia*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (6,'http://www.diariodearousa.com/seccion/ulla-umia/', 'DA Ulla-Umia', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '6',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '6',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.diariodearousa.com/album/.*', '', '6',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/more-share-button.png', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/cc.png', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/facebook.*', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/twitter.*', '', '6',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/feed.*', '', '6',2);



/*NUEVO HOST*/ 
 /*DP Caldas-Deza*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (7,'http://diariodepontevedra.galiciae.com/comarcas-pontevedra/caldasdeza', 'DP Caldas-Deza', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '7',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '7',0);

 /*RULE TYPE 1*/

 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://diariodepontevedra.galiciae.com/encuesta/.*', '', '7',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://diariodepontevedra.galiciae.com/tarifas-publicitarias', '', '7',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://diariodepontevedra.galiciae.com/opinion/.*', '', '7',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '7',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '7',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '7',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/bot_.*', '', '7',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/diariodepontevedra.png', '', '7',2);



/*NUEVO HOST*/ 
 /*Pontevedraviva Portada*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (8,'http://pontevedraviva.com/', 'Pontevedraviva Portada', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '8',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '8',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/opinion/.*', '', '8',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/axenda/.*', '', '8',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/texto/contacto/', '', '8',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/feedback/', '', '8',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/tempo/.*', '', '8',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '8',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '8',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '8',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/banners/.*', '', '8',2);



/*NUEVO HOST*/ 
 /*Pontevedraviva Umia*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (9,'http://pontevedraviva.com/lugar/umia/', 'Pontevedraviva Umia', 11);
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '9',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '9',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/opinion/.*', '', '9',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/axenda/.*', '', '9',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/texto/contacto/', '', '9',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/feedback/', '', '9',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://pontevedraviva.com/tempo/.*', '', '9',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '9',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '9',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '9',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/banners/.*', '', '9',2);



/*NUEVO HOST*/ 
 /*20minutos España*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`) VALUES (10,'http://www.20minutos.es/', '20minutos España');
 /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '10',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '10',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/especiales/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/galeria/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/.*directo.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/especial/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/graficos/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/encuesta/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/fotos/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/blogs_opinion/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/consultorios/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/minuteca/.*', '', '10',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/cine/cartelera/.*', '', '10',1);
    INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.20minutos.es/videos/.*', '', '10',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '10',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '10',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '10',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/chart_cerrado.gif.*', '', '10',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/install_flash.gif.*', '', '10',2);


/*NUEVO HOST*/ 
 /*Marca* /
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (11,'http://www.marca.com/', 'Marca', 4);
   /*RULE TYPE 0* /
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '11',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '11',0);

 /*RULE TYPE 1* /
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/.*opinion/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/tag/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/multimedia/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/.*directo.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/radio(/|).*', '', '11',1);
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/[^/]* /', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/participa/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/encuentros/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/eventos/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/futbol/equipos/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/.*clasificacion.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/debate/2015/06/152378/prevotaciones152378.html', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/futbol/calendario/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/videosmarca/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/albumes/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/.*directo', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/widgets/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/promociones/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/corporativo/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/social/.*', '', '11',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/en/.*', '', '11',1);
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.marca.com/.* /multimedia/.*', '', '11',1);

 /*RULE TYPE 2* /
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.* /ico.*', '', '11',2);
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.* /(l|L)ogo.*', '', '11',2);
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.* /btn.*', '', '11',2);
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.* /.*\\.gif', '', '11',2);
 --Borrar el espacio--INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.* /compartirfb.*', '', '11',2);
 */



/*NUEVO HOST*/ 
 /*FARO DE VIGO*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`) VALUES (12,'http://www.farodevigo.es/', 'Faro de Vigo');
   /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '12',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '12',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/suscriptor/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/deportes/pontevedra/', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/galicia/', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/humor/', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/servicios/club/acerca.jsp', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/servicios/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/opinion/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/vida-y-estilo/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/especiales/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/empresas-en-vigo/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/servicios/.*', '', '12',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.farodevigo.es/.*[^.html]$', '', '12',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '12',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '12',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '12',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/.*(_|-)suscriptor(_|-).*', '', '12',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/concursos/.*', '', '12',2);




/*NUEVO HOST*/ 
 /*XATAKA*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`, `default_tag`) VALUES (13,'http://www.xataka.com/', 'Xataka', 9);
   /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '13',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '13',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.xataka.com/tag/.*', '', '13',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.xataka.com/lomejor', '', '13',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.xataka.com/autor/.*', '', '13',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.xataka.com/respuestas.*', '', '13',1);

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '13',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '13',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '13',2);




/*NUEVO HOST*/ 
 /*EL PAIS*/
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`) VALUES (14,'http://elpais.com/', 'El Pais');
   /*RULE TYPE 0*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '14',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '14',0);

 /*RULE TYPE 1*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/elpais/planeta_futuro.html', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/.*/opinion.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/publi-especial/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/especiales/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/pagina_inicio.html', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/promociones/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/.*/album/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/.*/eps/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/tematicos/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/tag/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/especiales/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/corporativos/.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/suscripciones.*', '', '14',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/.*/media/.*', '', '14',1);
    INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/autor/.*', '', '14',1);
    INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://elpais.com/.*/vinetas/', '', '14',1);
    
    

 /*RULE TYPE 2*/
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/ico.*', '', '14',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/(l|L)ogo.*', '', '14',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*/btn.*', '', '14',2);




/*NO FUNCIONA EL AS NO SÉ CUAL ES LA CAUSA */

/*NUEVO HOST* /  
 /*AS* /
 INSERT INTO `ARADB`.`BDL_HOST` (`id`, `url`, `title`) VALUES (15,'http://www.as.com/', 'As');
   /*RULE TYPE 0* /
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '[^\\|]*\\|[^\\|]*\\|.*', '', '15',0);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*Copyrigth.*', '', '15',0);

 /*RULE TYPE 1* /
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/static/.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/promocion/.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/edigitales/.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/.*directo.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/motormercado/.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/ocio/.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/opinion/.*', '', '15',1);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, 'http://www.as.com/masdeporte/.*album.*', '', '15',1);

 /*RULE TYPE 2* /
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*\\/ico.*', '', '15',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*\\/(l|L)ogo.*', '', '15',2);
 INSERT INTO `ARADB`.`BDL_RULE` (`id`, `value`, `description`, `id_Host`, `type`) VALUES (NULL, '.*\\/btn.*', '', '15',2);
*/