/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.ejecutadores;

import com.boudly.jobs.noticiascomplejo.ObtainContentJob;
import com.boudly.jobs.noticiascomplejo.ObtainUrlsJob;
import com.boudly.jobs.logic.Url;
import com.boudly.orm.config.DB;
import com.tekstly.exceptions.LoadFileException;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.New;
import com.boudly.jobs.noticiascomplejo.AnalizerJob;
import com.tekstly.exceptions.LoadWebException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MainComplejas {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            DB.init();
            List<Host> noticias = Host.getAll();
            loggerLoad(noticias);
            List<Url> result = obtainUrls(noticias);
            saveInDB(result);
            BoudlyLog.getInstance().trace("Carga: Finalizada");

        } catch (Exception ex) {
            BoudlyLog.getInstance().error(ex.getMessage());
        }finally{
            DB.close();
        }
    }

    private static void loggerLoad(List<Host> noticias) {
        BoudlyLog.getInstance().trace("Vamos a cargar noticias de las siguientes fuentes de datos:");
        for (Host n : noticias) {
            BoudlyLog.getInstance().trace(n.getUrl());
        }
        BoudlyLog.getInstance().trace("##############################################");
    }

    private static List<Url> obtainUrls(List<Host> noticias) {
        BoudlyLog.getInstance().trace("Iniciando proceso de carga");
        ObtainUrlsJob urlsJob = new ObtainUrlsJob();
        try {
            urlsJob.execute((List<Object>) (Object) noticias);
        } catch (LoadWebException ex) {
            BoudlyLog.getInstance().error(ex.getMessage());
        }
        return (List<Url>) urlsJob.getResult();
    }

    private static void saveInDB(List<Url> result) throws LoadFileException {
        long timeIni = Calendar.getInstance().getTime().getTime();
        AnalizerJob analisisJob = new AnalizerJob();
        BoudlyLog.getInstance().trace("Carga modelo clasificador: " + (Calendar.getInstance().getTime().getTime() - timeIni));

        Integer total = 0;
        for (Url r : result) {
            List<New> newsToAdd = new ArrayList<>();
            if (New.search(r.getElement()) == null) {
                ObtainContentJob content = new ObtainContentJob();
                try {
                    content.execute(Arrays.asList(((Object) r)));
                    List<Object> resumenes = (List<Object>) content.getResult();
                    analisisJob.execute(resumenes);
                } catch (Exception ex) {
                    BoudlyLog.getInstance().error("Error in url: " + r.getElement() + " - " + ex.getMessage());
                }

                List<New> noticias = (List<New>) analisisJob.getResult();
                for (New noticia : noticias) {
                    newsToAdd.add(noticia);
                }
            } else {
                BoudlyLog.getInstance().trace("Ya guardada: " + r.getElement());
            }

            total += saveAllElementsNow(newsToAdd);
        }

        BoudlyLog.getInstance().trace("##############################################");
        BoudlyLog.getInstance().trace("Cargadas: " + total + " noticias nuevas");
        BoudlyLog.getInstance().trace("##############################################");

    }

    private static Integer saveAllElementsNow(List<New> news) {
        Integer elements = 0;
        for (New n : news) {

            try {
                if (n.saveNow()) {
                    BoudlyLog.getInstance().trace("Guardando: " + n.getUrl());
                    elements++;
                }

            } catch (org.javalite.activejdbc.DBException ex) {
                BoudlyLog.getInstance().error("Error al insertar: " + ex.getMessage());
            }
        }
        return elements;
    }

}
