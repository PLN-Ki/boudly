/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.ejecutadores;

import com.boudly.jobs.modeloclasificador.ObtainUrlsJobModelo;
import com.boudly.jobs.logic.Url;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtractorModelo {
    public static final String ARTE_CULTURA = "http://gnspain.com/category/cultura/page/";
    public static final String TAG_NAME_ARTE_CULTURA = "ARTE_Y_CULTURA";
    public static final String CIENCIA = "http://gnspain.com/category/ciencia/page/";
    public static final String TAG_NAME_CIENCIA = "CIENCIA";
    public static final String TECNOLOGIA = "http://gnspain.com/category/tecnologia/page/";
    public static final String TAG_NAME_TECNOLOGIA = "TECNOLOGIA";
    public static final String POLITICA = "http://gnspain.com/category/politica/page/";
    public static final String TAG_NAME_POLITICA = "POLITICA";
    public static final String DEPORTES = "http://gnspain.com/category/deportes/page/";
    public static final String TAG_NAME_DEPORTES = "DEPORTES";
    public static final String ECONOMIA = "http://gnspain.com/category/economia/page/";
    public static final String TAG_NAME_ECONOMIA = "ECONOMIA";
    public static final String SALUD = "http://gnspain.com/category/salud/page/";
    public static final String TAG_NAME_SALUD = "SALUD";
    public static final String EDUCACION = "http://gnspain.com/category/educacion/page/";
    public static final String TAG_NAME_EDUCACION = "EDUCACION";
    public static final String SOCIAL = "http://gnspain.com/category/social/page/";
    public static final String TAG_NAME_SOCIAL = "SOCIAL";
    public static final Integer MAX_ELEMENTS = 50;

    public static void main(String[] args) {
        System.out.println("Empezamos");
        List<Url> urls = obtainUrlsProcess(ARTE_CULTURA);
        filter(TAG_NAME_ARTE_CULTURA, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(CIENCIA);
        filter(TAG_NAME_CIENCIA, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(TECNOLOGIA);
        filter(TAG_NAME_TECNOLOGIA, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(POLITICA);
        filter(TAG_NAME_POLITICA, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(DEPORTES);
        filter(TAG_NAME_DEPORTES, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(ECONOMIA);
        filter(TAG_NAME_ECONOMIA, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(SALUD);
        filter(TAG_NAME_SALUD, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(EDUCACION);
        filter(TAG_NAME_EDUCACION, urls);
        System.out.println("##########");
        urls = obtainUrlsProcess(SOCIAL);
        filter(TAG_NAME_SOCIAL, urls);
        System.out.println("##########");

    }

    public static List<Url> obtainUrlsProcess(String baseUrl) {
        List<Url> totalUrls = new ArrayList<>();
        boolean llegoUltimaNoticia = false;
        Integer page = 1;
        while (!llegoUltimaNoticia) {
            try {
                totalUrls.addAll(getUrls(baseUrl + page + "/"));
                page++;
                //System.out.println("Obtenida la página "+(page-1));
            } catch (Exception ex) {//Cuando salta la exception es que no tienen nada ya las web que está procesando
                llegoUltimaNoticia = true;
                System.out.println("======> FIN, hasta la página "+(page-1));
            }
        }
        return totalUrls;
    }

    public static List<Url> filter(String tagName, List<Url> urls) {
        System.out.println("Noticias de "+tagName);
        for (Url u : urls) {
            System.out.println(u.getElement());
        }
        return urls;
    }

    public static List<Url> getUrls(String url) throws Exception {
        ObtainUrlsJobModelo urlsJob = new ObtainUrlsJobModelo();
        urlsJob.execute(new ArrayList<Object>(Arrays.asList(url)));
        return (List<Url>) urlsJob.getResult();

    }
}
