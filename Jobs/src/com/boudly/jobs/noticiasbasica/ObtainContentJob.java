/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.noticiasbasica;

import com.boudly.jobs.IJob;
import com.boudly.jobs.logic.Url;
import com.boudly.jobs.properties.LoaderConfiguration;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.New;
import com.tekstly.extractor.WebExtractor;
import com.tekstly.linguist.Language;
import com.tekstly.linguist.Summarizer;
import com.tekstly.logic.extractor.Block;
import com.tekstly.logic.summarizer.Sentence;
import com.tekstly.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;


public class ObtainContentJob implements IJob {

    public static final Float SIZE_SUMMER_DEFAULT = Float.parseFloat("0.3");
    private static final Integer MIN_BLOCKS = 0;
    private static final Integer PRINCIAL_BLOCK = 0;
    private static final Integer MIN_SIZE_TEXT = 170;//caracteres
    private final List<New> news;

    public ObtainContentJob() {
        this.news = new ArrayList<>();
    }

    /**
     * @return devuelve una lista de noticias
     */
    @Override
    public Object getResult() {
        return this.news;
    }

    /**
     *
     * @param newsToObtain lista con las urls de donde obtener la información
     * @throws java.lang.Exception
     */
    @Override
    public void execute(List<Object> newsToObtain) throws Exception{
        for (Object url : newsToObtain) {
            stractProcess((Url)url);
        }
    }

    private void stractProcess(Url url) throws Exception{
            WebExtractor extractor = new WebExtractor();
            extractor.extractSinceUrl(url.getElement());
            List<Block> blocksObtain = extractor.getBlocks(1);
            String titlePage = extractor.getTitlePage();
            String text = blocksObtain.get(PRINCIAL_BLOCK).getValue();
            if (newSatisfyParameters(blocksObtain.size(), titlePage, text)) {
                Host h = new Host();
                h.set(url.getParent());
                this.news.add(new New(text, summarizer(text), url.getElement(), h, titlePage));
//                this.news.add(new New(text, summarizer(text), url.getElement(), url.getParent(), titlePage));
                
            }
    }
    
    private String summarizer(String text) throws Exception{
        List<Sentence> sentencesSummarizer = Summarizer.getInstance(Language.languageClassifier(text)).generateSummary(text, SIZE_SUMMER_DEFAULT);
        return StringUtils.unionInText(sentencesSummarizer, "[...]");
    }

    private boolean newSatisfyParameters(Integer numberBlocks, String titlePage, String text){
        return (numberBlocks > MIN_BLOCKS && !titlePage.trim().isEmpty() && text.length()>MIN_SIZE_TEXT && !hasGarbage(text));//Si el titulo es blanco no nos interesa xq es noticias cativeiras
            
    }
    
    private boolean hasGarbage(String text){
        List<String> basuras = LoaderConfiguration.getRulesDiscard();
        for(String itemBasura:basuras){
            if(text.matches(itemBasura)){
                return true;
            }
        }
        
        return false;
    }
}
