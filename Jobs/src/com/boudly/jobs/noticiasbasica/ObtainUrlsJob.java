/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.noticiasbasica;

import com.boudly.jobs.IJob;
import com.boudly.jobs.logic.Url;
import com.boudly.jobs.properties.LoaderConfiguration;
import com.tekstly.exceptions.LoadWebException;
import com.tekstly.logic.extractor.wrapper.DocumentWrapper;
import com.tekstly.logic.extractor.wrapper.ElementWrapper;
import com.tekstly.logic.extractor.wrapper.RequestWrapper;
import java.util.ArrayList;
import java.util.List;


public class ObtainUrlsJob implements IJob {

    private final List<String> urlsParents;
    private final List<Url> newsToObtain;
    public static final Integer BOUNDARY_ELEMENTS_ANALYSIS = 1;

    public ObtainUrlsJob() {
        this.urlsParents = new ArrayList<>();
        this.newsToObtain = new ArrayList<>();
    }

    /**
     * @return devuelve una lista de string con las urls
     */
    @Override
    public Object getResult() {
        return this.newsToObtain;
    }

    /**
     * @param args Recibe un lista de String con las urls padres en las que
     * queremos obtener la información
     * @throws com.boudly.exceptions.LoadWebException
     */
    @Override
    public void execute(List<Object> args) throws LoadWebException{
        List<String> totalParents = new ArrayList<>();
        for (Object url : args) {
            this.urlsParents.add(url.toString());//Le introducimos la url inicial
            getNewsURLs(url.toString());
            totalParents.addAll(urlsParents);
            this.urlsParents.clear();
        }
        this.urlsParents.addAll(totalParents);
    }

    private void getNewsURLs(String hostInitial) throws LoadWebException {
            Integer elementosAnalizados = 0;
            while (urlsParents.size() > 0 && elementosAnalizados < BOUNDARY_ELEMENTS_ANALYSIS) {
                try {
                    String url = urlsParents.get(0);
                    DocumentWrapper document = RequestWrapper.get(url);
                    classifierLinks(document.getAllLinks(), hostInitial);
                    urlsParents.remove(0);
                    elementosAnalizados++;
                } catch (LoadWebException ex) {
                    throw  new LoadWebException("PRoblema obteniendo urls de " + hostInitial + " : \n" + ex.getMessage());
                }
            }
    }

    private void classifierLinks(List<ElementWrapper> elements, String host) {
        for (ElementWrapper e : elements) {
            String href = e.getHref().replaceAll("#.*", "").replaceAll("\\?.*", "").trim();
            if (useThisLink(href, host)) {
                if (isLinkWithNews(e.innerText())) {
                    if (!continInListNews(href)) {
                        newsToObtain.add(new Url(href, host));
                    }
                } else {
                    if (!urlsParents.contains(href) && !newsToObtain.contains(href)) {
                        urlsParents.add(href);
                    }
                }
            }
        }
    }
    
    private boolean continInListNews(String href){
        for(Url news:newsToObtain){
            if(news.getElement().equals(href)){
                return true;
            }
        }
        return false;
    }

    public static boolean useThisLink(String href, String host) {
        String baseHost = Url.getBasicHost(host); //usamos solo el host para la comparación
        String baseHref = href.split("#")[0];//usamos esto para eliminar todo lo que venga detras del #
        
        if(baseHref.equals(host)){
            return false;
        }else if (baseHref.matches("^" + baseHost + ".*") && !baseHref.matches("(jpeg|jpg|gif|png|mp4|webm|gif )$") && !baseHref.matches("^" + baseHost + "$")) {
           
            List<String> rules = LoaderConfiguration.getRulesNoMatch();
            for (String rule : rules) {
                if (baseHref.matches(rule)) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    private static boolean isLinkWithNews(String tittle) {
        return tittle.split(" ").length > 3;
        //Principio de titulo y sección. Normalmente un titulo tiene más de 3 palabras
        //Sin embargo hay veces que eso no es así, por ejemplo Catastrofe. Por lo tanto tenemos que pensar como analizar esto

    }
}
