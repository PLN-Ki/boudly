/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs;

import java.util.List;

public interface IJob {
    public Object getResult();
    public void execute(List<Object> args) throws Exception;
}
