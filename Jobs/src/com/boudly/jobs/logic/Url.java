/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.logic;

import com.boudly.orm.logic.Host;


public class Url {
    private String element;
    private String parent;
    private Host host;
    
    
    public Url(String element, Host parent){
        this.parent = parent.getUrl();
        this.element = element;
        this.host = parent;
    }
    
    public Url(String element, String parent){
        this.parent = parent;
        this.element = element;
        this.host = new Host(getBasicHost(element), getBasicHost(element));
    }
    
    public static String getBasicHost(String url){
        String urlModify = url.replace("http://", "");
        urlModify = urlModify.split("/")[0];
        return "http://"+urlModify+"/";
    }

    /**
     * @return the element
     */
    public String getElement() {
        return element;
    }

    /**
     * @param element the element to set
     */
    public void setElement(String element) {
        this.element = element;
        this.host = new Host(getBasicHost(element), getBasicHost(element));
    }

    /**
     * @return the parent
     */
    public String getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * @return the str host
     */
    public String getHostStr() {
        return host.getUrl();
    }
    
    /**
     * @return the host
     */
    public Host getHost() {
        return host;
    }
    
}
