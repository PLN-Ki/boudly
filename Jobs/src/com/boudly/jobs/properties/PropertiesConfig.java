/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.properties;

import com.tekstly.exceptions.LoadFileException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class PropertiesConfig {

    private static final Map<String,Properties> properties = new HashMap<>();
    private static final String PROPERTIES_FILE = "ara_jobs_properties.properties";
    public static final String RULES_CONTAINS_FILE = "rules2contains";
    public static final String RULES_DISCARD_FILE = "rules2discard";

    public static Properties getInstance() throws LoadFileException {
        return getInstance(PROPERTIES_FILE);

    }
    
    public static Properties getInstance(String file) throws LoadFileException {
        if (!properties.containsKey(file)) {
            properties.put(file, openProperties(file));
        }
        return properties.get(file);

    }

    public static List<String> loadFile(String file) {
        List<String> loader = new ArrayList<>();

        BufferedReader br = null;

        try {

            String sCurrentLine;
            br = openReader(file);

            while ((sCurrentLine = br.readLine()) != null) {

                if (!sCurrentLine.startsWith("#")) {
                    loader.add(sCurrentLine.trim());
                }
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return loader;
    }

    public static BufferedReader openReader(String file) throws Exception {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            throw new Exception("Not found file");
        }

        return br;
    }

    private static Properties openProperties(String path) throws LoadFileException {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(path);

            prop.load(input);

        } catch (IOException io) {
            //Como la biblioteca se puede usar tambien des
            throw new LoadFileException("Imposible read file properties", LoadFileException.IMPOSIBLE_READ_FILE);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }

        }
        return prop;
    }


}
