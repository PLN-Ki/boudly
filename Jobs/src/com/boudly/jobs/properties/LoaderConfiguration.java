/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.properties;

import com.tekstly.exceptions.LoadFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoaderConfiguration {
    

    public static final String URLS = "com.boudly.jobs.config.urls.";
    private static List<String> rules;
    private static List<String> discard;
    

    /**
     * 
     * @param param
     * @return param
     * @throws LoadFileException 
     */
    public static String getParam(String param) throws LoadFileException{
        return PropertiesConfig.getInstance().getProperty(param);
    }
    
    public static List<Object> getUrls(){
        List<Object> elements = new ArrayList<>();
        try {
            boolean haveMore = true;
            Integer number = 1;
            while(haveMore){
                String url = getParam(URLS+number);
                if(url!=null){
                    elements.add(getParam(URLS+number));
                    number++;
                }else{
                    haveMore = false;
                }
            }
        } catch (LoadFileException ex) {
            Logger.getLogger(LoaderConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
        return elements;
    }
    
    public static List<String> getRulesNoMatch(){
        if(rules==null){
            rules = PropertiesConfig.loadFile(PropertiesConfig.RULES_CONTAINS_FILE);
        }
        return rules;
    }
    
    public static List<String> getRulesDiscard(){
        if(discard==null){
            discard = PropertiesConfig.loadFile(PropertiesConfig.RULES_DISCARD_FILE);
        }
        return discard;
    }
    
}
