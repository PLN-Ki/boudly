/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.noticiascomplejo;

import com.tekstly.exceptions.LoadFileException;
import com.boudly.jobs.IJob;
import com.boudly.orm.logic.New;
import com.boudly.classifier.NewsClassifier;
import com.boudly.jobs.properties.PropertiesConfig;
import com.tekstly.linguist.Language;
import com.tekstly.linguist.SentimentAnalysis;
import java.util.ArrayList;
import java.util.List;

public class AnalizerJob implements IJob {
    private static NewsClassifier instance;
    private List<New> newsClassifiers;
    private static final String TRAINNER = "com.boudly.jobs.classifier.model";

    public AnalizerJob() throws LoadFileException {
        instance = new NewsClassifier(PropertiesConfig.getInstance().getProperty(TRAINNER));
    }

    /**
     *
     * @param news lista con las noticia a clasificar
     * @throws java.lang.Exception
     */
    @Override
    public void execute(List<Object> news) throws Exception{
        this.newsClassifiers = new ArrayList<>();
        for (Object noticia : news) {
            this.newsClassifiers.add(classifierNew(obtainSentiment((New)noticia)));
        }
    }
    
    /**
     * @return devuelve las noticias clasificadas
     */
    @Override
    public Object getResult() {
        return this.newsClassifiers;
    }
    
    
    private New obtainSentiment(New noticia) throws Exception{
        SentimentAnalysis sentiment = SentimentAnalysis.getInstance(Language.ES);//No lo reconoce xq por aora solo funciona en español
        noticia.setSentiment(sentiment.calculateSentiment(noticia.getTitle()));
        return noticia;
        
    }
    
    private New classifierNew(New noticia){
        Integer idTag = noticia.getHost().getIdDefaultTag(); 
        if(idTag==null){
            idTag = this.instance.clasificar(noticia.getTextComplete()).getId();
        }
        
        noticia.setTag(idTag);
        
        return noticia;
        
    }
    
}
