/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.noticiascomplejo;

import com.boudly.orm.logic.Rule;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class CheckerRules {
    private static final Map<String, List<Rule>> rules = new HashMap<>();
    
    //Vamos a crear un singleton para evitar tener que consultar continuamente a la bd
    public static List<Rule> getRules(Integer type, String idHost){
        String key = type+"_"+idHost;
        if(!rules.containsKey(key)){
            rules.put(key, Rule.search(type, idHost));
        }
        return rules.get(key);
    }
}
