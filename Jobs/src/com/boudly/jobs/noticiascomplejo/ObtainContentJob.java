/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.jobs.noticiascomplejo;

import com.boudly.ejecutadores.BoudlyLog;
import com.boudly.jobs.IJob;
import com.boudly.jobs.logic.Url;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.New;
import com.boudly.orm.logic.Photo;
import com.boudly.orm.logic.Rule;
import com.tekstly.exceptions.LoadWebException;
import com.tekstly.extractor.WebExtractor;
import com.tekstly.linguist.Language;
import com.tekstly.linguist.Summarizer;
import com.tekstly.logic.extractor.Block;
import com.tekstly.logic.summarizer.Sentence;
import com.tekstly.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class ObtainContentJob implements IJob {

    public static final Float SIZE_SUMMER_DEFAULT = Float.parseFloat("0.3");
    private static final Integer MIN_BLOCKS = 0;
    private static final Integer PRINCIAL_BLOCK = 0;
    private static final Integer MIN_SIZE_TEXT = 170;//caracteres
    private final List<New> news;

    public ObtainContentJob() {
        this.news = new ArrayList<>();
    }

    /**
     * @return devuelve una lista de noticias
     */
    @Override
    public Object getResult() {
        return this.news;
    }

    /**
     *
     * @param newsToObtain lista con las urls de donde obtener la información
     * @throws java.lang.Exception
     */
    @Override
    public void execute(List<Object> newsToObtain) throws Exception {
        for (Object url : newsToObtain) {
            stractProcess((Url) url);
        }
    }

    private void stractProcess(Url url) {
        try {
            WebExtractor extractor = new WebExtractor();
            extractor.extractSinceUrl(url.getElement());
            List<Block> blocksObtain = extractor.getBlocks(1);
            if (!blocksObtain.isEmpty()) {
                String titlePage = extractor.getTitlePage().split("(-|\\|)")[0];
                String text = blocksObtain.get(PRINCIAL_BLOCK).getValue();
                List<Photo> photos = convertPhotos(blocksObtain.get(PRINCIAL_BLOCK).getImages(), url.getHost());
                if (newSatisfyParameters(blocksObtain.size(), titlePage, text, url.getHost())) {
                    this.news.add(new New(text, summarizer(text), url.getElement(), url.getHost(), titlePage, photos));

                }
            }
        } catch (LoadWebException ex) {
            BoudlyLog.getInstance().error(ex.getMessage());
        } catch (Exception ex) {
            BoudlyLog.getInstance().error(ex.getMessage());

        }
    }

    private List<Photo> convertPhotos(List<String> urls, Host host) {
        List<Photo> photos = new ArrayList<>();
        for (String url : urls) {
            if (!photoToDelete(url, host.getId())) {
                if (!url.matches("^http://.*") && !url.matches("^www\\,.*")) {
                    //Hay images que vienen con ruta realativa dentro de la web aqui intentamos corregirlas
                    url = Url.getBasicHost(host.getUrl()) + url;
                }
                photos.add(new Photo(url));
            }
        }

        return photos;
    }

    private boolean photoToDelete(String url, String idHost) {
        List<Rule> rules = CheckerRules.getRules(Rule.PHOTOS_TYPE, idHost);
        for (Rule item : rules) {
            if (url.matches(item.getValue())) {
                return true;
            }
        }

        return false;
    }

    private String summarizer(String text) throws Exception {
        List<Sentence> sentencesSummarizer = Summarizer.getInstance(Language.languageClassifier(text)).generateSummary(text, SIZE_SUMMER_DEFAULT);
        return StringUtils.unionInText(sentencesSummarizer, "[...]");
    }

    private boolean newSatisfyParameters(Integer numberBlocks, String titlePage, String text, Host host) {
        return (numberBlocks > MIN_BLOCKS && !titlePage.trim().isEmpty() && text.length() > MIN_SIZE_TEXT && !hasGarbage(text, host));//Si el titulo es blanco no nos interesa xq es noticias cativeiras

    }

    private boolean hasGarbage(String text, Host host) {
        List<Rule> basuras = CheckerRules.getRules(Rule.CONTAIN_TYPE, host.getId());
        for (Rule itemBasura : basuras) {
            if (text.matches(itemBasura.getValue())) {
                return true;
            }
        }

        return false;
    }
}
