/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.shortner.exception;

public class LoadException extends ShortnerApiException {

    public static final Integer NOT_FOUND_FILE = 404;

    public static final Integer IMPOSIBLE_READ_FILE = 405;

    public static final Integer MALFORMED_FILE = 406;

    private Integer errorCode;

    /**
     *
     *
     * @param msg exception msg
     *
     *
     * @param code exception code
     * @param fileName
     *
     *
     */
    public LoadException(String msg, Integer code, String fileName) {

        super(code+" "+fileName+" : "+msg);

        this.errorCode = code;

    }

    
    /**
     *
     *
     * @param msg exception msg
     *
     *
     * @param code exception code
     * @param fileName
     *
     *
     */
    public LoadException(String msg, Integer code) {

        super(code+" : "+msg);

        this.errorCode = code;

    }
    
    /**
     *
     *
     * @return errorCode error code
     *
     *
     */
    public Integer getErrorCode() {

        return this.errorCode;

    }

}
