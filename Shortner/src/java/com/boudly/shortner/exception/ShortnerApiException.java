package com.boudly.shortner.exception;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class ShortnerApiException extends Exception{
    public static final Integer DEFAULT_CODE = 500;
    private Integer code;
    
    public ShortnerApiException(String mensage) {
        super(mensage);
        this.code = DEFAULT_CODE;
    }
    
    public ShortnerApiException(String mensage, Integer code) {
        this(mensage);
        this.code = code;
    }
    
    public Integer getCode(){
        return this.code;
    }
}
