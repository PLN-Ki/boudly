package com.boudly.shortner.api.actions;

import com.boudly.orm.config.DB;
import com.boudly.orm.logic.Shortner;
import com.boudly.shortner.exception.LoadException;
import com.boudly.shortner.exception.ShortnerApiException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2016
 */
public class RedirectAction {

    /* @param ctx
     * @param authToken
     * @param roles
     * @return idUSer
     *
     * @since 1.0.0
     */
    public static URI search(String uuid) throws ShortnerApiException {
        DB.initWithJniName();
        Shortner redirect = Shortner.search(uuid);
        if (redirect != null) {
            try {
                Shortner.incrementOpenCount(redirect.getIdShortner());
                DB.close();
                return new URI(redirect.getUrl());
            } catch (URISyntaxException ex) {
                throw new LoadException("URL " + uuid + " imposible redirect.", LoadException.IMPOSIBLE_READ_FILE);
            }
        } else {
            throw new LoadException("URL " + uuid + " not found.", LoadException.NOT_FOUND_FILE);
        }
    }

}
