package com.boudly.shortner.api;

import com.boudly.orm.logic.Shortner;
import com.boudly.shortner.api.actions.RedirectAction;
import com.boudly.shortner.api.logic.Erro;
import com.boudly.shortner.exception.ShortnerApiException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
public class WebServiceEditor extends org.glassfish.jersey.servlet.ServletContainer {

    @Context
    ServletContext ctx;


    /*
     * GET api/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        return Response.ok("pong").build();

    }

    /*
     * GET api/
     * 
     * WS return url since a uuid 
     *
     * @since 1.0.0
     *
     * @param uuid
     * @return url
     *
     */
    @GET
    @Path("/{uuid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getUrl(@PathParam("uuid") String uuid) {
        try {
            return Response.temporaryRedirect(RedirectAction.search(uuid)).build();
        } catch (ShortnerApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }

//    /*
//     * POST api/
//     * 
//     * WS return url since a uuid 
//     *
//     * @since 1.0.0
//     *
//     * @param url
//     * @return uuid
//     *
//     */
//    @POST
//    @Path("/")
//    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
//    public Response createUrl(@FormParam("url") String url) {
//        return Response.ok("Hola! " + url).build();
//
//    }

}
