angular.module('scroll', [])

.directive('whenScrolled', function($document, $window) {
    return function(scope, elm, attr) {
        var raw = elm[0];

        $document.bind( 'scroll', function() {
          if( ($window.innerHeight + $window.scrollY) > raw.scrollHeight - 100) {
                scope.$apply(attr.whenScrolled);
          } 
        });
    };
});
