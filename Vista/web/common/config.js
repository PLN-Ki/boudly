'use strict';

function boudlyAppConfig($routeProvider, $translateProvider) {
    $translateProvider.useLoader('customTranslateLoader', {
      prefix: 'i18n/',
      suffix: '.json',
      defaultLang: 'en'
    });



    $routeProvider
      .when('/', {
        templateUrl : 'templates/news/list.html',
        controller  : 'NewsCtrl',
        controllerAs: "news"
      })

      .when('/search', {
        templateUrl : 'templates/searcher/result.html',
        controller  : 'SearcherResultCtrl',
        controllerAs: 'resultSearch'
      })

      .when('/login', {
        templateUrl : 'templates/login/login.html',
        controller  : 'LoginCtrl',
        controllerAs: 'lctrl'
      })

      .when('/profile', {
        templateUrl : 'templates/profile/profile.html',
        controller  : 'ProfileCtrl',
        controllerAs: "pctrl"
      })

      .when('/news', {
        templateUrl : 'templates/news/list.html',
        controller  : 'NewsCtrl',
        controllerAs: "news"
      })

      .when('/news/read/later', {
        templateUrl : 'templates/news/list.html',
        controller  : 'FavoritesCtrl',
        controllerAs: "news"
      })
}

var configurationUrls = {
  'newsUrl' : "http://api.boudly.argalladas.eu/tags/news?",//"http://api.ara.boudly.com/jsonp/news?callback=JSON_CALLBACK&";//
  'newsImportantUrl' : "http://api.boudly.argalladas.eu//tags/news/important?",
  'tagsUrl' : "http://api.boudly.argalladas.eu/tags?",//"http://api.ara.boudly.com/jsonp/hosts?callback=JSON_CALLBACK";
  'searcherNewsUrl' : "http://api.boudly.argalladas.eu/search/es/news?q=",
  'loginUrl' : "http://api.boudly.argalladas.eu/user/login",
  'userHosts' : "http://api.boudly.argalladas.eu/user/hosts",
  'searcherHosts': "http://api.boudly.argalladas.eu/search/hosts?q=",
  'checkToken': "http://api.boudly.argalladas.eu/user/validate",
  'userHost': "http://api.boudly.argalladas.eu/user/host/:idHost",
  'user': "http://api.boudly.argalladas.eu/user",
  'userTag' : "http://api.boudly.argalladas.eu/user/tag/:idTag?visibility=",
  'addFavorite' : "http://api.boudly.argalladas.eu/user/news/favorites/:id_new?favorite_value=",
  'favorites' : "http://api.boudly.argalladas.eu/user/news/favorites?",
  'markRead' : "http://api.boudly.argalladas.eu/user/news/:id_new?read=",
  "hostFeedback": "http://api.boudly.argalladas.eu/feedback/host/suggestion",
  "appFeedback": "http://api.boudly.argalladas.eu/feedback/app/suggestion",
  "newsFeedback": "http://api.boudly.argalladas.eu/feedback/news/:id_new/error"

};


function configurationRun($window, $rootScope, $location, UserService, event, next, current) {
      //Iniciamos next si no viene iniciao
      next = next==undefined||next==null?{}:next;
      
      // register listener to watch route changes
        UserService.isValidToken(function(isValid){
            if(!isValid && $location.$$path==="/anonymous"){
              UserService.setAnonymousUser();
              $location.path( "/news" );
            }else if(next.originalPath==="/news" && UserService.isUserType(UserService.TYPE_ANONYMOUS)){

            }else if(!isValid && next.originalPath==="/login") {
              $window.location = './login.html';
            }else if(!isValid){
              $location.path( "/login" );
            }else if(isValid && (next.originalPath==="/login")){
              $location.path( "/news" );
            }else if(isValid && next.originalPath==="/"){
              $location.path( "/news" );
            }

        });        
}

