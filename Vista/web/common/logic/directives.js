'use strict';

angular.module('boudly')

.directive('news', function() {
  return {
    restrict: 'E',
    controller  : 'DetailNewsCtrl',
    controllerAs: 'ctrlDetail',
    scope: {
      newInfo: '=info'
    },
    templateUrl: './templates/directive/news.html'
  };
})

.directive('feedback', function() {
  return {
    restrict: 'E',
    controller  : 'FeedbackCtrl',
    controllerAs: 'fdkCtrl',
    scope: {
      type: '=type',
      header: '=header',
      idnew: '=idnew'

    },
    templateUrl: './templates/directive/feedback.html'
  };
})

.directive('showError', function() {
  return {
    restrict: 'E',
    templateUrl: './templates/directive/show-error.html',
    controller  : 'ShowErrorCtrl',
    controllerAs: 'showErrorCtl',
  }; 
})

.directive('notNews', function() {
  return {
    restrict: 'E',
    templateUrl: './templates/directive/not-news.html',
    controller  : 'NotNewsCtrl',
    controllerAs: 'notNewCtl',
  };
})

.directive('loading', function() {
  return {
    restrict: 'E',
    templateUrl: './templates/directive/loading.html'
  };
})

.directive('menu', function() {
  return {
    restrict: 'E',
    templateUrl: './templates/directive/menu.html',
    controller  : 'SearcherCtrl'
  };
});