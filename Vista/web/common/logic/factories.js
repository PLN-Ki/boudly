'use strict';

angular.module('boudly')

.service('News', newsFactory)

.service('Favorites', favoritesFactory)

.factory('SettingsMenu', settingsMenuFactory)

.factory('SettingsNews', settingsNewsFactory)

.factory('SettingsFavorites', settingsFavoritesFactory)

.factory('UserService', userServiceFactory)

.factory('SearcherNews', searcherNewsFactory)

.factory('SearcherHost', searcherHostFactory)

.factory('FeedbackFactory', feedbackFactory)

.factory('customTranslateLoader', customTranslateLoader);


function feedbackFactory($http, configuration, UserService) {
  var isLoading = false;

  return {
    TYPE_FDK_APP: 1,
    TYPE_FDK_HOST: 2,
    TYPE_FDK_NEWS: 3,

    isLoading: function(){
      return isLoading;
    },

    convertTypeInUrl: function(params){
      var url = undefined;
      var that = this;

      if(params.type==that.TYPE_FDK_APP){
        url = configuration.appFeedback;

      }else if(params.type==that.TYPE_FDK_HOST){
        url = configuration.hostFeedback;

      }else if(params.type==that.TYPE_FDK_NEWS){
        url = addParamsPathUrl(configuration.newsFeedback, {'id_new': params.id_new});

      }

      return url;
    },

    send: function(parameters, ok, error){
      var that = this;
      if(isLoading){
        return;
      }

      isLoading  = true;

      $http.defaults.headers.common['user_token']= UserService.getUser().token;

      
      $http.post(that.convertTypeInUrl(parameters), $.param(parameters),{headers:
          {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
      }).then(function(response) {
          isLoading  = false;
          ok(response.data);
      }, function(response) {
          isLoading  = false;
          error(response);
      });
    }
  }
}


function customTranslateLoader($http, $q, $translate) {

  return function (options) {

    if (!options || (!angular.isArray(options.files) && (!angular.isString(options.prefix) || !angular.isString(options.suffix)))) {
      throw new Error('Couldn\'t load static files, no files and prefix or suffix specified!');
    }

    if (!options.files) {
      options.files = [{
        prefix: options.prefix,
        suffix: options.suffix,
        defaultLang: options.defaultLang
      }];
    }

    var load = function (file) {
      if (!file || (!angular.isString(file.prefix) || !angular.isString(file.suffix))) {
        throw new Error('Couldn\'t load static file, no prefix or suffix specified!');
      }

      var deferred = $q.defer();

      $http(angular.extend({
        url: [
          file.prefix,
          options.key,
          file.suffix
        ].join(''),
        method: 'GET',
        params: ''
      }, options.$http)).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        //Fix para que use el idioma por defecto que no funcionaba en la versión por defecto
        $translate.use(options.defaultLang);
      });

      return deferred.promise;
    };

    var deferred = $q.defer(),
        promises = [],
        length = options.files.length;

    for (var i = 0; i < length; i++) {
      promises.push(load({
        prefix: options.files[i].prefix,
        key: options.key,
        suffix: options.files[i].suffix
      }));
    }

    $q.all(promises).then(function (data) {
      data = [data[0]];
      var length = data.length,
          mergedData = {};

      for (var i = 0; i < length; i++) {
        for (var key in data[i]) {
          mergedData[key] = data[i][key];
        }
      }

      deferred.resolve(mergedData);
    }, function (data) {
      deferred.reject(data);
    });

    return deferred.promise;
  };
}

function favoritesFactory($http, SettingsNews, SettingsFavorites, configuration, UserService) {
  var news = [];
  var isLoading = false;

  return {
    isLoading: function(){
      return isLoading;
    },

    all: function() {
      return news;
    },
    remove: function(nw) {
      news.splice(news.indexOf(nw), 1);
    },
    get: function(newId) {
      var numberElements = news.length;
      for (var i = 0; i < numberElements; i++) {
        if (news[i].id === parseInt(newId)) {
          return news[i];
        }
      }
      return null;
    },
    set: function(element){
      news.push(element)
    },

    cleanAll:function(){
      news = [];
    },

    obtainMoreNews: function(url, ok, error){
      var that = this;
      if(isLoading  || !SettingsFavorites.haveMoreData()){
        return;
      }

      isLoading  = true;

      $http.defaults.headers.common['user_token']= UserService.getUser().token;

      $http.get(url)
      .then(function(response) {
        if(response.data.length===0){
              SettingsFavorites.setMoreData(false);
        }else{
              response.data.forEach(function(element) {
                that.set(element);
                SettingsFavorites.incrementFrom();
            });
          }

          isLoading  = false;
          ok(response);
      }, function(response) {
        isLoading  = false;
        error(response);
      });
    },

  };
}

function newsFactory($http, SettingsNews, configuration, UserService) {
  var news = [];
  var isLoading = false;
  var WAITING_FINISH_VALUE = 1;
  var READ_VALUE = 1;

  return {
    markRead : function(news2mark, onFinish){
      var that = this;
      that.waiting = news2mark.length;//Cuantas peticiones tenemos que esperar hasta que podamos enviar la función de finish
      if(that.waiting==0){
        onFinish();
      }else{
          isLoading  = true;
          for(var pos=0;pos<news2mark.length;pos++){//Enviamos peticiones
            that.markReadElement(news2mark[pos].id, function(response){
              SettingsNews.decrementedFrom();
              that.waiting--;
              if(that.waiting<WAITING_FINISH_VALUE){//Si ya tenemos todas las respuestas por la que estabamos esperando
                isLoading  = false;
                onFinish();
              }

            }, function(response){
              that.waiting--;
              if(that.waiting<WAITING_FINISH_VALUE){//Si ya tenemos todas las respuestas por la que estabamos esperando
                isLoading  = false;
                onFinish();
              }

            });
          }
      }
      
    },

    markReadElement: function(idNew, ok, error){
      $http.defaults.headers.common['user_token']= UserService.getUser().token;
      
      $http.put(addParamsPathUrl(configuration.markRead, {'id_new': idNew})+READ_VALUE)
      .then(function(response) {
          ok(response);
      }, function(response) {
        error(response);
      });

    },

    isLoading: function(){
      return isLoading;
    },

    all: function() {
      return news;
    },
    remove: function(nw) {
      news.splice(news.indexOf(nw), 1);
    },
    get: function(newId) {
      var numberElements = news.length;
      for (var i = 0; i < numberElements; i++) {
        if (news[i].id === parseInt(newId)) {
          return news[i];
        }
      }
      return null;
    },
    set: function(element){
      news.push(element)
    },

    cleanAll:function(){
      news = [];
    },

    changeFavoriteValue:function(item, ok, error){
      var that = this;
      $http.defaults.headers.common['user_token']= UserService.getUser().token;
      var favoriteValue = (!item.isFavorite)?1:0;
      $http.put(addParamsPathUrl(configuration.addFavorite, {'id_new': item.id})+favoriteValue)
      .then(function(response) {
        item.isFavorite = !item.isFavorite;
      }, function(response) {
        error(response);
      });
    },

    obtainMoreNews: function(url, ok, error){
      var that = this;
      if(isLoading  || !SettingsNews.haveMoreData()){
        return;
      }

      isLoading  = true;
      $http.defaults.headers.common['user_token']= UserService.getUser().token;

      $http.get(url)
      .then(function(response) {
        //Cuando no vienen ya más noticias no tenemos que solicitar más
        if(response.data.length===0){
              SettingsNews.setMoreData(false);
        }else{
          //cuando el usuario es logued no necesitamos solicitar más noticias, podríamos pero nos repetiría las mismas
              if(!UserService.isUserType(UserService.TYPE_LOGGED)){
                SettingsNews.setMoreData(false);
              }
              response.data.forEach(function(element) {
                that.set(element);
                SettingsNews.incrementFrom();
            });
          }

          isLoading  = false;
          ok(response);
      }, function(response) {
        isLoading  = false;
        error(response);
      });
    },

  };
}


function userServiceFactory($http, $localStorage, configuration) {
  var $storage = $localStorage;

  $storage.user = $storage.user === undefined?{}:$storage.user;


  var isLoading = false;
  var hosts = [];

  return {
    TYPE_ANONYMOUS: "anonymous",
    TYPE_LOGGED : "logged",
    getHosts: function(){
      return this.hosts;
    },

    getHost: function(idHost){
      var elements = this.hosts.length;
      for(var pos =0;pos<elements;pos++){
        if(this.hosts[pos].id===idHost){
          return this.hosts[pos];
        }
      }
      return undefined;
    },

    setUser: function(user){
      $storage.user = user;

    },

    setAnonymousUser: function(){
      if(this.getToken()===undefined || this.getToken() ==""){
        $storage.user = {type: this.TYPE_ANONYMOUS};
      }
    },

    isAnonymousUser : function(){
      return this.getUser().type===this.TYPE_ANONYMOUS;
    },

    isUserType : function(type){
      return this.getUser()!==undefined && this.getUser().type===type;
    },

    isValidToken: function(reponseFunction){
      var that = this;
      var token = that.getToken();
      
      if(token === undefined || token ==""){
        reponseFunction(false);
      }else{
        $http.defaults.headers.common['user_token']= token;
        $http.get(configuration.checkToken).then(function(response) {
            reponseFunction(true);
        }, function(response) {
            that.setToken(undefined);
            reponseFunction(false);
        });
      }
    },

    getUser: function(){
      return $storage.user;
    },

    setToken: function(token){
      if($storage.user===undefined){
        $storage.user = {};
      }
      $storage.user.token = token;
    },

    getToken: function(){
      return $storage.user===undefined?undefined:$storage.user.token;
    },

    logOut: function(){
      $http.defaults.headers.common['user_token']=null;
      delete $storage.user;
    },

    login: function(user, pass, ok, error){
      var that = this;
      if(isLoading){
        return;
      }
      that.setUser(user)

      isLoading  = true;

      var access_token = btoa(pass + "::" + that.getUser().email);


      $http.defaults.headers.common['access_token']= access_token;


      $http.get(configuration.loginUrl).then(function(response) {
          that.setToken(response.data.token);
          that.getUser().type = that.TYPE_LOGGED;
          isLoading  = false;
          ok(response.data);
      }, function(response) {
          that.logOut();
          isLoading  = false;
          error(response);
      });
    },

    signUp: function(user, pass, ok, error){
      var that = this;
      if(isLoading){
        return;
      }

      isLoading  = true;

      var access_token = btoa(pass + "::" + user.email);

      $http.defaults.headers.common['access_token']= access_token;
      

      $http.post(configuration.user, $.param({name:user.name}),{headers:
          {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          }
      }).then(function(response) {
          isLoading  = false;
          ok(response.data);
      }, function(response) {
          isLoading  = false;
          error(response);
      });
    },

    obtainHosts: function(ok, error){
        var that = this;
        if(isLoading){
          return;
        }

        isLoading  = true;

        $http.defaults.headers.common['user_token']= that.getUser().token;


        $http.get(configuration.userHosts).then(function(response) {
            isLoading  = false;
            that.hosts = response.data;
            ok(response.data);
        }, function(response) {
            isLoading  = false;
            error(response);
        });
    },

    addHost: function(idHost, ok, error){
        var that = this;
        if(isLoading){
          return;
        }

        isLoading  = true;

        $http.defaults.headers.common['user_token']= that.getUser().token;

        $http.post(addParamsPathUrl(configuration.userHost, {'idHost': idHost})).then(function(response) {
            isLoading  = false;
            that.hosts = response.data;
            ok(response.data);
        }, function(response) {
            isLoading  = false;
            error(response);
        });
    },

    removeHost: function(idHost, ok, error){
        var that = this;
        if(isLoading){
          return;
        }

        isLoading  = true;

        $http.defaults.headers.common['user_token']= that.getUser().token;


        $http.delete(addParamsPathUrl(configuration.userHost, {idHost: idHost})).then(function(response) {
            isLoading  = false;
            that.hosts = response.data;
            ok(response.data);
        }, function(response) {
            isLoading  = false;
            error(response);
        });
    }

  };
}

function searcherNewsFactory($http, SettingsNews, configuration) {
  var news = [];
  var isLoading = false;

  var to = 5;
  var defaultFrom = -1;
  var actualFrom = defaultFrom;
  var moredata = true;
  var queryNew = "";


  return {

    restartVarsDefault: function(){
      actualFrom = defaultFrom;
      moredata = true;
      news = [];
      queryNew = "";
    },

    getSearchNewsUrl: function(query){
        return configuration.searcherNewsUrl+query+"&from="+this.getNextFrom()+"&to="+this.getTo();
    },

    haveMoreData:function(){
      return moredata;
    },

    setMoreData: function(setter){
      moredata = setter;
    },

    getTo: function(){
      return to;
    },

    getNextFrom: function(){
      return actualFrom+1;
    },

    getActualFrom: function(){
      return actualFrom;
    },

    incrementFrom: function(){
      actualFrom +=1;
    },

    isLoading: function(){
      return isLoading;
    },

    all: function() {
      return news;
    },

    remove: function(nw) {
      news.splice(news.indexOf(nw), 1);
    },

    get: function(newId) {
      var numberElements = news.length;
      for (var i = 0; i < numberElements; i++) {
        if (news[i].id === parseInt(newId)) {
          return news[i];
        }
      }
      return null;
    },

    setNew: function(element){
      news.push(element)
    },

    getQueryElement: function(){
      return this.queryNew;
    },

    moreNews: function(url, ok, error){
      var that = this;
      if(isLoading  || !that.haveMoreData()){
        return;
      }

      isLoading  = true;

      $http.get(url)
      .then(function(response) {
        that.queryNew = response.data.query;
        if(response.data.found.length===0){
              that.setMoreData(false);
        }else{
              response.data.found.forEach(function(element) {
                that.setNew(element);
                that.incrementFrom();
            });
          }

          isLoading  = false;
          ok(response);
      }, function(response) {
        isLoading  = false;
        error(response);
      });
    },

  };
}

function searcherHostFactory($http, $localStorage, UserService, configuration) {
  var result = [];
  var isLoading = false;

  var to = 10;
  var defaultFrom = -1;
  var actualFrom = defaultFrom;
  var moredata = true;

  return {
    restartVarsDefault: function(){
      actualFrom = defaultFrom;
      moredata = true;
      result = [];
    },

    getSearchHostUrl: function(query){
        return configuration.searcherHosts+query+"&from="+this.getNextFrom()+"&to="+this.getTo();
    },

    haveMoreData:function(){
      return moredata;
    },

    getTo: function(){
      return to;
    },

    getNextFrom: function(){
      return actualFrom+1;
    },

    getActualFrom: function(){
      return actualFrom;
    },

    incrementFrom: function(increment){
      actualFrom +=increment===undefined?1:increment;
    },

    isLoading: function(){
      return isLoading;
    },

    getResult: function(){
      return result;
    },

    setResult: function(resultado){
      result = resultado;
    },

    moreHosts: function(url, ok, error){
        var that = this;
        if(isLoading){
          return;
        }

        isLoading  = true;

        $http.defaults.headers.common['user_token']= UserService.getUser().token;


        $http.get(url).then(function(response) {
            that.setResult(that.getResult().concat(response.data.found));
            isLoading  = false;
            moredata = response.data.found.length!==0;
            that.incrementFrom(response.data.found.length);
            ok(response.data.found);
        }, function(response) {
            isLoading  = false;
            error(response);
        });
    }

  };
}

function settingsMenuFactory($http, $localStorage, $sessionStorage, configuration, UserService) {

  var $storage = $localStorage;
  var menu;

  $storage.menuSaved = $storage.menuSaved === undefined?[]:$storage.menuSaved;


  return {

    changeSettingElement:function(newElement){
      var numberElements = this.menu.length;
      var that = this;
      for (var i = 0; i < numberElements; i++) {
        if (this.menu[i].id === parseInt(newElement.id)) {
          this.changeUserSettingElement(newElement, function(elementChanged){
            that.changeVisibilityElement(i, elementChanged);
          }, function(){/*EERROR*/});
          return;
        }
      }
    },

    changeVisibilityElement: function(pos, elementChanged){
      var that = this;
      if(!UserService.isUserType(UserService.TYPE_LOGGED)){//cambiamos valor en la chacé
        $storage.menuSaved[pos] = elementChanged;
      }
      that.menu[pos] = elementChanged;
    },

    changeUserSettingElement:function(newElement, ok, error){
      if(UserService.isUserType(UserService.TYPE_LOGGED)){
          var that = this;
          $http.defaults.headers.common['user_token']= UserService.getUser().token;
          var visible = newElement.enable?1:0;
          $http.put(addParamsPathUrl(configuration.userTag, {'idTag': newElement.id})+visible).then(function(response) {
              ok(newElement);
          }, function(response) {
              error(response);
          });
      }else{
        ok(newElement);
      }
    },

    all: function() {
      return this.menu;
    },

    isSaved: function(id){
      var elements = $storage.menuSaved.length;
      for(var i=0;i<elements;i++){
        if($storage.menuSaved[i].id===id){
          return $storage.menuSaved[i];
        }
      }
      return undefined;
    },

    loadSettings: function(ok, error){

        var that = this;

        $http.defaults.headers.common['user_token']= UserService.getUser().token;


        $http.get(configuration.tagsUrl)
        .then(function(response) {
            if(!UserService.isUserType(UserService.TYPE_LOGGED)){//Si no está logueado salvamos del storage
              response.data.forEach(function(element) {
                var savedElement = that.isSaved(element.id);//cargamos de la chaché del navegador
                element.enable = savedElement===undefined?true:savedElement.enable;
              });
              //Lo guardamos cuando no es logueado para poder precargarlo
              $storage.menuSaved = response.data;
            }
              
            that.menu = response.data;
            ok(response);
        }, function(response) {
          error(response);
        });
    }

  };
}

function settingsNewsFactory($http, $localStorage, $sessionStorage, configuration, UserService, SettingsMenu) {

  var to = 5;
  var defaultFrom = -1;
  var actualFrom = defaultFrom;
  var moredata = true;
  var basicUrl = undefined;


  return {

    getBasicUrl :function(){
      if(!UserService.isUserType(UserService.TYPE_LOGGED)){
        return configuration.newsImportantUrl+this.getHosts();
      } else if(basicUrl===undefined){
        basicUrl = configuration.newsUrl+this.getHosts();
      }
      return basicUrl;
    },


    haveMoreData:function(){
      return moredata;
    },

    setMoreData: function(setter){
      moredata = setter;
    },


    getTo: function(){
      return to;
    },

    getNextFrom: function(){
      return actualFrom+1;
    },

    getActualFrom: function(){
      return actualFrom;
    },

    incrementFrom: function(){
      actualFrom +=1;
    },

    decrementedFrom: function(){
      actualFrom -=1;
    },

    restartVarsDefault: function(){
      actualFrom = defaultFrom;
      moredata = true;
      basicUrl = undefined;
    },

    getHosts: function(){
      var hosts = "";
      SettingsMenu.all().forEach(function(element) {
          if(element.enable){
            hosts+="ids="+element.id+"&";
          }
      });
      return hosts;
    },

  };
}

function settingsFavoritesFactory($http, $localStorage, $sessionStorage, configuration, UserService, SettingsMenu) {

  var to = 5;
  var defaultFrom = -1;
  var actualFrom = defaultFrom;
  var moredata = true;
  var basicUrl = undefined;


  return {

    getBasicUrl :function(){
        return configuration.favorites;
    },

    haveMoreData:function(){
      return moredata;
    },

    setMoreData: function(setter){
      moredata = setter;
    },


    getTo: function(){
      return to;
    },

    getNextFrom: function(){
      return actualFrom+1;
    },

    getActualFrom: function(){
      return actualFrom;
    },

    incrementFrom: function(){
      actualFrom +=1;
    },

    restartVarsDefault: function(){
      actualFrom = defaultFrom;
      moredata = true;
      basicUrl = undefined;
    },

  };
}
