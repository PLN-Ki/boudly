'use strict';

var app = angular.module('boudly',  [
	'boudly.controllersGUI',
	'boudly.controllersBootStrap',
	'boudly.controllersTable',
    'boudly.serviceGUI',
	'ngStorage', 
	'scroll',
  'ui.bootstrap',
	'ngRoute',
  'pascalprecht.translate',
/*
    'ngAnimate',
    'ngResource',
    'ui.router',
    'angular-loading-bar',
    'oc.lazyLoad',
    'nouislider',
    'ngTable'*/
])

.config(boudlyAppConfig)
.constant('configuration', configurationUrls)
.run(function($window, $rootScope, $location, UserService, $translate) {
      var langKey = $window.navigator.userLanguage || $window.navigator.language;
      if(langKey.indexOf("-")==-1){
        langKey += "-"+langKey.toUpperCase();
      }
      $translate.use(langKey);
      // register listener to watch route changes
      $rootScope.$on( "$routeChangeStart", function(event, next, current) {
            configurationRun($window, $rootScope, $location, UserService, event, next, current); 
      });

      if($location.$$path==="/anonymous"){
        configurationRun($window, $rootScope, $location, UserService, undefined, undefined, undefined);
      }    
});


