angular.module('boudly')

.controller('LoginCtrl', loginCtrl);

function loginCtrl($rootScope, $location, $window, UserService) {
  var self = this;
  self.user = {};
  self.userSignup = {};
  self.showLogin = 1;
  self.showRegister = 0;
  self.showForgot = 0;

  self.login = function(){
    var userWS = {email: self.user.email, token: ""};
    UserService.login(userWS, CryptoJS.SHA256(self.user.pass).toString(CryptoJS.enc.Base64), 
    function(data){/*ok*/ 
      $window.location = './#/profile';
    },
    function(data){/*error*/ 
      var errorValue = data.status=404?'error.get.user.notFound':'error.get.user.execution';
      $rootScope.$broadcast("showError", errorValue);

    });
  },

  self.init = function(){
    //Si tenemos un usuario correcto registrado cambiar de vista. No se hace en el config ya que la página de login es especial y no carga la config inicial
    UserService.isValidToken(function(isValid){
          if(isValid){
            $window.location = './#/news';
          }
              
    });
  },

  self.signUp = function(){
    if(self.userSignup.pass !== self.userSignup.pass2){
      $rootScope.$broadcast("showError", "error.add.checkPass");
      return;
    }
    var userWS = {email: self.userSignup.email, name: self.userSignup.name};
    UserService.signUp(userWS, CryptoJS.SHA256(self.userSignup.pass).toString(CryptoJS.enc.Base64), 
    function(data){/*ok*/ 
      self.cleanSignUp();
      $rootScope.$broadcast("showMsg", 'success.add.user');
      self.showLogin = 1;
      self.showRegister = 0;
      self.showForgot = 0;
    },
    function(data){/*error*/ 
      var errorValue = data.status=400?'error.add.inUseUser':'error.add.user';
      $rootScope.$broadcast("showError", errorValue);
    });

  },

  self.cleanSignUp = function(){
    self.userSignup = {};
  }
}