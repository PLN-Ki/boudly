angular.module('boudly')

.controller('ProfileCtrl', profileCtrlProfile)

.controller('DetailHostCtrl', detailHostCtrl);


function profileCtrlProfile(UserService, SearcherHost, $rootScope) {
  var self = this;
  self.user;
  self.querySearch = "";
  self.resultWord = "";
  self.showMyNewspaper = true;
  self.showSearcherResult = false;



  self.visibleMoreData = function(){
    return SearcherHost.haveMoreData();
  },


  self.getHosts = function(){
      return UserService.getHosts();
  },

  self.hostSelected = function(idHost){
    var elements = UserService.getHosts();
    return UserService.getHost(idHost)!==undefined;
  },

  self.init = function(){
    this.user = UserService.getUser();
    this.obtainHosts();
   
  },

  self.obtainHosts = function(){
    var that = this;
     UserService.obtainHosts(
      function(data){
        /*OK*/
      },
      function(data){
        /*ERROR*/
        $rootScope.$broadcast("showError", 'error.get.hosts');
      });
  },

  self.getResultSearch = function(){
    return SearcherHost.getResult();
  },

  self.cleanParam = function(){
    this.querySearch = "";
  },

  self.showSearcherNewsPaper = function(){
      this.showMyNewspaper = false;
      this.showSearcherResult = true;
  },

  self.showListNewsPaper = function(){
      this.showMyNewspaper = true;
      this.showSearcherResult = false;
  },

  self.searchHosts = function(){
    SearcherHost.restartVarsDefault();
    self.resultWord = self.querySearch;
    self.moreHosts();
  }

  self.moreHosts = function(){
    if(this.resultWord!==""){
      that = this;
      SearcherHost.moreHosts(SearcherHost.getSearchHostUrl(this.resultWord), 
        function(data){
          that.cleanParam();
          that.showSearcherNewsPaper();
        }, 
        function(){
          $rootScope.$broadcast("showError", 'error.get.hosts');
        });

      }
  }
}

function detailHostCtrl(UserService) {
  var self = this;
  self.init = function(){
  },

  self.delete = function(idHost){
    UserService.removeHost(idHost, function(data){
  }, function(data){
        $rootScope.$broadcast("showError", 'error.delete.hosts');});
  },

  self.add=function(idHost){
    UserService.addHost(idHost, function(data){
    }, function(data){
        $rootScope.$broadcast("showError", 'error.add.hosts');});
  }
}