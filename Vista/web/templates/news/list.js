'use strict';

angular
  .module('boudly')
  .controller('NewsCtrl', newsCtrl)
  .controller('FavoritesCtrl', favoritesCtrl);


function newsCtrl($scope, $window, $rootScope, $location, News, SettingsNews) {/*, Settings, News) {*/
  var self = this;
  self.queueToMarkRead = [];

  $scope.$on("reloadNews", function () {
    News.cleanAll();
    self.cleanCtrlVars();
    SettingsNews.restartVarsDefault();
    self.loadMore();
    
  });

  self.cleanCtrlVars = function(){
      self.queueToMarkRead = [];
  },


  self.getNews = function(){
     return News.all();
  };
  

  self.getNews = function(){
     return News.all();
  };
  
  self.getUrlActual = function(){
      return SettingsNews.getBasicUrl()+"&from="+SettingsNews.getNextFrom()+"&to="+SettingsNews.getTo();
  };

  self.showLoading = function(){
    return News.isLoading && SettingsNews.haveMoreData();
  };


  self.loadMore = function(){
      if(!News.isLoading()){//Si ya está trabajando el News Factory no realizará ninguna acción
          var news = self.queueToMarkRead.splice(0,SettingsNews.getTo()); //vamos a marcar las N primeras noticias  que hemos solicitado
          News.markRead(news, function(){//Primero marcamos
              News.obtainMoreNews(//Y una vez marcadas solicitamos más noticias
              self.getUrlActual(), 

              function(data) {
                //Si quisieramos hacer algo en el ok podríamos pero no queremos :P
                self.queueToMarkRead = self.queueToMarkRead.concat(data.data);
              }, 

              function(data) {
                $rootScope.$broadcast("showError", 'error.get.news');
              });
          });
      }
      
      
  };

  self.init = function(){
      self.cleanCtrlVars();
      SettingsNews.restartVarsDefault();
      $rootScope.$broadcast("loadSetting");
      //Temporal para tener noticias
      //SettingsNews.loadSettings(function(data){self.loadMore();},function(data){alert("Error: "+data)});
      
  }
}


function favoritesCtrl($scope, $window, $rootScope, $location, Favorites, SettingsFavorites) {/*, Settings, News) {*/
  $window.scrollTo(0,0);

  var self = this;

  self.getNews = function(){
     return Favorites.all();
  };
  
  self.getUrlActual = function(){
      return SettingsFavorites.getBasicUrl()+"&from="+SettingsFavorites.getNextFrom()+"&to="+SettingsFavorites.getTo();
  };

  self.showLoading = function(){
    return Favorites.isLoading && SettingsFavorites.haveMoreData();
  };


  self.loadMore = function(){
      Favorites.obtainMoreNews(
        self.getUrlActual(), 

        function(data) {
          //Si quisieramos hacer algo en el ok podríamos pero no queremos :P
        }, 

        function(data) {
          $rootScope.$broadcast("showError", 'error.get.favorites');
        });
  };

  self.init = function(){
      SettingsFavorites.restartVarsDefault();
      Favorites.cleanAll();
      self.loadMore();
      //Temporal para tener noticias
      //SettingsFavorites.loadSettings(function(data){self.loadMore();},function(data){alert("Error: "+data)});
      
  }
};



