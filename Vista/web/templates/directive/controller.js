'use strict';

angular
  .module('boudly')
  .controller('DetailNewsCtrl', detailNewsCtrl)
  .controller('NotNewsCtrl', notNewsCtrl)
  .controller('ShowErrorCtrl', showErrorCtrl)
  .controller('FeedbackCtrl', feedbackCtrl);

function feedbackCtrl(FeedbackFactory, $rootScope){
  var self = this;
  self.params;
  
  self.isVisibleMail = false;

  self.changeVisibilityMail = function(option){
    if(option===undefined){
      self.isVisibleMail = !self.isVisibleMail ;
    }else{
      self.isVisibleMail = option;
    }
  }

  self.initParams = function(){
    self.params = {
      "msg": "",
      "email": "",
      "name": ""
    };
  }

  self.send = function(type, idnew){
    self.params.type = type;
    self.params.id_new = idnew;
    FeedbackFactory.send(self.params, function(){
      //OK 
      self.changeVisibilityMail(false);
      $rootScope.$broadcast("showMsg", 'success.add.feedback');
      self.initParams();

    }, function(){
      $rootScope.$broadcast("showError", 'error.generic');
    });
  }


}


function showErrorCtrl($scope, $rootScope, $timeout){
  var self = this;
  self.visible = "fadeOutUp";
  self.msg = "";
  self.type = "";

  $scope.$on("showError", function (events, args) {
    self.show("danger", args);
    
  });

  $scope.$on("showWarning", function (events, args) {
    self.show("warning", args);
    
  });

  $scope.$on("showMsg", function (events, args) {
    self.show("success", args);
    
  });


  self.hide = function(){
    self.visible = "fadeOutUp";
  }

  self.show = function(type, msg){
    self.visible = "fadeInDown";
    self.msg = msg;
    self.type = "alert-"+type;
    $timeout(function() {
      self.hide();
    }, 5000);
  }

}


function notNewsCtrl(){

  var self = this;
  self.isVisibleMail = false;

  self.changeVisibilityMail = function(option){
    if(option===undefined){
      self.isVisibleMail = !self.isVisibleMail ;
    }else{
      self.isVisibleMail = option;
    }
  }

}


function detailNewsCtrl($location, $rootScope, SettingsNews, SettingsMenu, News) {/*, Settings, Cache*/
  var self = this;
  self.isVisible = false;
  self.shared = false;


  self.changeFavoriteValue = function(itemNew){

    News.changeFavoriteValue(itemNew, function(){
      alert("OK");
    }, function(){
      $rootScope.$broadcast("showError", 'error.add.favorite');
    });
  }


  self.changeVisibilitySummarizer = function(option){
    if(option===undefined){
      self.isVisible = !self.isVisible ;
    }else{
      self.isVisible = option;
    }
  }


  self.changeVisibilityShared = function(option){
    if(option===undefined){
      self.shared = !self.shared;
    }else{
      self.shared = option;
    }
    
      
  }
      
}