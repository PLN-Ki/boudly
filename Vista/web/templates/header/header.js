angular.module('boudly')

.controller('HeaderCtrl', headerCtrl);

function headerCtrl($scope, $rootScope, $location, $routeParams, UserService, SearcherNews, SettingsMenu) {
  	var self = this;
    self.isFullScreen = false;
  	self.q = '';

  	self.searchNow = function(){
  	  $location.path("/search").search({q: self.q});
   	 	self.q = '';
   	 	self.closeSearch();
  	};


    self.isVisibleButtonNews = function(){
      return $location.$$path == "/profile" || $location.$$path == "/news/read/later" || $location.$$path == "/search";
    }


    self.isVisibleButtonProfile = function(){
      return $location.$$path !== "/profile" && !this.isAnonymousUser();
    }


    self.isVisibleButtonFavs = function(){
      return $location.$$path !== "/news/read/later" && !this.isAnonymousUser();
    }


	// Top Search
    self.openSearch = function(){
        angular.element('#header').addClass('search-toggled');
        angular.element('#top-search-wrap').find('input').focus();
    }

   	self.closeSearch = function(){
   	    angular.element('#header').removeClass('search-toggled');
   	}
   	
    
    //Fullscreen View
    self.fullScreen = function() {
            //Launch
            function launchIntoFullscreen(element) {
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
            
            self.isFullScreen =  !self.isFullScreen;
    }


	self.notificationChange = function(item) {
	    item.enable = !item.enable;
	    SettingsMenu.changeSettingElement(item);
	    SearcherNews.restartVarsDefault();
	    $rootScope.$broadcast("reloadNews");
	};


  self.changeVisibilityToAll = function(visibility) {
      SettingsMenu.all().forEach(function(item) {
        item.enable = visibility;
        SettingsMenu.changeSettingElement(item);
      });

      $rootScope.$broadcast("reloadNews");
  };

	self.getSettings = function(){
	    return SettingsMenu.all();
	};

	self.init = function(){
    	SettingsMenu.loadSettings(function(){/*OK*/$rootScope.$broadcast("reloadNews");}, function(){/*error*/
          $rootScope.$broadcast("showError", 'error.generic');
      });
  };


  self.logOut = function(){
      UserService.logOut();
  };

  self.isAnonymousUser = function(){
      return UserService.isUserType(UserService.TYPE_ANONYMOUS);
  };


	$scope.$on("loadSetting", function () {
	    self.init();
	    
	});

}


