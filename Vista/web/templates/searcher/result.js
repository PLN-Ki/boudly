angular.module('boudly')

.controller('SearcherResultCtrl', searcherResultCtrl)

function searcherResultCtrl($location, $routeParams, SearcherNews, $rootScope) {
  var self = this;
  self.query = "";
  self.getNews = function(){
    return SearcherNews.all();
  };

  self.showLoading = function(){
    return SearcherNews.isLoading && SearcherNews.haveMoreData();
  };

  self.querySearched = function(){
    return SearcherNews.getQueryElement();
  };


  self.loadMore= function(){
      if(self.query===""){
        SearcherNews.setMoreData(false);
        return;
      }
      SearcherNews.moreNews(
        SearcherNews.getSearchNewsUrl(self.query), 

        function(data) {
          //Si quisieramos hacer algo en el ok podríamos pero no queremos :P
        }, 

        function(data) {
          $rootScope.$broadcast("showError", 'error.get.searcher');
        });
  };


  self.init = function(){
    self.query = $routeParams.q;
    SearcherNews.restartVarsDefault();
    self.loadMore();
  };
}


