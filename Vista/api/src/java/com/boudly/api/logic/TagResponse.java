/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.logic;

import com.boudly.orm.logic.Tag;
import com.boudly.orm.logic.UserTags;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TagResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String value;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String description;
    @JsonIgnore
    private Integer isVisible;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer enable;
    
    
    
    public static List<TagResponse> convert(List<Tag> hosts) {
        List<TagResponse> devolver = new ArrayList<>();
        for (Tag h : hosts) {
            devolver.add(new TagResponse(h));
        }
        return devolver;
    }
    
    /**
     *
     * @param hosts
     * @return
     */
    public static List<TagResponse> convertSinceUserTag(List<UserTags> hosts) {
        List<TagResponse> devolver = new ArrayList<>();
        for (UserTags h : hosts) {
            devolver.add(new TagResponse(h));
        }
        return devolver;
    }

    public TagResponse(UserTags host) {
        this(host.getIdTag(), host.getValue(), host.getDescription(), host.getIsVisible());
        this.enable = host.getUserVisible();
    }

    public TagResponse(Tag host) {
        this(host.getId(), host.getValue(), host.getDescription(), host.getIsVisible());
    }
    
    public TagResponse(String value, String description) {
        this.value = value;
        this.description = description;
    }
    
    public TagResponse(Integer id, String value, String description, Integer isVisible) {
        this(value, description);
        this.id = id;
        this.isVisible = isVisible;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the isVisible
     */
    public Integer getIsVisible() {
        return isVisible;
    }

    /**
     * @param isVisible the isVisible to set
     */
    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * @return the enable
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * @param enable the enable to set
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
}
