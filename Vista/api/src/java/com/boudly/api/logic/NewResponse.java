package com.boudly.api.logic;

import com.boudly.logic.serializer.JsonDateSerializer;
import com.boudly.orm.logic.New;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@XmlRootElement
public class NewResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String summarizer;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String url;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date moment;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private HostResponse host;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String title;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer sentiment;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer tag;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean showed;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean isFavorite;
    
    
    private List<PhotoResponse> photos;
    
    public NewResponse(){
    }
    
    
    public static List<NewResponse> convert(List<New> noticias){
        List<NewResponse> devolver = new ArrayList<>();
        for(New n:noticias){
            devolver.add(new NewResponse(n));
        }
        return devolver;
    }
    
    public NewResponse(New noticia){
        this.summarizer = noticia.getTextSummarizer();
        this.url = noticia.getUrl();
        this.moment = noticia.getMoment();
        this.host = new HostResponse(noticia.getHost());
        this.title = noticia.getTitle();
        this.id = noticia.getId();
        this.photos = PhotoResponse.convert(noticia.getPhotos());
        this.sentiment = noticia.getSentiment();
        this.tag = noticia.getTag();
        this.showed = noticia.getVisited();
        this.isFavorite = noticia.getIsFavorite()==null?false:noticia.getIsFavorite();
    }

    /**
     * @return the summarizer
     */
    public String getSummarizer() {
        return summarizer;
    }

    /**
     * @param summarizer the summarizer to set
     */
    public void setSummarizer(String summarizer) {
        this.summarizer = summarizer;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the moment
     */
    @JsonSerialize(using=JsonDateSerializer.class)
    public Date getMoment() {
        return moment;
    }

    /**
     * @param moment the moment to set
     */
    public void setMoment(Date moment) {
        this.moment = moment;
    }

    /**
     * @return the host
     */
    public HostResponse getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(HostResponse host) {
        this.host = host;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the photos
     */
    public List<PhotoResponse> getPhotos() {
        return photos;
    }

    /**
     * @param photos the photos to set
     */
    public void setPhotos(List<PhotoResponse> photos) {
        this.photos = photos;
    }

    /**
     * @return the sentiment
     */
    public Integer getSentiment() {
        return sentiment;
    }

    /**
     * @param sentiment the sentiment to set
     */
    public void setSentiment(Integer sentiment) {
        this.sentiment = sentiment;
    }

    /**
     * @return the tag
     */
    public Integer getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(Integer tag) {
        this.tag = tag;
    }

    /**
     * @return the showed
     */
    public Boolean getShowed() {
        return showed;
    }

    /**
     * @param showed the showed to set
     */
    public void setShowed(Boolean showed) {
        this.showed = showed;
    }

    /**
     * @return the isFavorite
     */
    public Boolean getIsFavorite() {
        return isFavorite;
    }

    /**
     * @param isFavorite the isFavorite to set
     */
    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }
    

    
}
