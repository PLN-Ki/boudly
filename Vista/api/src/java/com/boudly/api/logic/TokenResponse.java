/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.logic;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TokenResponse {
    private String token;
    
    public TokenResponse(){
        
    }
    
    public TokenResponse(String token){
        this.token = token;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
}
