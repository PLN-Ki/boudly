/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.exceptions.BoudlyApiException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.Suggestion;

public class SuggestionAction  extends Action{
    
    public static final String NOT_NEW = "-";

    /**
     * @param email
     * @param name
     * @param msg
     * @param idUser
     * @param type
     * @param idNew
     * @since 1.0
     * @return boolean with creation solution
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static boolean createSuggestion(String email, String name, String msg, String idUser, Integer type, String idNew) throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Crear sugerencia de tipo "+type+" por el usuario "+idUser);
        openDB();
        boolean saved = false;
        try {
            Suggestion sugestion = new Suggestion();
            sugestion.setEmail(email);
            sugestion.setName(name);
            sugestion.setMsg(msg);
            sugestion.setType(type);
            sugestion.setIdNew(idNew);
            //El usuario no es necesario por lo que si no lo recibimos lo guardamos sin el
            sugestion.setIdUser(idUser);
            saved = sugestion.saveNow();
        } finally {
            closeDB();

        }
        return saved;
    }

}
