/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.exceptions.BoudlyApiException;
import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.RuleResponse;
import com.boudly.exceptions.DBException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.Rule;
import com.boudly.orm.logic.UserHosts;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public class HostsAction extends Action{

    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<HostResponse> getHostList() throws BoudlyApiException {
        openDB();
        List<Host> hosts = new ArrayList<>();
        try {
            hosts = Host.getAll();
        } finally {
            closeDB();
        }
        return HostResponse.convert(hosts);
    }

    /**
     * @since 1.0
     * @param idUSer
     * @return list host to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<HostResponse> getHostList(String idUSer) throws BoudlyApiException {
        if (idUSer == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        BoudlyLog.getInstance().trace("Obtener lista de los host del usuario " + idUSer);
        openDB();
        List<Host> hostsUser = new ArrayList<>();
        try {
            hostsUser = UserHosts.searchHostDetail(idUSer);
        } catch (SQLException ex) {
            throw new DBException("Imposible obtain all host to "+idUSer+" : "+ex.getMessage());
        } finally {
            closeDB();
        }

        return HostResponse.convert(hostsUser);
    }

    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<HostResponse> getAllHostListWithRules() throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obtener la lista de los hosts con reglas");
        openDB();
        List<HostResponse> hosts = new ArrayList<>();
        try {
            hosts = HostResponse.convert(Host.getAll());

            for (HostResponse h : hosts) {
                h.setRules(RuleResponse.convert(Rule.searchByHost(h.getId())));
            }
        } finally {
            closeDB();
        }

        return hosts;
    }

    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<HostResponse> getVisibleHostListWithRules() throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obtener lista de hosts visibles");
        openDB();
        List<HostResponse> hosts = new ArrayList<>();
        try {
            hosts = HostResponse.convert(Host.getAllVisible());

            for (HostResponse h : hosts) {
                h.setRules(RuleResponse.convert(Rule.searchByHost(h.getId())));
            }
        } finally {
            closeDB();
        }

        return hosts;
    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     * @param idHost
     * @param visibility
     * @return boolean if is changed
     */
    public static boolean setVisibilityHost(String idHost, Integer visibility) throws BoudlyApiException{
        BoudlyLog.getInstance().trace("Añadir visibilidad " + visibility + " al idHost " + idHost);
        openDB();
        boolean isCorrect = false;
        try {
            Host h = Host.search(idHost);
            h.setVisible(visibility);
            isCorrect = h.saveNow();
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @return if modify host opetation was correct
     * @throws com.boudly.exceptions.BoudlyApiException
     * @see modify host
     * @since 1.0
     * @param host
     */
    public static boolean modifyHost(Host host) throws BoudlyApiException{
        BoudlyLog.getInstance().trace("Modificar el host " + host.getId());
        openDB();
        boolean isCorrect = false;
        try {
            host.setVisible(host.getVisible() == null ? Host.search(host.getId()).getVisible() : host.getVisible());
            isCorrect = host.saveNow();
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @return if add operation was correct
     * @throws com.boudly.exceptions.BoudlyApiException
     * @see If exist host modify It, if not exist create It
     * @since 1.0
     * @param host
     */
    public static boolean addHost(Host host) throws BoudlyApiException{
        BoudlyLog.getInstance().trace("Añadir el host " + host.getId());
        openDB();
        boolean isCorrect = false;
        try {
            isCorrect = host.saveNow();
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @see If exist rule modify It, if not exist create It
     * @since 1.0
     * @param rules
     */
    public static void addRules(List<Rule> rules) throws BoudlyApiException{
        BoudlyLog.getInstance().trace("Añadir " + rules.size() + " nuevas reglas");
        openDB();
        try {
            for (Rule rule : rules) {
                rule.saveNow();
            }
        } finally {
            closeDB();
        }
    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @see If exist rule modify It, if not exist create It
     * @since 1.0
     * @param rule
     * @return save it
     */
    public static boolean addRule(Rule rule) throws BoudlyApiException{
        BoudlyLog.getInstance().trace("Añadir regla");
        openDB();
        boolean isCorrect = false;
        try {
            isCorrect = rule.saveNow();
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     * @param idRule
     * @return boolean if is removed
     */
    public static boolean removeRule(Integer idRule) throws BoudlyApiException{
        BoudlyLog.getInstance().trace("Eliminar regla " + idRule);
        openDB();
        boolean isCorrect = false;
        try {
            Rule rule = Rule.search(idRule);
            isCorrect = rule.delete();
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @since 1.0
     * @param idHost
     * @return list rules to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<RuleResponse> getRulesList(String idHost) throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obtener la lista de las reglas del idHost " + idHost);
        openDB();
        List<Rule> rules =  new ArrayList<>();
        try {
            rules = Rule.searchByHost(idHost);
        } finally {
            closeDB();
        }
        return RuleResponse.convert(rules);
    }

    /**
     * @param idUser
     * @param idHost
     * @return wasSave
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     */
    public static boolean addHost(String idUser, String idHost) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The id_user parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);

        } else if (idHost == null) {
            throw new BoudlyApiException("The id_host parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);

        }
        BoudlyLog.getInstance().trace("Añadir marca de idHost " + idHost + " al usuario " + idUser);
        openDB();
        boolean isCorrect = false;
        try {
            isCorrect = new UserHosts(idUser, idHost, true).saveNow();
        } catch (Exception ex) {
            throw new BoudlyApiException(ex.getMessage(), BoudlyApiException.DEFAULT_CODE);
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @param idUser
     * @param idHost
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     */
    public static void removeHost(String idUser, String idHost) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The id_user parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);

        } else if (idHost == null) {
            throw new BoudlyApiException("The id_host parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);

        }
        BoudlyLog.getInstance().trace("Eliminar marca de id host " + idHost + " al usuario " + idUser);
        openDB();
        try {
            String query = UserHosts.FIELD_IDHOST + " like ? AND " + UserHosts.FIELD_IDUSER + " like ?";
            UserHosts.delete(query, idHost, idUser);
        } catch (Exception ex) {
            throw new BoudlyApiException(ex.getMessage(), BoudlyApiException.DEFAULT_CODE);
        } finally {
            closeDB();
        }
    }

}
