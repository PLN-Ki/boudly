/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.RuleResponse;
import com.boudly.api.logic.TagResponse;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.exceptions.NotModifyParamsException;
import com.boudly.jobs.logic.Url;
import com.boudly.jobs.noticiascomplejo.ObtainUrlsJob;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.Host;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletAdminAction  extends Action{

    /**
     * Send error msg
     *
     * @param request
     * @param response
     * @param msg
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public static void sendError(HttpServletRequest request, HttpServletResponse response, String msg) throws ServletException, IOException {
        BoudlyLog.getInstance().trace("Mostar el error: " + msg);
        HttpSession session = request.getSession();
        session.setAttribute("error", msg);
        response.sendRedirect("../error");
    }

    /**
     * Refresh or add necesary data from view
     *
     * @param request
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static void refreshDataAtributtes(HttpServletRequest request) throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obteniendo y añadiendo en sessión los hosts y los tags para su configuración");
        HttpSession session = request.getSession();
        List<HostResponse> hosts = HostsAction.getAllHostListWithRules();
        session.setAttribute("hosts", hosts);
        List<TagResponse> tags = TagsAction.getTagList();
        session.setAttribute("tags", tags);
    }
    /**
     *
     * @param option
     * @param host
     * @throws BoudlyApiException
     */
    public static void modifyHost(String option, HostResponse host) throws BoudlyApiException {
        BoudlyLog.getInstance().trace(option + " host " + host.getId());
        switch (option) {
            case "delete":
                if (!HostsAction.setVisibilityHost(host.getId(), 0)) {
                    throw new NotModifyParamsException("Delete host " + host.getId());
                }
                break;
            case "update":
                if (!HostsAction.modifyHost(host.host())) {
                    throw new NotModifyParamsException("Update host " + host.getId());
                }
                break;
            case "enable":
                if (!HostsAction.setVisibilityHost(host.getId(), 1)) {
                    throw new NotModifyParamsException("Enable host " + host.getId());
                }
                break;
            case "insert":
                if (!HostsAction.addHost(host.host())) {
                    throw new NotModifyParamsException("Insert host " + host.getId());
                }
                break;

        }

    }
    
    /**
     *
     * @param option
     * @param rule
     * @throws BoudlyApiException
     */
    public static void modifyRule(String option, RuleResponse rule) throws BoudlyApiException {
        BoudlyLog.getInstance().trace(option + " regla " + rule.getId() + " del host " + rule.getHost().getId());
        switch (option) {
            case "delete":
                if (!HostsAction.removeRule(rule.getId())) {
                    throw new NotModifyParamsException("Delete host " + rule.getId());
                }
                break;
            case "update":
                if (!HostsAction.addRule(rule.getRule())) {
                    throw new NotModifyParamsException("Update host " + rule.getId());
                }
                break;
            case "insert":
                if (!HostsAction.addRule(rule.getRule())) {
                    throw new NotModifyParamsException("Insert host " + rule.getId());
                }
                break;
        }

    }
    
    /**
     *
     * @param idHost 
     * @param request 
     * @throws BoudlyApiException
     */
    public static void testHosts(String idHost, HttpServletRequest request) throws BoudlyApiException {
        //Este metodo es un poco tochon pero hace todo lo que tendría que hacer una carga de urls en un solo método
        BoudlyLog.getInstance().trace("Realizar test de los hosts para el idHost: " + idHost);
        try {
            openDB();

            List<Host> noticias = new ArrayList<>();
            noticias.add(Host.search(idHost));

            ObtainUrlsJob urlsJob = new ObtainUrlsJob();
            urlsJob.execute((List<Object>) (Object) noticias);
            List<Url> result = (List<Url>) urlsJob.getResult();

            HttpSession session = request.getSession();
            if (result.isEmpty()) {
                BoudlyLog.getInstance().trace("No hay elementos para el id " + idHost);
                session.setAttribute("host", "No hay elementos");
            } else {
                BoudlyLog.getInstance().trace("Número de elementos para el id " + idHost + ": " + result.size());
                session.setAttribute("host", "Elementos de " + result.get(0).getHost().getTitle());
            }

            session.setAttribute("test", result);
        } catch (Exception ex) {
            throw new BoudlyApiException("Imposible realizar el test: " + ex.getMessage(), BoudlyApiException.DEFAULT_CODE);
        } finally {
            closeDB();
        }
    }
}
