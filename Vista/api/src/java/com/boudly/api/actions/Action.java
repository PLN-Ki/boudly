/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.exceptions.DBException;
import com.boudly.orm.config.DB;
import javax.naming.Context;
import javax.naming.InitialContext;


public class Action {
    public static void openDB() throws DBException{
        try {
            Context ctx = new InitialContext();
            DB.initWithJniName(ctx);
        } catch (Exception ex) {
            throw new DBException("Imposible init DB: "+ex.getMessage());
        }
    }
    
    public static void closeDB(){
        DB.close();
    }
    
    
}
