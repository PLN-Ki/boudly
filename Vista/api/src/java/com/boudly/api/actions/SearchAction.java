/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.exceptions.BoudlyApiException;
import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.NewResponse;
import com.boudly.api.logic.SearcherHostResponse;
import com.boudly.api.logic.SearcherNewResponse;
import com.boudly.logging.BoudlyLog;
import com.tekstly.linguist.Stemmer;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.New;
import com.tekstly.exceptions.LoadFileException;
import com.tekstly.exceptions.NotFoundLanguageException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SearchAction  extends Action{

    private static final Integer DEFAULT_INITIAL_POSITION = 0;
    private static final Integer DEFAULT_STEP = 20;

    private static String stemmer(String lang, String value) throws BoudlyApiException {
        try {
            if (value == null || value.isEmpty()) {
                throw new BoudlyApiException("The query parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
            }
            return Stemmer.getInstance(lang).getStemmer(value);
        } catch (NotFoundLanguageException | LoadFileException ex) {
            throw new BoudlyApiException("Lang not deffined", HttpServletResponse.SC_NOT_FOUND);
        }
    }

    /**
     * @param lang
     * @param value
     * @param from
     * @param to
     * @since 1.0
     * @return list news where appear the stemmer value in title or summarizer
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static SearcherNewResponse searchNew(String lang, String value, Integer from, Integer to) throws BoudlyApiException {
        if (from == null) {
            from = DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = DEFAULT_STEP;
        }
        BoudlyLog.getInstance().trace("Buscar noticia en " + lang + " valor: " + value);
        String query = New.FIELD_TITLE + " REGEXP ? OR " + New.FIELD_SUMMER + " REGEXP ?";

        String stemmerValue = SearchAction.stemmer(lang, value);
        String searchValue = "(^|[^aA-zZ])" + stemmerValue;

        openDB();
        List<New> news = new ArrayList<>();
        try {
            news = New.convertModel(New.find(query, searchValue, searchValue).limit(to).offset(from));

        } finally {
            closeDB();
        }

        return new SearcherNewResponse(stemmerValue, NewResponse.convert(news));
    }

    /**
     * @param value
     * @param from
     * @param to
     * @since 1.0
     * @return list news where appear the stemmer value in title
     * @throws com.boudly.exceptions.BoudlyApiException
     */
//    public static SearcherHostResponse searchHost(String lang, String value, Integer from, Integer to) throws AraApiException {
    public static SearcherHostResponse searchHost(String value, Integer from, Integer to) throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Buscar host con valor " + value);
        String query = Host.FIELD_TITLE + " REGEXP ? OR " + Host.FIELD_URL + " REGEXP ? AND " + Host.FIELD_VISIBLE + " = 1";
        String searchValue = ".*" + value.replaceAll(" ", ".*") + ".*";
        openDB();
        List<Host> hosts = new ArrayList<>();
        try {
            hosts = Host.convertModel(from != null && to != null ? Host.find(query, searchValue, searchValue).limit(to).offset(from) : Host.find(query, searchValue, searchValue));

        } finally {
            closeDB();
        }

        return new SearcherHostResponse(searchValue, HostResponse.convert(hosts));
    }

}
