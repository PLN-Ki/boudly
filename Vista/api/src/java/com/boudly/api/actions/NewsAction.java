/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.exceptions.BoudlyApiException;
import com.boudly.api.logic.NewResponse;
import com.boudly.exceptions.DBException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.New;
import static com.boudly.orm.logic.New.FIELD_MOMENT;
import static com.boudly.orm.logic.New.FIELD_TAG;
import static com.boudly.orm.logic.New.convertModel;
import com.boudly.orm.logic.UserNews;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.common.Util;

public class NewsAction extends Action {

    public static final Integer DEFAULT_INITIAL_POSITION = 0;
    public static final Integer DEFAULT_STEP = 20;

    /**
     * @since 1.0
     * @param idUser
     * @param idsHost
     * @param to
     * @param from
     * @return list news to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<NewResponse> getNewsByHost(String idUser, List<String> idsHost, Integer from, Integer to) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        if (from == null) {
            from = DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = DEFAULT_STEP;
        }
        BoudlyLog.getInstance().trace("Obtener de noticias para " + idsHost.size() + " hosts del idUser " + idUser);
        openDB();
        List<New> noticias = new ArrayList<>();
        try {
            noticias = UserNews.searchByHost(idUser, idsHost, to, from);
        } catch (SQLException ex) {
            throw new DBException("Imposible obtain news to host " + idsHost + " : " + ex.getMessage());
        } finally {
            closeDB();
        }

        return NewResponse.convert(noticias);
    }

    /**
     * @param idUser
     * @since 1.0
     * @param idsTag
     * @param to
     * @param from
     * @return list news to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<NewResponse> getNewsByTag(String idUser, List<Integer> idsTag, Integer from, Integer to) throws BoudlyApiException {
        if (from == null) {
            from = DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = DEFAULT_STEP;
        }
        BoudlyLog.getInstance().trace("Obtener noticias para " + idsTag.size() + " tags del idUser " + idUser);
        openDB();
        List<New> noticias = new ArrayList<>();
        try {
            noticias = UserNews.searchNewsByUserAndTag(idUser, idsTag, to, from);
        } catch (SQLException ex) {
            throw new DBException("Imposible obtain all news to tags " + idsTag + " : " + ex.getMessage());
        } finally {
            closeDB();
        }
        return NewResponse.convert(noticias);
    }

    /**
     * @since 1.0
     * @param idsTag
     * @param date
     * @param to
     * @param from
     * @return list news to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<NewResponse> getYesterdayNewsByTag(List<Integer> idsTag, java.sql.Date date, Integer from, Integer to) throws BoudlyApiException {
        if (from == null) {
            from = DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = DEFAULT_STEP;
        }
        BoudlyLog.getInstance().trace("Obtener noticias para " + idsTag.size()+" tags del dia "+date);
        openDB();
        List<New> noticias = new ArrayList<>();
        try {

            String query = FIELD_TAG + " IN ('" + Util.join(idsTag, "','") + "') AND " + FIELD_MOMENT + " = ?";
            LazyList<Model> find = New.find(query, date).limit(to).offset(from).orderBy(FIELD_MOMENT + " desc");

            noticias = convertModel(find);
        } finally {
            closeDB();
        }
        return NewResponse.convert(noticias);
    }

    /**
     * @since 1.0
     * @param idsTag
     * @param date
     * @return list of the 20 most important news of the previous day
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<NewResponse> getImportantNewsReads(List<Integer> idsTag, java.sql.Date date) throws BoudlyApiException {
        Integer from = DEFAULT_INITIAL_POSITION;
        Integer to = DEFAULT_STEP;

        BoudlyLog.getInstance().trace("Obtener las 20 noticias más importantes para " + idsTag.size() + " tags del dia: "+date);
        openDB();
        List<New> noticias = new ArrayList<>();
        try {
            String query = "select "
                    + " BDL_NEW.id, "
                    + " BDL_NEW.id_Host, "
                    + " BDL_NEW.url, "
                    + " BDL_NEW.summarizer, "
                    + " BDL_NEW.text, "
                    + " BDL_NEW.moment, "
                    + " BDL_NEW.title, "
                    + " BDL_NEW.type, "
                    + " BDL_NEW.tag, "
                    + " BDL_NEW.hash, "
                    + " BDL_NEW.sentiment "
                    + " from "
                    + " BDL_USER_VISITED_NEW left join BDL_NEW ON BDL_NEW.id = BDL_USER_VISITED_NEW.idNew "
                    + " where "
                    + " moment = ? "
                    + " AND BDL_NEW." + New.FIELD_TAG + " IN ('" + Util.join(idsTag, "','") + "')"
                    + " group by "
                    + " BDL_USER_VISITED_NEW.idNew "
                    + " order by "
                    + " count(idNew) DESC limit ?,?";

            //String previousDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis()-24*60*60*1000));
            noticias = UserNews.executeMyQuery(query, new Object[]{date, from, to});

        } catch (SQLException ex) {
            throw new DBException("Imposible obtain all news to tags " + idsTag + " : " + ex.getMessage());
        } finally {
            closeDB();
        }
        return NewResponse.convert(noticias);
    }

    /**
     * @since 1.0
     * @param idsTag
     * @return list of the 20 most important news of the previous day
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<NewResponse> getImportanYesterdaytNews(List<Integer> idsTag) throws BoudlyApiException {
        Integer from = DEFAULT_INITIAL_POSITION;
        Integer to = DEFAULT_STEP;
        
        List<NewResponse> news = new ArrayList<>();
        java.sql.Date previousDay = new java.sql.Date(new java.util.Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000).getTime());
        
        BoudlyLog.getInstance().trace("Obtener las 20 noticias más importante para " + idsTag.size() + " tags de día: "+previousDay);

        news.addAll(NewsAction.getImportantNewsReads(idsTag, previousDay));
        //Vamos a solicitar las noticias que nos faltan para llegar a las necesarias
        to = to<news.size()?to-news.size():to;
        news.addAll(NewsAction.getYesterdayNewsByTag(idsTag, previousDay, from, to));
        
        return news;
    }
    
    
                

    /**
     * @since 1.0
     * @param idNew
     * @return boolean if is removed
     */
    public static boolean removeNew(String idNew) throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Eliminar noticia " + idNew);
        openDB();
        boolean isCorrect = false;
        try {
            isCorrect = New.search(idNew).delete();
        } finally {
            closeDB();
        }
        return isCorrect;
    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     * @param idUser
     * @param news
     * @return boolean if is saved
     */
    public static boolean markRead(String idUser, NewResponse news) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        BoudlyLog.getInstance().trace("Marcar como leida noticia " + news.getId() + " para el usuario " + idUser);
        boolean saveIt = true;
        openDB();
        try {

            UserNews element = searchUserNew(idUser, news.getId());
            if (element == null) {
                UserNews userNews = new UserNews();
                userNews.setIdNew(news.getId());
                userNews.setIdUser(idUser);
                saveIt = userNews.saveNow() && saveIt;
            } else {
                throw new BoudlyApiException("The news was already marked", BoudlyApiException.PRECONDITION_FAILD);
            }
        } finally {
            closeDB();
        }

        return saveIt;

    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     * @param idUser
     * @param idNew
     * @return new by user
     */
    public static UserNews getUserNew(String idUser, String idNew) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        } else if (idNew == null) {
            throw new BoudlyApiException("The idNew parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        BoudlyLog.getInstance().trace("Obtener noticia " + idNew + " por el usuario " + idUser);
        openDB();
        UserNews element = null;
        try {
            NewsAction.searchUserNew(idUser, idNew);
        } finally {
            closeDB();
        }
        return element;
    }

    private static UserNews searchUserNew(String idUser, String idNew) throws BoudlyApiException {
        //No lleva conexión se llamará desde otros metodos con la conexión activa
        String query = UserNews.FIELD_IDNEW + "=? AND " + UserNews.FIELD_IDUSER + "=?";
        UserNews element = UserNews.convertModel(UserNews.findFirst(query, idNew, idUser));

        return element;
    }

    /**
     * @param favoriteValue
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     * @param idUser
     * @param idNew
     * @return boolean if id marked favorite
     */
    public static Boolean addFavorite(String idUser, String idNew, Integer favoriteValue) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        } else if (idNew == null) {
            throw new BoudlyApiException("The idNew parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        boolean saveIt = false;
        BoudlyLog.getInstance().trace("Añadir a favoritos con el valor " + favoriteValue + " del usuario " + idUser + " la noticia " + idNew);
        openDB();
        try {
            String update = UserNews.FIELD_FAVORITE + " = ?";
            String contidions = UserNews.FIELD_IDNEW + " = ? AND " + UserNews.FIELD_IDUSER + " = ?";
            saveIt = UserNews.update(update, contidions, favoriteValue, idNew, idUser) == 1;
            if (!saveIt) {//Si no ha sido leido no se podrá marcar como leído, por lo tanto lo marcamos como leído y como favorito
                new UserNews(idUser, idNew, UserNews.FAVORITE_VALUE).saveNow();
                saveIt = true;
            }
            closeDB();
        } catch (Exception ex) {
            throw new BoudlyApiException("Imposible create favorite mark in idNew " + idNew, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            closeDB();
        }

        return saveIt;
    }

    /**
     * @throws com.boudly.exceptions.BoudlyApiException
     * @since 1.0
     * @param idUser
     * @param from
     * @param to
     * @return new by user
     */
    public static List<NewResponse> getUserFavorites(String idUser, Integer from, Integer to) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        if (from == null) {
            from = NewsAction.DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = NewsAction.DEFAULT_STEP;
        }
        BoudlyLog.getInstance().trace("Obtener favoritos del idUSer " + idUser);
        openDB();
        List<NewResponse> elements = new ArrayList<>();
        try {
            elements = NewResponse.convert(UserNews.searchFavoriteNews(idUser, to, from));

        } catch (SQLException ex) {
            throw new DBException("Imposible obtain favorites to " + idUser + " : " + ex.getMessage());
        } finally {
            closeDB();
        }
        return elements;
    }

}
