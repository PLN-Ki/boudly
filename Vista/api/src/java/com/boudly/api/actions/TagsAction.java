/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.actions;

import com.boudly.api.logic.TagResponse;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.exceptions.DBException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.Tag;
import com.boudly.orm.logic.UserTags;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;

public class TagsAction  extends Action{

    /**
     * @since 1.0
     * @return list host visible to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<TagResponse> getTagVisibleList() throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obtener lista de tags visibles");
        openDB();
        List<Tag> tags = new ArrayList<>();
        try {
            tags = Tag.getAllVisible();

        } finally {
            closeDB();
        }

        return TagResponse.convert(tags);
    }

    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<TagResponse> getTagList() throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obtener lista de todos los tags");
        openDB();
        List<Tag> tags = new ArrayList<>();
        try {
            tags = Tag.getAll();

        } finally {
            closeDB();
        }

        return TagResponse.convert(tags);
    }

    /**
     * @since 1.0
     * @param idUser
     * @return list host visible by user to response
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static List<TagResponse> getTagVisibleList(String idUser) throws BoudlyApiException {
        BoudlyLog.getInstance().trace("Obtener lista de tags visibles para el usuario " + idUser);
        openDB();
        List<UserTags> tags = new ArrayList<>();
        try {
            tags = UserTags.getAllVisible(idUser);

        } catch (SQLException ex) {
            throw new DBException("Imposible obtain all visible tags to "+idUser+" : "+ex.getMessage());
        } finally {
            closeDB();
        }

        return TagResponse.convertSinceUserTag(tags);
    }

    /**
     * @since 1.0
     * @param idTag
     * @param showed
     * @param idUser
     * @throws com.boudly.exceptions.BoudlyApiException
     */
    public static void changeShowed(String idUser, Integer idTag, Integer showed) throws BoudlyApiException {
        if (idUser == null) {
            throw new BoudlyApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        } else if (idTag == null) {
            throw new BoudlyApiException("The idTag parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        } else if (showed == null || showed < 0 || showed > 1) {
            throw new BoudlyApiException("The idTag parameter is necessary with value 0 or 1", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        BoudlyLog.getInstance().trace("Cambiar visiibilidad " + showed + " del tag " + idTag + " para el usuario " + idUser);
        openDB();

        String update = UserTags.FIELD_USER_VISIBLE + "=?";
        String condition = UserTags.FIELD_ID_TAG + " = ? AND " + UserTags.FIEL_ID_USER + " = ?";

        int update1 = 1;
        try {
            UserTags tags = new UserTags(idTag, idUser, showed);
            tags.saveNow();
        } catch (Exception e) {
            update1 = UserTags.update(update, condition, showed, idTag, idUser);
        } finally {
            closeDB();
        }
        if (update1 != 1) {
            throw new BoudlyApiException("Imposible uptate showed to \"" + showed + "\"param in " + idTag, BoudlyApiException.DEFAULT_CODE);
        }
    }

}
