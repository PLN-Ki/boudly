package com.boudly.api.actions;

import com.boudly.api.logic.TokenResponse;
import com.boudly.crypto.Cipher;
import com.boudly.crypto.SHAHelper;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.exceptions.NotAuthorizedException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.Token;
import com.boudly.orm.logic.User;
import java.util.Date;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.javalite.activejdbc.DBException;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class UserAction  extends Action{

    public static void createUser(ServletContext ctx, String accesToken, User user) throws BoudlyApiException {
        if (accesToken == null) {
            throw new BoudlyApiException("The accesstoken parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }

        BoudlyLog.getInstance().trace("Crear usuario: " + user.getMail());

        String[] decodeAccessToken = Cipher.decodeAccessToken(accesToken, ctx);

        String password = SHAHelper.encodeSHA256(decodeAccessToken[0]);//En DB esta dos veces hasheada y una ya viene del cliente
        String mail = decodeAccessToken[1];
        openDB();

        user.setPassword(password);
        user.setMail(mail);
        user.setRole(User.ROLE_VALUE_USER);
        try {
            user.saveNow();
        } catch (DBException ex) {
            if (ex.getMessage().contains("Duplicate entry")) {
                throw new BoudlyApiException("mail in use", BoudlyApiException.BAD_REQUEST_CODE);
            } else {
                throw new BoudlyApiException("Imposible create user", BoudlyApiException.DEFAULT_CODE);
            }
        } finally {
            closeDB();
        }
    }

    public static TokenResponse login(ServletContext ctx, String accesToken) throws BoudlyApiException {
        if (accesToken == null) {
            throw new BoudlyApiException("The accesstoken parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        BoudlyLog.getInstance().trace("Login usuario: " + accesToken);
        openDB();
        String[] decodeAccessToken = Cipher.decodeAccessToken(accesToken, ctx);

        String password = SHAHelper.encodeSHA256(decodeAccessToken[0]);//En DB esta dos veces hasheada y una ya viene del cliente
        String mail = decodeAccessToken[1];

        User user = User.search(mail, password);
        if (user == null) {
            throw new NotAuthorizedException(accesToken);
        }
        TokenResponse accessToken = new TokenResponse();
        try {
            String token = generateToken(user.getIdUser());

            if (!saveToken(user.getIdUser(), token)) {
                throw new BoudlyApiException("Imposible generate token");
            }

            accessToken.setToken(Cipher.generateAuthToken(token, ctx));
        } catch (Exception ex) {
            throw new BoudlyApiException(ex.getMessage());
        } finally {
            closeDB();
        }
        return accessToken;

    }
    /* @param ctx
     * @param authToken
     * @param roles
     * @return idUSer
     *
     * @since 1.0.0
     */

    public static String authorized(ServletContext ctx, String authToken, String[] roles) throws BoudlyApiException {
        String idUser = null;
        if (authToken == null) {
            throw new BoudlyApiException("The auth_token parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }

        BoudlyLog.getInstance().trace("Chequear autorizacion " + authToken);
        boolean authorized = false;
        try {
            openDB();
            String token = Cipher.decodeAuthToken(authToken, ctx);
            Token searchByToken = Token.searchByToken(token);
            idUser = token.split("::")[0];

            if (searchByToken != null && searchByToken.getIdUser().equals(idUser)) {
                User user = User.searchById(idUser);
                for (String role : roles) {
                    if (user.getRole().equals(role)) {
                        authorized = true;
                        break;
                    }
                }
            }

        } catch (Exception ex) {
            throw new BoudlyApiException(ex.getMessage());

        } finally {
            closeDB();
        }

        if (!authorized) {
            throw new NotAuthorizedException("token invalid");
        }
        return idUser;
    }

    private static boolean saveToken(String idUsuario, String token) {
        //Este metodo hay que llamarlo desde otro que abra la consulta
        BoudlyLog.getInstance().trace("Guardar token " + token);
        Token tokenDB = new Token(idUsuario, null, new Date(), token);
        return tokenDB.saveNow();
    }

    private static String generateToken(String idUsuario) {
        BoudlyLog.getInstance().trace("Generar token para " + idUsuario);
        //Total de elementos max 100+19+5+9
        Random rand = new Random();
        Integer randomNumber = rand.nextInt((1000000000 - 1000) + 1) + 1000;
        long timeStamp = new java.util.Date().getTime();
        Boolean booleanRand = rand.nextBoolean();
        return idUsuario + "::" + timeStamp + booleanRand + randomNumber;
    }

}
