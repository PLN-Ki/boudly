package com.boudly.api;

import com.boudly.api.actions.SuggestionAction;
import com.boudly.api.actions.UserAction;
import com.boudly.api.logic.Erro;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.Suggestion;
import com.boudly.orm.logic.User;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.JSONP;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@Path("feedback")
public class WebServiceFacadeFeedback {

    @Context
    ServletContext ctx;


    /*
     * GET api/feedback/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        BoudlyLog.getInstance().trace("api/feedback/ping");
        return Response.ok("pong").build();

    }

    /*
     * GET api/feedback/jsonp/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("jsonp/ping")
    @Produces("application/javascript" + ";charset=UTF-8")
    @JSONP(callback = "callback", queryParam = "callback")
    public Response pingJSONP() {
        BoudlyLog.getInstance().trace("api/feedback/jsonp/ping");
        return Response.ok("pong").build();

    }

    /*
     * POST api/feedback/app/suggestion
     * 
     * WS to create suggestion
     *
     * @since 1.0.0
     */
    @POST
    @Path("app/suggestion")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response createAppSuggestion(@HeaderParam("user_token") String userToken, @FormParam("email") String email, @FormParam("name") String name, @FormParam("msg") String msg) {
        try {
            
            String idUser = userToken == null ? null : UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            boolean saveNow = SuggestionAction.createSuggestion(email, name, msg, idUser, Suggestion.TYPE_APP_SUGGESTION, SuggestionAction.NOT_NEW);
            return saveNow?Response.ok().build():Response.serverError().build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }

    /*
     * POST api/feedback/news/{id_new}/error
     * 
     * WS to create error in new
     *
     * @since 1.0.0
     */
    @POST
    @Path("news/{id_new}/error")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response createNewsError(@HeaderParam("user_token") String userToken, @PathParam("id_new") String idNew, @FormParam("email") String email, @FormParam("name") String name, @FormParam("msg") String msg) {
        try {
            String idUser = userToken == null ? null : UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            boolean saveNow = SuggestionAction.createSuggestion(email, name, msg, idUser, Suggestion.TYPE_NEW_ERROR, idNew);
            return saveNow?Response.ok().build():Response.serverError().build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }

    /*
     * POST api/feedback/host/suggestion
     * 
     * WS to create error in new
     *
     * @since 1.0.0
     */
    @POST
    @Path("host/suggestion")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response createHostSuggestion(@HeaderParam("user_token") String userToken, @FormParam("email") String email, @FormParam("name") String name, @FormParam("msg") String msg) {
        try {
            String idUser = UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            boolean saveNow = SuggestionAction.createSuggestion(email, name, msg, idUser, Suggestion.TYPE_HOST_SUGGESTION, SuggestionAction.NOT_NEW);
            return saveNow?Response.ok().build():Response.serverError().build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }

}
