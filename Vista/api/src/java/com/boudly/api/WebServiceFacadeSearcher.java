package com.boudly.api;

import com.boudly.api.actions.SearchAction;
import com.boudly.api.actions.UserAction;
import com.boudly.api.logic.Erro;
import com.boudly.api.logic.SearcherHostResponse;
import com.boudly.api.logic.SearcherNewResponse;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.User;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.JSONP;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@Path("search")
public class WebServiceFacadeSearcher extends org.glassfish.jersey.servlet.ServletContainer{

    @Context ServletContext ctx;
    

    /*
     * GET api/search/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        BoudlyLog.getInstance().trace("api/search/ping");
        return Response.ok("pong").build();

    }
    
    /*
     * GET api/search/jsonp/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/jsonp/ping")
    @Produces("application/javascript" + ";charset=UTF-8")
    @JSONP(callback = "callback", queryParam = "callback")
    public Response pingJSONP() {
        BoudlyLog.getInstance().trace("api/search/jsonp/ping");
        return Response.ok("pong").build();

    }
    
    
    /*
     * GET api/hosts
     * 
     * WS that search hosts
     *
     * @since 1.0.0
     */
    @GET
    @Path("/hosts")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response searchHosts(@HeaderParam("user_token") String userToken, @QueryParam("q") String q, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try{
            UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            SearcherHostResponse hosts = SearchAction.searchHost(q, from, to);
            return Response.ok(hosts).build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }

    
    /*
     * GET api/{lang}/news
     * 
     * WS that search news
     *
     * @since 1.0.0
     */
    @GET
    @Path("/{lang}/news")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response searchNews(@PathParam("lang") String lang, @QueryParam("q") String q, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try{
            SearcherNewResponse news = SearchAction.searchNew(lang, q, from, to);
            return Response.ok(news).build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }


}
