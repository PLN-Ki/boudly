package com.boudly.api;

import com.boudly.api.actions.NewsAction;
import com.boudly.api.actions.TagsAction;
import com.boudly.api.actions.UserAction;
import com.boudly.api.logic.Erro;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.api.logic.NewResponse;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.JSONP;
import com.boudly.api.logic.TagResponse;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.User;
import java.util.ArrayList;
import javax.ws.rs.HeaderParam;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@Path("")
public class WebServiceFacadeNews {

    @Context ServletContext ctx;
    

    /*
     * GET api/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        BoudlyLog.getInstance().trace("api/ping");
        return Response.ok("pong").build();

    }
    
    /*
     * GET api/jsonp/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/jsonp/ping")
    @Produces("application/javascript" + ";charset=UTF-8")
    @JSONP(callback = "callback", queryParam = "callback")
    public Response pingJSONP() {
        BoudlyLog.getInstance().trace("api/jsonp/ping");
        return Response.ok("pong").build();

    }
    
        
    /*
     * GET api/tags
     * 
     * WS that get list tags
     *
     * @since 1.0.0
     */
    @GET
    @Path("/tags")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getTags(@HeaderParam("user_token") String userToken) {
        try {
            String idUSer = userToken==null?null:UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            List<TagResponse> news = idUSer==null?TagsAction.getTagVisibleList():TagsAction.getTagVisibleList(idUSer);
            return Response.ok(news).build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    /*
     * GET api/tags/news
     * 
     * WS that get list news
     *
     * @since 1.0.0
     */
    @GET
    @Path("/tags/news")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getNewsByTag(@HeaderParam("user_token") String userToken, @QueryParam("ids") List<Integer> ids, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try {
            String idUSer = UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            List<NewResponse> news = NewsAction.getNewsByTag(idUSer, ids, from, to);
            return Response.ok(news).build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    
    /*
     * GET api/tags/news
     * 
     * WS that get list news
     *
     * @since 1.0.0
     */
    @GET
    @Path("/tags/news/important")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getIMportantNewsByTag(@QueryParam("ids") List<Integer> ids) {
        try {
            List<NewResponse> news = new ArrayList<>();
            news = NewsAction.getImportanYesterdaytNews(ids);
            return Response.ok(news).build();
        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Código: "+ex.getCode()+" "+ex.getMessage());
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    



}
