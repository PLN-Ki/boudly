package com.boudly.api.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */

public class CorsResponseFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse responsehttp = ((HttpServletResponse)response);
        
        responsehttp.addHeader("Access-Control-Allow-Origin", "*");
        responsehttp.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, HEAD");
        responsehttp.addHeader("Access-Control-Allow-Headers","Content-Type, access_token, user_token");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
