package com.boudly.logging;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class BoudlyLog {
    
    private final Logger logger = Logger.getLogger(BoudlyLog.class);
    private static BoudlyLog instance;
    
    public BoudlyLog(){
        PropertyConfigurator.configure("log4j.properties");
    }
    
    public static Logger getInstance(){
        if(instance==null){
            instance =  new BoudlyLog();
        }
        
        return instance.logger;
    }
    
}
