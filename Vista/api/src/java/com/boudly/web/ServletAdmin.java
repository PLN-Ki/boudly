/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.web;

import com.boudly.api.actions.ServletAdminAction;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.RuleResponse;
import com.boudly.exceptions.NotModifyParamsException;
import com.boudly.logging.BoudlyLog;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ServletAdmin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequestGET(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BoudlyLog.getInstance().trace("Solicitud GET en la web de admin: "+request.getServletPath());
        try {
            switch (request.getServletPath()) {
                case "/admin":
                    ServletAdminAction.refreshDataAtributtes(request);
                    request.getRequestDispatcher("/adminfiles/index.jsp").forward(request, response);
                    break;
                case "/test/host":
                    if(request.getParameter("id")==null){
            BoudlyLog.getInstance().error("Solicitud del test de un host sin introducir el id");
                        throw new BoudlyApiException("host param is necesary");
                    }
                    ServletAdminAction.testHosts(request.getParameter("id"), request);
                    request.getRequestDispatcher("/adminfiles/testout.jsp").forward(request, response);
                    break;
            }

        } catch (BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Error en la web admin: "+ex.getMessage());
            ServletAdminAction.sendError(request, response, ex.getMessage());
        }

    }

    protected void processRequestPOST(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BoudlyLog.getInstance().trace("Solicitud POST en la web de admin: "+request.getServletPath());
        try {
            switch (request.getServletPath()) {
                case "/modify/host":
                    String idHost = null;
                    if(!request.getParameter("hostId").isEmpty()){
                        idHost = request.getParameter("hostId");
                    }
                    HostResponse host = new HostResponse(request.getParameter("hostUrl"), request.getParameter("hostTitle"), idHost);
                    boolean haveTag = (request.getParameter("tagDefault")!=null) && (Integer.parseInt(request.getParameter("tagDefault"))>-1);
                    host.setDefaultTag(haveTag?Integer.parseInt(request.getParameter("tagDefault")):null);
                    host.setOrder(Integer.parseInt(request.getParameter("orderHost")));
                    ServletAdminAction.modifyHost(request.getParameter("actionExecute"), host);
                    break;
                case "/modify/rule":
                    Integer idRule = null;
                    if(request.getParameter("ruleId")!=null){
                        idRule = Integer.parseInt(request.getParameter("ruleId"));
                    }
                    RuleResponse rule = new RuleResponse(idRule, request.getParameter("ruleValue"), request.getParameter("ruleDescription"),Integer.parseInt(request.getParameter("ruleType")),request.getParameter("hostRuleId"));
//                    ServletAdminAction.modifyHost(request.getParameter("actionExecute"), host);
                    ServletAdminAction.modifyRule(request.getParameter("actionExecute"), rule);
                    break;
                default:
            }
            response.sendRedirect("../admin");

        } catch (NumberFormatException | IOException | BoudlyApiException ex) {
            BoudlyLog.getInstance().error("Error en la web admin: "+ex.getMessage());
            ServletAdminAction.sendError(request, response, ex.getMessage());
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestGET(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestPOST(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "V1.0 ARA Config";
    }// </editor-fold>

}
