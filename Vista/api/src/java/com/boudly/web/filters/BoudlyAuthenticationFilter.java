/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.web.filters;

import com.boudly.api.actions.UserAction;
import com.boudly.crypto.SHAHelper;
import com.boudly.exceptions.BoudlyApiException;
import com.boudly.logging.BoudlyLog;
import com.boudly.orm.logic.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;
import org.apache.tomcat.util.codec.binary.Base64;

public class BoudlyAuthenticationFilter implements Filter {
    private String realm = "Protected";
    
    public static final String LOGIN_HEADER_PARAM = "Login-Authenticate-boudly";
    public static final String AUTHORIZATION_HEADER_PARAM = "Authorization";
    
    private ServletContext servletContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        servletContext = filterConfig.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String authHeader = request.getHeader(AUTHORIZATION_HEADER_PARAM);
        BoudlyLog.getInstance().trace("Entrada en zona admin de "+authHeader);
        if (authHeader != null) {
            StringTokenizer st = new StringTokenizer(authHeader);
            if (st.hasMoreTokens()) {
                String basic = st.nextToken();

                if (basic.equalsIgnoreCase("Basic")) {
                    try {

                        String credentials = new String(Base64.decodeBase64(st.nextToken()), "UTF-8");
                        
                        int p = credentials.indexOf(":");
                        if (p != -1) {
                            String _username = credentials.substring(0, p).trim();
                            String _password = SHAHelper.encodeSHA256(credentials.substring(p + 1).trim());
                                    
                            String login = _password + "::" + _username;
                            String accesstoken = Base64.encodeBase64String(login.getBytes());
                            String token = UserAction.login(servletContext, accesstoken).getToken();
                            UserAction.authorized(servletContext, token, new String[]{User.ROLE_VALUE_ADMIN});

                            filterChain.doFilter(servletRequest, servletResponse);
                        } else {
                            BoudlyLog.getInstance().error("No autorizado en zona admin"+authHeader);
                            unauthorized(response, "Invalid authentication token");
                        }

                    } catch (UnsupportedEncodingException e) {
                        BoudlyLog.getInstance().error("Couldn't retrieve authentication"+e.getMessage());
                        throw new Error("Couldn't retrieve authentication", e);
                    } catch (BoudlyApiException ex) {
                        BoudlyLog.getInstance().error("No autorizado en zona admin"+authHeader);
                        unauthorized(response,  ex.getMessage());
                    }
                }
            }
        } else {
            unauthorized(response);
        }
    }

    @Override
    public void destroy() {
    }

    private void unauthorized(HttpServletResponse response, String message) throws IOException {
        response.setHeader("WWW-Authenticate", "Basic realm=\"" + realm + "\"");
        response.sendError(401, message);
    }

    private void unauthorized(HttpServletResponse response) throws IOException {
        unauthorized(response, "Unauthorized");
    }

}
