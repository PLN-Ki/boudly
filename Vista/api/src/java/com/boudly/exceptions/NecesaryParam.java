package com.boudly.exceptions;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class NecesaryParam extends BoudlyApiException{
    public static final Integer NECESARY_PARAM_CODE = 412;

    public NecesaryParam(String mensage) {
        super(mensage);
    }
    
    @Override
    public Integer getCode(){
        return NECESARY_PARAM_CODE;
    }
    
}
