/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.exceptions;

public class DBException extends BoudlyApiException {

    /**
     * @param msg exception msg
     *
     */
    public DBException(String msg) {
        super(msg);

    }

}
