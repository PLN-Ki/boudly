package com.boudly.exceptions;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class NotAuthorizedException extends BoudlyApiException {
    private static final Integer NOT_FOUND_CODE = 404;

    public NotAuthorizedException(String token) {
        super("Not Authorized: " + token, NOT_FOUND_CODE);
    }

}
