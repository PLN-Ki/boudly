package com.boudly.exceptions;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class BoudlyApiException extends Exception{
    public static final Integer BAD_REQUEST_CODE = 400;
    public static final Integer PRECONDITION_FAILD = 412;
    public static final Integer DEFAULT_CODE = 500;
    private Integer code;
    
    public BoudlyApiException(String mensage) {
        super(mensage);
        this.code = DEFAULT_CODE;
    }
    
    public BoudlyApiException(String mensage, Integer code) {
        this(mensage);
        this.code = code;
    }
    
    public Integer getCode(){
        return this.code;
    }
}
