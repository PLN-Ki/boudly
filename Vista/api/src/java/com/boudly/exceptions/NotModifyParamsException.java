/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.exceptions;

public class NotModifyParamsException extends BoudlyApiException {
    private String param;
    public NotModifyParamsException(String param){
        super("Imposible modify param: "+param);
    }
    

}
