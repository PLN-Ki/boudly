/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.crypto.logic;

import java.math.BigInteger;

public class RSAKeyValue {
    
    private static BigInteger modulus;
    private static BigInteger exponent;
    
    public RSAKeyValue(BigInteger modulus, BigInteger exponent){
        this.modulus = modulus;
        this.exponent = exponent;
    }

    /**
     * @return the modulus
     */
    public static BigInteger getModulus() {
        return modulus;
    }

    /**
     * @param aModulus the modulus to set
     */
    public static void setModulus(BigInteger aModulus) {
        modulus = aModulus;
    }

    /**
     * @return the exponent
     */
    public static BigInteger getExponent() {
        return exponent;
    }

    /**
     * @param aExponent the exponent to set
     */
    public static void setExponent(BigInteger aExponent) {
        exponent = aExponent;
    }
}
