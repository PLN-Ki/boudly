<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ARA Config</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="adminfiles/css/style.css">

    </head>
    <body>
        <h1><c:out value="Configuración ARA"/></h1>
        
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-success">
                <div class="panel-heading" role="tab" id="headingNew">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNew" aria-expanded="true" aria-controls="collapseNew">
                            Insertar nuevo host
                        </a>
                    </h4>
                </div>
                <div id="collapseNew" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingNew">
                    <div class="panel-body">
                        <form action="modify/host" method="post" id="hostModify">
                            <!--inputs-->
                            <input title="Titulo para el host. Este aparecerá en la app." placeholder="Titulo para el host. Este aparecerá en la app." type="text" class="form-control"  name="hostTitle" value="" autocomplete="off"> 
                            <input title="Url a partir de la cual analizará el host." placeholder="Url a partir de la cual analizará el host." type="text" class="form-control margin-top"  name="hostUrl" value=""  autocomplete="off"> 
                            <select title="Tag por defecto en la clasificación. Si tiene no clasificará automáticamente." class="form-control  margin-top"  name="tagDefault">
                                <option value="-1">Sin determinar</option>
                                
                                <c:forEach var="tag" items="${sessionScope.tags}">
                                    <option value="<c:out value="${tag.id}"/>"><c:out value="${tag.value}"/></option>
                                </c:forEach>
                            </select>
                            <input title="Orden de aparición y ejecución." type="number" class="form-control margin-top"  name="orderHost" min="1" max="10" value ="1"> 
                            <input type="hidden" name="hostId" value="">
                            <input type="hidden" id="actionExecute" name="actionExecute" value="">
                            <!--buttons-->

                            <button type="button" class="btn btn-default pull-right margin-left margin-top" value="insert" onclick="sendHostOption('', 'insert')">Crear Host</button>
                        </form>
                    </div>
                </div>
            </div>
            
            <!--List Hosts-->
            <c:forEach var="host" items="${sessionScope.hosts}">
                <div class="panel panel-<c:choose><c:when test="${host.visible==0}">default</c:when><c:otherwise>info</c:otherwise></c:choose>">
                    <div class="panel-heading" role="tab" id="heading<c:out value="${host.id}"/>">
                        <h4 class="panel-title">
                            <form action="modify/host" method="post" id="hostModify<c:out value="${host.id}"/>">
                                <!--inputs-->
                                <input title="Titulo para el host. Este aparecerá en la app." type="text" class="form-control"  name="hostTitle" value="<c:out value="${host.value}"/>" autocomplete="off" <c:if test="${host.visible==0}">disabled</c:if>> 
                                <input title="Url a partir de la cual analizará el host." type="text" type="text" class="form-control margin-top"  name="hostUrl" value="<c:out value="${host.url}"/>"  autocomplete="off" <c:if test="${host.visible==0}">disabled</c:if>> 
                                <select title="Tag por defecto en la clasificación. Si tiene no clasificará automáticamente." class="form-control  margin-top"  name="tagDefault">
                                    <option value="-1">Sin determinar</option>

                                    <c:forEach var="tag" items="${sessionScope.tags}">
                                        <option value="<c:out value="${tag.id}"/>" <c:if test="${tag.id==host.defaultTag}">selected</c:if>><c:out value="${tag.value}"/></option>
                                    </c:forEach>
                                </select>
                                <input title="Orden de aparición y ejecución." type="number" class="form-control margin-top"  name="orderHost" autocomplete="off" min="1" max="10" value="<c:out value="${host.order}"/>"> 
                                <input type="hidden" name="hostId" value="<c:out value="${host.id}"/>">
                                <input type="hidden" id="actionExecute" name="actionExecute" value="">
                                <!--buttons-->
                                <c:choose>
                                    <c:when test="${host.visible==1}">
                                        <button type="button" class="btn btn-danger pull-right margin-left margin-top" value="delete" onclick="sendHostOption(<c:out value="${host.id}"/>, 'delete')">Desactivar Host</button>
                                        <button type="button" class="btn btn-warning pull-right margin-left margin-top" value="update" onclick="sendHostOption(<c:out value="${host.id}"/>, 'update')">Actualizar Host</button>
                                        <button type="button" class="btn btn-success pull-right margin-left margin-top" value="enable" onclick="showTest(<c:out value="${host.id}"/>)">Test</button>
                                        <button type="button" class="btn btn-success pull-right margin-left margin-top" value="enable" data-toggle="collapse" data-parent="#accordion" href="#collapse<c:out value="${host.id}"/>" aria-expanded="false" aria-controls="collapse<c:out value="${host.id}"/>">Ver Reglas</button>
                                
                                    </c:when>
                                    <c:otherwise>
                                        <button type="button" class="btn btn-success pull-right margin-left margin-top" value="enable" onclick="sendHostOption(<c:out value="${host.id}"/>, 'enable')">Activar Host</button>

                                    </c:otherwise>
                                </c:choose>
                                
                                
                            </form>
                        </h4>
                        <div class="clearfix margin-bottom"></div>
                    </div>
                    <div id="collapse<c:out value="${host.id}"/>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<c:out value="${host.id}"/>">
                        <div class="panel-body">
                             <div class="panel-body">

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Regla</th>
                                            <th>Descripción</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="rule" items="${host.rules}">
                                            <tr>
                                        <form action="modify/rule" method="post" id="ruleModify<c:out value="${rule.id}"/>">
                                            <td>
                                                <select name="ruleType" class="margin-top">
                                                    <option value="0" <c:if test="${rule.type==0}">selected</c:if> >Contenido</option>
                                                    <option value="1" <c:if test="${rule.type==1}">selected</c:if>>URL</option>
                                                    <option value="2" <c:if test="${rule.type==2}">selected</c:if>>Foto</option>
                                                    </select>

                                                </td>
                                                <td>
                                                    <input type="text" class="form-control margin-top"  name="ruleValue" value="<c:out value="${rule.value}"/>" autocomplete="off"> 
                                            </td>
                                            <td>
                                                <input type="text" class="form-control margin-top"  name="ruleDescription" value="<c:out value="${rule.description}"/>" autocomplete="off"> 
                                            </td>
                                            <td>
                                                <input type="hidden" name="ruleId" value="<c:out value="${rule.id}"/>">

                                                <button type="button" class="btn btn-danger pull-right margin-left margin-top margin-bottom" value="delete" onclick="sendRuleOption(<c:out value="${rule.id}"/>, 'delete')">Eliminar Regla</button>
                                                <button type="button" class="btn btn-warning pull-right margin-left margin-top margin-bottom" value="update" onclick="sendRuleOption(<c:out value="${rule.id}"/>, 'update')">Actualizar Regla</button>

                                                <input type="hidden" id="hostRuleId" name="hostRuleId" value="<c:out value="${host.id}"/>">
                                                <input type="hidden" id="actionExecute" name="actionExecute" value="">
                                            </td>

                                        </form>
                                        </tr>
                                    </c:forEach>
                                    <tr>
                                    <form action="modify/rule" method="post" id="ruleModify_newRuleInHost<c:out value="${host.id}"/>">
                                        <td>
                                            <select name="ruleType" class="margin-top">
                                                <option value="0">Contenido</option>
                                                <option value="1">URL</option>
                                                <option value="2">Foto</option>
                                            </select>

                                        </td>
                                        <td>
                                            <input type="text" class="form-control margin-top"  name="ruleValue" value="" autocomplete="off"> 
                                        </td>
                                        <td>
                                            <input type="text" class="form-control margin-top"  name="ruleDescription" value="" autocomplete="off"> 
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-default pull-right margin-left margin-top" value="insert" onclick="sendRuleOption('_newRuleInHost<c:out value="${host.id}"/>','insert')">Crear Regla</button>
                                            <input type="hidden" id="hostRuleId" name="hostRuleId" value="<c:out value="${host.id}"/>">
                                            <input type="hidden" id="actionExecute" name="actionExecute" value="">
                                        </td>
                                    </form>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <!--End List Hosts-->
        </div>

    </body>

    <script>

        function getFormNameModifyHost(id){
            return "hostModify" + id
        }

        function sendHostOption(id, option){
            document.getElementById(getFormNameModifyHost(id)).actionExecute.value = option;
            document.getElementById(getFormNameModifyHost(id)).submit();
        }
        
        function getFormNameModifyRule(id){
            return "ruleModify" + id
        }

        function sendRuleOption(id, option){
            document.getElementById(getFormNameModifyRule(id)).actionExecute.value = option;
            document.getElementById(getFormNameModifyRule(id)).submit();
        }
        
        function showTest(id){
            window.open("test/host?id="+id);
        }

    </script>
</html>
