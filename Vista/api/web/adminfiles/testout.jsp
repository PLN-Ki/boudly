<%-- 
    Document   : testout
    Created on : 31-ago-2015, 14:22:56
    Author     : fblanco
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Result</title>
    </head>
    <body>
        <h1><c:out value="${sessionScope.host}"/></h1>
        <ul>
            <c:forEach var="url" items="${sessionScope.test}">
                <li><a href="<c:out value="${url.element}"/>" target="_blank"><c:out value="${url.element}"/></a></li>
            </c:forEach>
        </ul>
        
    </body>
</html>
