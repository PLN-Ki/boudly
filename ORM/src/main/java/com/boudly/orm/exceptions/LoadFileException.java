/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */

package com.boudly.orm.exceptions;

public class LoadFileException extends Exception{
    public static final Integer NOT_FOUND_FILE = 404;
    public static final Integer IMPOSIBLE_READ_FILE = 405;
    public static final Integer MALFORMED_FILE = 406;
    private Integer errorCode;
    
    /**
     * @param msg exception msg
     * @param code exception code
     */
    public LoadFileException(String msg, Integer code){
        super(msg);
        this.errorCode = code;
    }
    
    /**
     * @return errorCode error code
     */
    public Integer getErrorCode(){
        return this.errorCode;
    }
}
