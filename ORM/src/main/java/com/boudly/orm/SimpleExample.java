/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm;

import com.boudly.orm.config.DB;
import com.boudly.orm.exceptions.LoadFileException;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.New;
import com.boudly.orm.logic.Photo;
import com.boudly.orm.logic.Rule;
import com.boudly.orm.logic.User;
import com.boudly.orm.logic.UserHosts;
import com.boudly.orm.logic.UserNews;
import com.boudly.orm.logic.UserTags;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.javalite.activejdbc.Base;

public class SimpleExample {

    public static void main(String[] args) throws LoadFileException {
        try {
//            DB.init();
            
//            UserNews userNews = new UserNews(1, 1);
//            userNews.saveNow();
//            Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3309/ara2db", "root", "root");
//           User u = new User();
//           u.setName("fernando");
//           u.setMail("fernando@argalladas.com");
//           u.saveNow();
//           
           
           
            DB.close();
        } catch (org.javalite.activejdbc.DBException dbe) {
            System.err.println(dbe.getMessage());
        }
    }

//
//    private static void selectEmployee() {
//        Employees e = Employees.findFirst("first_name = ?", "John");
//        System.out.println(e.toString());
//    }
//
//    private static void updateEmployee() {
//        Employees e = Employees.findFirst("first_name = ?", "John");
//        e.set("last_name", "Steinbeck").saveIt();
//    }
//
//    private static void deleteEmployee() {
//        Employees e = Employees.findFirst("first_name = ?", "John");
//        e.delete();
//    }
//
//    private static void deleteAllEmployees() {
//            Employees.deleteAll();
//    }
//
//    private static void selectAllEmployees() {
//        System.out.println("Employees list: " + Employees.findAll());
//    }
}
