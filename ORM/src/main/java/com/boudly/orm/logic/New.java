/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import com.boudly.orm.helper.MyUUID;
import static com.boudly.orm.helper.SHA.encodeSHA256;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Many2Many;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@Table("BDL_NEW")
public class New extends Model {
    public static final String FIELD_URL = "url";
    private String url;
    public static final String FIELD_ID = "id";
    private String id;
    public static final String FIELD_SUMMER = "summarizer";
    private String summarizer;
    public static final String FIELD_COMPLETE = "text";
    private String complete;
    
    public static final String FIELD_MOMENT = "moment";
    private Date moment;
    public static final String FIELD_HOST = "id_Host";
    private Host host;
    public static final String FIELD_TITLE = "title";
    private String title;
    public static final String FIELD_LABEL_TYPE = "label_type";
    private String labelType;
    public static final String FIELD_TAG = "tag";
    private Integer tag;
    public static final String FIELD_SENTIMENT = "sentiment";
    private Integer sentiment;
    public static final String FIELD_HASH = "hash";
    private String hash;
    public static final String FIELD_VISITED = "visited";//Este método no es de la tabla pero se realiza con una consulta externa
    private Boolean visited;
    private Boolean isFavorite;
    
    
    private List<Photo> photos;

    public New(String complete, String summarizer, String url, Host host, String title) {
        this();
        this.saveTextAndHash(complete);
        this.summarizer = summarizer;
        this.url = url;
        this.moment = new Date();
        this.host = host;
        this.title = title;
    }

    public New(String complete, String summarizer, String url, Host host, Date moment, String title) {
        this();
        this.saveTextAndHash(complete);
        this.summarizer = summarizer;
        this.url = url;
        this.moment = moment;
        this.host = host!=null?host: new Host();
        this.title = title;
    }
    public New(String complete, String summarizer, String url, Host host, Date moment, String title, Integer idTag, List<Photo> photos) {
        this(complete, summarizer, url, host, moment, title);
        this.photos = photos!=null?photos: new ArrayList<Photo>();
        this.tag = idTag;
    }
    
    
    public New(String complete, String summarizer, String url, Host host, String title, List<Photo> photos) {
        this(complete, summarizer, url, host, title);
        this.photos = photos!=null?photos: new ArrayList<Photo>();
    }

    public New(String complete, String summarizer, String url, Host host, Date moment, String title, String id, Integer idTag, List<Photo> photos, Integer sentiment) {
        this(complete, summarizer, url, host, moment, title, idTag, photos);
        this.id = id;
        this.sentiment = sentiment;
    }


    public New(String summarizer, String url, Host host, Date moment, String title, String id, Integer idTag, List<Photo> photos, Integer sentiment) {
        this();       
        this.summarizer = summarizer;
        this.url = url;
        this.moment = moment;
        this.host = host!=null?host: new Host();
        this.title = title;
        this.id = id;
        this.tag = idTag;
        this.photos = photos!=null?photos: new ArrayList<Photo>();
        this.sentiment = sentiment;
    }
    
    
    public New(String hash, String complete, String summarizer, String url, Host host, Date moment, String title, String id, Integer idTag, List<Photo> photos, Integer sentiment) {
        this(summarizer, url, host, moment, title, id, idTag, photos, sentiment);
        this.complete =  complete;
        this.hash =  hash;
    }
    
    
    public New(String hash, String complete, String summarizer, String url, Host host, Date moment, String title, String id, Integer idTag, List<Photo> photos, Integer sentiment, Boolean visited) {
        this(summarizer, url, host, moment, title, id, idTag, photos, sentiment);
        this.complete =  complete;
        this.hash =  hash;
        this.visited = visited;
    }
    
    
    public New(String hash, String complete, String summarizer, String url, Host host, Date moment, String title, String id, Integer idTag, List<Photo> photos, Integer sentiment, Boolean visited, Boolean isFavorite) {
        this(summarizer, url, host, moment, title, id, idTag, photos, sentiment);
        this.complete =  complete;
        this.hash =  hash;
        this.visited = visited;
        this.isFavorite = isFavorite;
    }

    public New() {
        this.photos = new ArrayList<>();
        this.visited = false;
        this.isFavorite = false;
    }
    
    public boolean saveTextAndHash(String text){
        try {
            this.setComplete(text);
            this.setHash(encodeSHA256(text));
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public String getTextSummarizer() {
        return this.getSummarizer();
    }

    public String getTextComplete() {
        return this.getComplete();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the summarizer
     */
    public String getSummarizer() {
        return summarizer;
    }

    /**
     * @param summarizer the summarizer to set
     */
    public void setSummarizer(String summarizer) {
        this.summarizer = summarizer;
    }

    /**
     * @return the complete
     */
    public String getComplete() {
        return complete;
    }

    /**
     * @param complete the complete to set
     */
    public void setComplete(String complete) {
        this.complete = complete;
    }

    /**
     * @return the read moment
     */
    public Date getMoment() {
        return moment;
    }

    /**
     * @param moment the readMoment to set
     */
    public void setMoment(Date moment) {
        this.moment = moment;
    }

    /**
     * @return the host
     */
    public Host getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(Host host) {
        this.host = host;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the labelType
     */
    public String getLabelType() {
        return labelType;
    }

    /**
     * @param labelType the labelType to set
     */
    public void setLabelType(String labelType) {
        this.labelType = labelType;
    }

    /**
     * @return the photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * @param photos the photos to set
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }
    
    public boolean saveNow() throws DBException {
        this.set(FIELD_SUMMER, this.summarizer);
        this.set(FIELD_COMPLETE, this.complete);
        this.set(FIELD_URL, this.url);
        this.set(FIELD_MOMENT, this.moment);
        this.set(FIELD_HOST, this.host.getId());
        this.set(FIELD_TITLE, this.title);
        this.set(FIELD_ID, this.id==null?MyUUID.generateUUID():this.id);
        this.set(FIELD_TAG, this.tag);
        this.set(FIELD_SENTIMENT, this.getSentiment());
        this.set(FIELD_HASH, this.getHash());
        
        boolean save = false;
        if(save=this.saveIt()){
            if(this.id==null){//Si es una nueva introdución vamos a meterle el id
                this.id = New.search(this.url).getId();
            }
            savePhotos();
        }
        
        return save;
    }
    
    private void savePhotos(){
        for(Photo p:photos){
            if(p.getIdHost()==null){
                p.setIdHost(this.host.getId());
            }
            
            if(p.getIdNew()==null){
                p.setIdNew(this.getId());
            }
            p.saveNow();
        }
    }
    
    public static List<New> getAll() {
        List<Model> found = New.findAll();
        return convertModel(found);
    }

    public static New search(String url) {
        String query = FIELD_URL + " = ?";
        Model found = New.findFirst(query, url);
        return convertModel(found);
    }

    public static New searchById(String id) {
        String query = FIELD_ID + " = ?";
        Model found = New.findFirst(query, id);
        return convertModel(found);
    }
    
    public static List<New> searchSince(Date moment) {
        String query = FIELD_URL + " >= ?";
        LazyList<Model> find = New.find(query, moment).orderBy(FIELD_MOMENT+" desc");
        return convertModel(find);
    }

    public static List<New> searchByHost(String idHost, Integer limit, Integer offset) {
        String query = FIELD_HOST + " = ?";
        LazyList<Model> find = New.find(query, idHost).limit(limit).offset(offset).orderBy(FIELD_MOMENT+" desc");

        return convertModel(find);
    }
    
    public static List<New> searchByHost(List<Integer> hosts, Integer limit, Integer offset) {
        //Esto puede dar problemas de sql injection pero no hay otra forma de hacer, tampoco es gran problema que nos saquen todas las noticias :P
        String query = FIELD_HOST + " IN ('" + Util.join(hosts, "','") + "')";
        LazyList<Model> find = New.find(query).limit(limit).offset(offset).orderBy(FIELD_MOMENT+" desc");

        return convertModel(find);
    }
    
    public static List<New> searchByTag(Integer tag, Integer limit, Integer offset) {
        String query = FIELD_TAG + " = ?";
        LazyList<Model> find = New.find(query, tag).limit(limit).offset(offset).orderBy(FIELD_MOMENT+" desc");

        return convertModel(find);
    }
    
    
    public static List<New> searchByTag(List<Integer> tags, Integer limit, Integer offset) {
        //Esto puede dar problemas de sql injection pero no hay otra forma de hacer, tampoco es gran problema que nos saquen todas las noticias :P
        String query = FIELD_TAG + " IN ('" + Util.join(tags, "','") + "')";
        LazyList<Model> find = New.find(query).limit(limit).offset(offset).orderBy(FIELD_MOMENT+" desc");

        return convertModel(find);
    }

    public static List<New> searchBySentiment(Integer sentiment, Integer limit, Integer offset) {
        String query = FIELD_SENTIMENT + " = ?";
        LazyList<Model> find = New.find(query, sentiment).limit(limit).offset(offset).orderBy(FIELD_MOMENT+" desc");

        return convertModel(find);
    }
    
    public static List<New> convertModel(List<Model> modelNew) {
        List<New> news = new ArrayList<>();
        for (Model found : modelNew) {
            news.add(convertModel(found));
        }
        return news;
    }

    public static New convertModel(Model modelNew) {
        try {
            Host host = Host.search(modelNew.getString(FIELD_HOST));//Obtenemos la parte de host
            List<Photo> photos = Photo.search(host.getId(), modelNew.getString(FIELD_ID));
            return new New(modelNew.getString(FIELD_HASH),modelNew.getString(FIELD_COMPLETE), modelNew.getString(FIELD_SUMMER), modelNew.getString(FIELD_URL), host, modelNew.getDate(FIELD_MOMENT), modelNew.getString(FIELD_TITLE), modelNew.getString(FIELD_ID), modelNew.getInteger(FIELD_TAG), photos, modelNew.getInteger(FIELD_SENTIMENT));
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }

    /**
     * @return the tag
     */
    public Integer getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(Integer tag) {
        this.tag = tag;
    }

    /**
     * @return the sentiment
     */
    public Integer getSentiment() {
        return sentiment;
    }

    /**
     * @param sentiment the sentiment to set
     */
    public void setSentiment(Integer sentiment) {
        this.sentiment = sentiment;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the visited
     */
    public Boolean getVisited() {
        return visited;
    }

    /**
     * @return the isFavorite
     */
    public Boolean getIsFavorite() {
        return isFavorite;
    }
    
    


}
