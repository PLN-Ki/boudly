/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

@Table("BDL_SUGGESTION")
public class Suggestion extends Model {
    public static final Integer TYPE_APP_SUGGESTION = 1;
    public static final Integer TYPE_HOST_SUGGESTION = 2;
    public static final Integer TYPE_NEW_ERROR = 3;
            
    public static final String FIELD_ID_SUGGESTION = "idSuggestion";
    private Integer idSuggestion;
    public static final String FIELD_ID_USER = "idUser";
    private String idUser;
    public static final String FIELD_ID_NEW = "idNew";
    private String idNew;
    public static final String FIELD_MOMENT = "moment";
    private Date moment;
    public static final String FIELD_NAME = "name";
    private String name;
    public static final String FIELD_EMAIL = "email";
    private String email;
    public static final String FIELD_MSG = "msg";
    private String msg;
    public static final String FIELD_TYPE = "type";
    private Integer type;
    
    public Suggestion(){
        this.moment = new Date();
        
    }
    public Suggestion(Integer idSuggestion, String idUser, String idNew, String email, String msg, String name, Integer type, Date moment) {
        this.idSuggestion = idSuggestion;
        this.idUser = idUser;
        this.idNew = idNew;
        this.email = email;
        this.msg = msg;
        this.name = name;
        this.type = type;
        this.moment = moment;
        
    }
    
    public boolean saveNow() {
        this.set(FIELD_ID_SUGGESTION, idSuggestion);
        this.set(FIELD_EMAIL, email);
        this.set(FIELD_ID_USER, idUser);
        this.set(FIELD_ID_NEW, idNew);
        this.set(FIELD_MSG, msg);
        this.set(FIELD_NAME, name);
        this.set(FIELD_TYPE, type);
        this.set(FIELD_MOMENT, this.moment);
        return this.saveIt();
    }
    
    public static List<Suggestion> convertModel(List<Model> modelNew) {
        List<Suggestion> suggestions = new ArrayList<>();
        for (Model found : modelNew) {
            suggestions.add(convertModel(found));
        }
        return suggestions;
    }

    public static Suggestion convertModel(Model modelNew) {
        try {
            return new Suggestion(modelNew.getInteger(FIELD_ID_SUGGESTION),
                    modelNew.getString(FIELD_ID_USER),
                    modelNew.getString(FIELD_ID_NEW),
                    modelNew.getString(FIELD_EMAIL),
                    modelNew.getString(FIELD_MSG),
                    modelNew.getString(FIELD_NAME),
                    modelNew.getInteger(FIELD_TYPE),
                    modelNew.getDate(FIELD_MOMENT)            
            );
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }

    /**
     * @return the idSuggestion
     */
    public Integer getIdSuggestion() {
        return idSuggestion;
    }

    /**
     * @param idSuggestion the idSuggestion to set
     */
    public void setIdSuggestion(Integer idSuggestion) {
        this.idSuggestion = idSuggestion;
    }

    /**
     * @return the idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * @return the idNew
     */
    public String getIdNew() {
        return idNew;
    }

    /**
     * @param idNew the idUser to set
     */
    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    /**
     * @return the moment
     */
    public Date getMoment() {
        return moment;
    }

    /**
     * @param moment the moment to set
     */
    public void setMoment(Date moment) {
        this.moment = moment;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }
}
