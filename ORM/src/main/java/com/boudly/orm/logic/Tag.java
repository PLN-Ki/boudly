/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;
import java.util.ArrayList;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


@Table("BDL_TAG")
public class Tag  extends Model{
    public static final String FIELD_ID = "idTag";
    private Integer id;
    public static final String FIELD_VALUE = "value";
    private String value;
    public static final String FIELD_DESCRIPTION = "description";
    private String description;
    public static final String FIELD_VISIBLE = "isVisible";
    private Integer isVisible;
    
    public Tag(){
        
    }
    
    public Tag(String value, String description) {
        this.value = value;
        this.description = description;
    }
    
    public Tag(Integer id, String value, String description, Integer isVisible) {
        this(value, description);
        this.id = id;
        this.isVisible = isVisible;
    }
    
    
    /**
     *
     * @return if  insert it
     * @throws DBException
     */
    public boolean saveNow() throws DBException {
        this.set(FIELD_ID, this.id);
        this.set(FIELD_DESCRIPTION, this.description);
        this.set(FIELD_VALUE, this.value);
        this.set(FIELD_VISIBLE, this.isVisible);
        return this.saveIt();

    }
    
    
    public static List<Tag> getAll() {
        List<Model> found = Tag.findAll();
        return convertModel(found);
    }
    
    public static List<Tag> getAllVisible(){
        
        String query = FIELD_VISIBLE + " = ?";
        List<Model> find = Tag.find(query, 1);

        return convertModel(find);
    }
    
    

    public static List<Tag> convertModel(List<Model> modelHost) {
        List<Tag> tags = new ArrayList<>();
        for (Model found : modelHost) {
            tags.add(convertModel(found));
        }
        return tags;
    }

    public static Tag convertModel(Model modelTag) {
        try {
            return new Tag(modelTag.getInteger(FIELD_ID), modelTag.getString(FIELD_VALUE), modelTag.getString(FIELD_DESCRIPTION), modelTag.getInteger(FIELD_VISIBLE));
            
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }
    

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the isVisible
     */
    public Integer getIsVisible() {
        return isVisible;
    }

    /**
     * @param isVisible the isVisible to set
     */
    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    
}
