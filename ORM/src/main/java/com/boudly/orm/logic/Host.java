/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import com.boudly.orm.helper.MyUUID;
import java.util.ArrayList;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Many2Many;
import org.javalite.activejdbc.annotations.Table;


@Table("BDL_HOST")
@IdName("id")
@Many2Many(other = User.class, join = "BDL_USER_HAS_HOST", sourceFKName = "idUser", targetFKName = "id")
public class Host extends Model {
    public static final String FIELD_URL = "url";
    private String url;
    public static final String FIELD_ID = "id";
    private String id;
    public static final String FIELD_TITLE = "title";
    private String title;
    public static final String FIELD_VISIBLE = "isVisible";
    private Integer isVisible;
    public static final String FIELD_TAG = "default_tag";
    private Integer idDefaultTag;
    public static final String FIELD_ORDER = "loader_order";
    private Integer order;
    
    private Boolean showed;
    
    public Host(String url, String title) {
        this.url = url;
        this.title = title;
    }
    
    public Host(String url,String title, String id, Integer isVisible, Integer idDefaultTag) {
        this(url, title);
        this.id = id;
        this.isVisible = isVisible;
        this.idDefaultTag = idDefaultTag;
    }
    
    public Host(String url,String title, String id, Integer isVisible, Integer idDefaultTag, Integer order) {
        this(url, title);
        this.id = id;
        this.isVisible = isVisible;
        this.idDefaultTag = idDefaultTag;
        this.order = order;
    }
    
    
    public Host(String url,String title, String id, Integer isVisible, Integer idDefaultTag, Integer order, Boolean showed) {
        this( url, title,  id,  isVisible,  idDefaultTag,  order);
        this.showed = showed;
    }

    public Host() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param isVisible the visible param to set
     */
    public void setVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * @return the isVisible
     */
    public Integer getVisible() {
        return isVisible;
    }

    /**
     *
     * @return if  insert it
     * @throws DBException
     */
    public boolean saveNow() throws DBException {
        this.set(FIELD_URL, this.url);
        this.set(FIELD_TITLE, this.title);
        this.set(FIELD_ID, this.id==null?MyUUID.generateUUID():this.id);
        this.set(FIELD_VISIBLE, this.isVisible);
        this.set(FIELD_TAG, this.idDefaultTag);
        this.set(FIELD_ORDER, this.order);
        return this.saveIt();
    }
    
    public static Host search(String idHost) {
        String query = FIELD_ID + " = ?";
        Model find = Host.findFirst(query, idHost);

        return convertModel(find);
    }
    
    public static List<Host> getAll() {
        List<Model> found = Host.findAll().orderBy(FIELD_ORDER);
        return convertModel(found);
    }
    
    public static List<Host> getAllVisible(){
        
        String query = FIELD_VISIBLE + " = ?";
        List<Model> find = Host.find(query, 1).orderBy(FIELD_ORDER);

        return convertModel(find);
    }


    public static List<Host> convertModel(List<Model> modelHost) {
        List<Host> hosts = new ArrayList<>();
        for (Model found : modelHost) {
            hosts.add(convertModel(found));
        }
        return hosts;
    }

    public static Host convertModel(Model modelHost) {
        try {
            return new Host(modelHost.getString(FIELD_URL), modelHost.getString(FIELD_TITLE), modelHost.getString(FIELD_ID), modelHost.getInteger(FIELD_VISIBLE), modelHost.getInteger(FIELD_TAG), modelHost.getInteger(FIELD_ORDER));
            
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }

    /**
     * @return the idDefaultTag
     */
    public Integer getIdDefaultTag() {
        return idDefaultTag;
    }

    /**
     * @param idDefaultTag the idDefaultTag to set
     */
    public void setIdDefaultTag(Integer idDefaultTag) {
        this.idDefaultTag = idDefaultTag;
    }

    /**
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Integer order) {
        this.order = order;
    }
    
    /**
     * @return the showed
     */
    public Boolean getShowed() {
        return showed;
    }
    
    /**
     * @param showed the order to set
     */
    public void setShowed(Boolean showed) {
        this.showed = showed;
    }

}
