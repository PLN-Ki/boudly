/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import com.boudly.orm.helper.MyUUID;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


@Table("BDL_TOKEN")
public class Token extends Model {
    public static final String FIELD_IDUSER = "idUser";
    private String idUser;
    public static final String FIELD_IDTOKEN = "idToken";
    private Integer idToken;
    public static final String FIELD_DATE_CREATION = "date_creation";
    private Date date;
    public static final String FIELD_TOKEN = "token";
    private String token;
    
    public Token(){
        
    }
    
    public Token(String idUser, Integer idToken, Date date, String token){
        this.idUser = idUser;
        this.idToken = idToken;
        this.date = date;
        this.token = token;
    }

    /**
     * @return the idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    
    /**
     * @return the idToken
     */
    public Integer getIdToken() {
        return idToken;
    }

    /**
     * @param idToken the idToken to set
     */
    public void setIdToken(Integer idToken) {
        this.idToken = idToken;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
    
    public boolean saveNow() throws DBException {
        this.set(FIELD_IDUSER, this.idUser==null?MyUUID.generateUUID():this.idUser);
        this.set(FIELD_DATE_CREATION, this.date);
        this.set(FIELD_IDTOKEN, this.idToken);
        this.set(FIELD_TOKEN, this.token);
        
        return this.saveIt();
        
    }
    
    

    public static Token searchByToken(String token) {
        String query = FIELD_TOKEN + " = ?";
        Model find = Token.findFirst(query, token);

        return convertModel(find);
    }
    
    

    public static List<Token> searchAll(Integer limit, Integer offset) {
        LazyList<Token> find = Token.findAll().limit(limit).offset(offset);

        return convertModel(find);
    }
    
    
    public static List<Token> convertModel(List<Token> modelUser) {
        List<Token> users = new ArrayList<>();
        for (Model found : modelUser) {
            users.add(convertModel(found));
        }
        return users;
    }

    public static Token convertModel(Model modelUser) {
        try {
            return new Token(modelUser.getString(FIELD_IDUSER), modelUser.getInteger(FIELD_IDTOKEN), modelUser.getDate(FIELD_DATE_CREATION), modelUser.getString(FIELD_TOKEN));
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }
}
