/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */

package com.boudly.orm.logic;

import com.boudly.orm.helper.MyUUID;
import java.util.ArrayList;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


@Table("BDL_USER")
public class User extends Model {
    public static final String FIELD_IDUSER = "idUSer";
    private String idUser;
    public static final String FIELD_MAIL = "mail";
    private String mail;
    public static final String FIELD_NAME = "name";
    private String name;
    public static final String FIELD_SURNAME = "surname";
    private String surname;
    public static final String FIELD_PASS = "password";
    private String password;
    public static final String FIELD_ROLE = "role";
    private String role;
    
    public static final String ROLE_VALUE_USER = "USER";
    public static final String ROLE_VALUE_ADMIN = "ADMIN";
    
    public User(){
        
    }
    
    public User(String idUser, String mail, String name, String surname, String pass){
        this.idUser = idUser;
        this.mail = mail;
        this.name = name;
        this.surname = surname;
        this.password = pass;
        this.role = ROLE_VALUE_USER;
    }
    
    public User(String idUser, String mail, String name, String surname, String pass, String role){
        this.idUser = idUser;
        this.mail = mail;
        this.name = name;
        this.surname = surname;
        this.password = pass;
        this.role = role;
    }

    /**
     * @return the idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }
    
    
    
    public boolean saveNow() throws DBException {
        this.set(FIELD_IDUSER, this.idUser==null?MyUUID.generateUUID():this.idUser);
        this.set(FIELD_MAIL, this.mail);
        this.set(FIELD_NAME, this.name);
        this.set(FIELD_PASS, this.password);
        this.set(FIELD_SURNAME, this.surname);
        this.set(FIELD_ROLE, this.role);
        
        return this.saveIt();
        
    }
    
    

    public static User search(String mail, String pass) {
        String query = FIELD_MAIL + " = ? AND "+FIELD_PASS+ " = ?";
        Model find = User.findFirst(query, mail, pass);

        return convertModel(find);
    }

    public static User searchById(String idUser) {
        String query = FIELD_IDUSER + " = ?";
        Model find = User.findFirst(query, idUser);

        return convertModel(find);
    }
    
    

    public static List<User> searchAll(Integer limit, Integer offset) {
        LazyList<User> find = User.findAll().limit(limit).offset(offset);

        return convertModel(find);
    }
    
    
    public static List<User> convertModel(List<User> modelUser) {
        List<User> users = new ArrayList<>();
        for (Model found : modelUser) {
            users.add(convertModel(found));
        }
        return users;
    }

    public static User convertModel(Model modelUser) {
        try {
            return new User(modelUser.getString(FIELD_IDUSER), modelUser.getString(FIELD_MAIL),modelUser.getString(FIELD_NAME),modelUser.getString(FIELD_SURNAME),modelUser.getString(FIELD_PASS), modelUser.getString(FIELD_ROLE));
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }
}
