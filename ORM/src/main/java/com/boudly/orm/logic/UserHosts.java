/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

@Table("BDL_USER_HAS_HOST")
public class UserHosts extends Model {

    public static final String FIELD_IDUSER = "idUser";
    private String idUser;
    public static final String FIELD_IDHOST = "idHost";
    private String idHost;
    public static final String FIELD_SHOWED = "showed";
    private Boolean showed;

    public UserHosts() {
    }

    public UserHosts(String idUser, String idHost, Boolean showed) {
        this.idUser = idUser;
        this.idHost = idHost;
        this.showed = showed;
    }

    /**
     * @return the idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * @return the idHost
     */
    public String getIdHost() {
        return idHost;
    }

    /**
     * @param idHost the idToken to set
     */
    public void setIdHost(String idHost) {
        this.idHost = idHost;
    }

    public boolean saveNow() throws DBException {
        this.set(FIELD_IDUSER, this.idUser);
        this.set(FIELD_IDHOST, this.idHost);
        this.set(FIELD_SHOWED, this.showed ? 1 : 0);

        return this.saveIt();

    }

    public static List<UserHosts> search(String idUser) {
        String query = FIELD_IDUSER + " = ? AND " + Host.FIELD_VISIBLE + " = 1";
        LazyList<Model> find = UserHosts.find(query, idUser);

        return convertModel(find);
    }

    public static List<Host> searchHostDetail(String idUser) throws SQLException {
        String q = "select BDL_HOST.*, BDL_USER_HAS_HOST.showed  from BDL_USER_HAS_HOST left join BDL_HOST on BDL_USER_HAS_HOST.idHost=BDL_HOST.id where " + FIELD_IDUSER + " = ? AND " + Host.FIELD_VISIBLE + " = 1";
        return executeMyQuery(q, new Object[]{idUser});
    }

    public static List<UserHosts> convertModel(List<Model> modelUser) {
        List<UserHosts> users = new ArrayList<>();
        for (Model found : modelUser) {
            users.add(convertModel(found));
        }
        return users;
    }

    public static Host convertModel(ResultSet modelHost) throws SQLException {
        Host h = new Host(modelHost.getString(Host.FIELD_URL), modelHost.getString(Host.FIELD_TITLE), modelHost.getString(Host.FIELD_ID), modelHost.getInt(Host.FIELD_VISIBLE), modelHost.getInt(Host.FIELD_TAG), modelHost.getInt(Host.FIELD_ORDER));
        h.setShowed(modelHost.getInt(FIELD_SHOWED) == 1);
        return h;

    }

    public static UserHosts convertModel(Model modelUser) {
        try {
            return new UserHosts(modelUser.getString(FIELD_IDUSER), modelUser.getString(FIELD_IDHOST), modelUser.getInteger(FIELD_SHOWED) == 1);
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }

    private static List<Host> executeMyQuery(String query, Object[] args) throws SQLException {//Integer idUser, Integer idHost, Integer limit, Integer offset) {
        List<Host> hosts = new ArrayList<>();
        Connection connection = Base.connection();

        PreparedStatement stmt = connection.prepareStatement(query);
        for (int i = 0; i < args.length; i++) {
            stmt.setObject(i + 1, args[i]);
        }

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            hosts.add(convertModel(rs));
        }

        rs.close();

        return hosts;
    }

    /**
     * @return the showed
     */
    public Boolean getShowed() {
        return showed;
    }

    /**
     * @param showed the showed to set
     */
    public void setShowed(Boolean showed) {
        this.showed = showed;
    }

}
