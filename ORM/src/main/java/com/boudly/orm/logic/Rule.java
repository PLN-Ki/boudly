/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import java.util.ArrayList;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


@Table("BDL_RULE")
public class Rule extends Model {
    public static final String FIELD_VALUE = "value";
    private String value;
    public static final String FIELD_ID = "id";
    private Integer id;
    public static final String FIELD_ID_HOST = "id_Host";
    private Host host;
    public static final String FIELD_DESCRIPTION = "description";
    private String description;
    public static final String FIELD_TYPE = "type";
    private Integer type;
    
    public static final Integer CONTAIN_TYPE = 0;
    public static final Integer URLS_TYPE = 1;
    public static final Integer PHOTOS_TYPE = 2;
    

    
    public Rule(String value, String description) {
        this.value = value;
        this.description = description;
    }
    
    
    public Rule(String value, String description, Host host, Integer type) {
        this(value, description);
        this.host = host!=null?host: new Host();
        this.type = type;
    }

    public Rule(String value, String description, Integer id, Host host, Integer type) {
        this(value, description, host, type);
        this.id = id;
    }

    public Rule() {

    }


    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the host
     */
    public Host getIdHost() {
        return host;
    }

    /**
     * @param host the hot to set
     */
    public void setIdHost(Host host) {
        this.host = host;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }
    
    public boolean saveNow() throws DBException {
        this.set(FIELD_DESCRIPTION, this.description);
        this.set(FIELD_ID, this.id);
        this.set(FIELD_ID_HOST, this.host.getId());
        this.set(FIELD_VALUE, this.value);
        this.set(FIELD_TYPE, this.type);
        return this.saveIt();
    }
    
    
    public static List<Rule> getAll() {
        List<Model> found = Rule.findAll();
        return convertModel(found);
    }
    
     public static List<Rule> search(Integer type, String idHost) {
        String query = FIELD_TYPE + " = ? AND "+FIELD_ID_HOST + " = ?";
        List<Model> found = Rule.find(query, type, idHost);
        return convertModel(found);
    }
    
    

    public static List<Rule> searchByHost(String idHost) {
        String query = FIELD_ID_HOST + " = ?";
        List<Model> found = Rule.find(query, idHost);
        return convertModel(found);
    }
    
    public static Rule search(Integer id) {
        String query = FIELD_ID + " = ?";
        Model found = Rule.findFirst(query, id);
        return convertModel(found);
    }


    public static List<Rule> convertModel(List<Model> modelRule) {
        List<Rule> rules = new ArrayList<>();
        for (Model found : modelRule) {
            rules.add(convertModel(found));
        }
        return rules;
    }

    public static Rule convertModel(Model modelRule) {
        try {
            Host host = Host.search(modelRule.getString(FIELD_ID_HOST));//Obtenemos la parte de host
            return new Rule(modelRule.getString(FIELD_VALUE), modelRule.getString(FIELD_DESCRIPTION), modelRule.getInteger(FIELD_ID), host, modelRule.getInteger(FIELD_TYPE));
   
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }


}
