/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2016
 * 
 */
package com.boudly.orm.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

@Table("BDL_USER_SHOW_TAG")
public class UserTags extends Model {

    public static final String FIELD_ID_TAG = "idTag";
    private Integer idTag;
    private static final String FIELD_VALUE = "value";
    private String value;
    public static final String FIELD_DESCRIPTION = "description";
    private String description;
    private static final String FIELD_VISIBLE = "isVisible";
    private Integer isVisible;
    public static final String FIEL_ID_USER = "idUser";
    private String idUser;
    public static final String FIELD_USER_VISIBLE = "visible";
    private Integer userVisible;

    public UserTags(Integer idTag, String value, String description, Integer isVisible, String idUser, Integer userVisibility) {
        this.value = value;
        this.description = description;
        this.idTag = idTag;
        this.isVisible = isVisible;
        this.idUser = idUser;
        this.userVisible = userVisibility;
    }

    public UserTags(Integer idTag, String idUser, Integer userVisibility) {
        this.idTag = idTag;
        this.idUser = idUser;
        this.userVisible = userVisibility;
    }

    public static List<UserTags> getAllVisible(String idUser)  throws SQLException {
        String query = "select  distinct result.idTag, result.value, result.description, result.isVisible, result.idUser, result.visible from ( "
                + " select BDL_TAG.*, BDL_USER_SHOW_TAG.idUSer, BDL_USER_SHOW_TAG.visible from BDL_TAG LEFT OUTER JOIN BDL_USER_SHOW_TAG on BDL_TAG.idTag=BDL_USER_SHOW_TAG.idTag where idUser = ? AND isVisible = ?  AND visible = 0 "
                + " union "
                + " select BDL_TAG.*, ? as idUSer, 1 as visible from BDL_TAG where isVisible = ?) as result GROUP BY result.idTag";
        return executeMyQuery(query, new Object[]{idUser, 1, idUser, 1});
    }

    public static List<UserTags> executeMyQuery(String query, Object[] args) throws SQLException {//Integer idUser, Integer idHost, Integer limit, Integer offset) {
        List<UserTags> tags = new ArrayList<>();
        Connection connection = Base.connection();

        PreparedStatement stmt = connection.prepareStatement(query);
        for (int i = 0; i < args.length; i++) {
            stmt.setObject(i + 1, args[i]);
        }

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            tags.add(convertModel(rs));
        }

        rs.close();

        return tags;
    }

    public static UserTags convertModel(ResultSet modelTag)  throws SQLException {
            return new UserTags(modelTag.getInt(FIELD_ID_TAG), modelTag.getString(FIELD_VALUE), modelTag.getString(FIELD_DESCRIPTION), modelTag.getInt(FIELD_VISIBLE), modelTag.getString(FIEL_ID_USER), modelTag.getInt(FIELD_USER_VISIBLE));

        
    }

    /**
     *
     * @return if insert it
     * @throws DBException
     */
    public boolean saveNow() throws DBException {
        this.set(FIEL_ID_USER, this.idUser);
        this.set(FIELD_ID_TAG, this.idTag);
        this.set(FIELD_USER_VISIBLE, this.userVisible);
        return this.saveIt();

    }

    /**
     * @return the id
     */
    public Integer getIdTag() {
        return idTag;
    }

    /**
     * @param id the id to set
     */
    public void setIdTag(Integer id) {
        this.idTag = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the isVisible
     */
    public Integer getIsVisible() {
        return isVisible;
    }

    /**
     * @param isVisible the isVisible to set
     */
    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * @return the idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUSer to set
     */
    public void setIdUser(String idUSer) {
        this.idUser = idUSer;
    }

    /**
     * @return the userVisible
     */
    public Integer getUserVisible() {
        return userVisible;
    }

    /**
     * @param userVisible the userVisible to set
     */
    public void setUserVisible(Integer userVisible) {
        this.userVisible = userVisible;
    }

}
