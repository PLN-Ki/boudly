/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import java.util.ArrayList;
import java.util.List;
import org.javalite.activejdbc.DBException;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

@Table("BDL_PHOTO")
public class Photo extends Model {
    public static final String FIELD_URL = "url_photo";
    private String url;
    public static final String FIELD_ID = "id";
    private Integer id;
    public static final String FIELD_ID_NEW = "New_id";
    private String idNew;
    public static final String FIELD_ID_HOST = "New_id_Host";
    private String idHost;

    public Photo(String url) {
        this.url = url;
    }
    
    public Photo(String url, String idNew, String idHost) {
        this(url);
        this.idNew = idNew;
        this.idHost = idHost;
    }
    
    public Photo(String url, String idNew, String idHost, Integer id) {
        this(url, idNew, idHost);
        this.id = id;
    }

    public Photo() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    
    /**
     * @return the idNew
     */
    public String getIdNew() {
        return idNew;
    }

    /**
     * @param idNew the idNew to set
     */
    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    /**
     * @return the idHost
     */
    public String getIdHost() {
        return idHost;
    }

    /**
     * @param idHost the idHost to set
     */
    public void setIdHost(String idHost) {
        this.idHost = idHost;
    }

    public boolean saveNow() throws DBException {
        this.set(FIELD_URL, this.url);
        this.set(FIELD_ID_HOST, this.getIdHost());
        this.set(FIELD_ID_NEW, this.getIdNew());
        this.set(FIELD_ID, this.id);
        return this.saveIt();
    }
    
    
    public static List<Photo> getAll() {
        List<Model> found = Photo.findAll();
        return convertModel(found);
    }

    
    public static List<Photo> search(String idHost, String idNew) {
        String query = FIELD_ID_HOST + " = ? AND "+FIELD_ID_NEW+" = ?";
        LazyList<Model> find = Photo.find(query, idHost, idNew);

        return convertModel(find);
    }

    public static List<Photo> convertModel(List<Model> modelPhoto) {
        List<Photo> photos = new ArrayList<>();
        for (Model found : modelPhoto) {
            photos.add(convertModel(found));
        }
        return photos;
    }

    public static Photo convertModel(Model modelPhoto) {
        try {
            return new Photo(modelPhoto.getString(FIELD_URL), modelPhoto.getString(FIELD_ID_NEW), modelPhoto.getString(FIELD_ID_HOST), modelPhoto.getInteger(FIELD_ID));
            
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }


}
