/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@Table("BDL_USER_VISITED_NEW")
public class UserNews extends Model {

    public static final Boolean NOT_FAVORITE_VALUE = false;
    public static final Boolean FAVORITE_VALUE = true;
    public static final Boolean DEFAULT_FAVORITE_VALUE = false;
    public static final String FIELD_IDUSER = "idUser";
    public static final String FIELD_IDNEW = "idNew";
    public static final String FIELD_FAVORITE = "favorite";
    private String idUser;
    private String idNew;
    private Boolean favorite;

    public UserNews() {
        this.favorite = DEFAULT_FAVORITE_VALUE;
    }

    /**
     *
     * @return saved correct
     */
    public boolean saveNow() {
        this.set(FIELD_IDUSER, this.idUser);
        this.set(FIELD_IDNEW, this.idNew);
        this.set(FIELD_FAVORITE, this.favorite ? 1 : 0);

        return this.saveIt();

    }

    public UserNews(String idUser, String idNew) {
        this();
        this.idUser = idUser;
        this.idNew = idNew;
    }

    public UserNews(String idUser, String idNew, Boolean favorite) {
        this.idUser = idUser;
        this.idNew = idNew;
        this.favorite = favorite;
    }

    public static List<New> executeMyQuery(String query, Object[] args) throws SQLException {//Integer idUser, Integer idHost, Integer limit, Integer offset) {
        List<New> news = new ArrayList<>();
        Connection connection = Base.connection();

        PreparedStatement stmt = connection.prepareStatement(query);
        for (int i = 0; i < args.length; i++) {
            stmt.setObject(i + 1, args[i]);
        }

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            news.add(convertModel(rs));
        }

        rs.close();

        return news;
    }

    public static List<New> searchByHost(String idUser, String idHost, Integer limit, Integer offset) throws SQLException {
        String query = "select BDL_NEW.*, CASE WHEN " + FIELD_IDUSER + "= ? THEN 1 ELSE 0 END as " + New.FIELD_VISITED + ", " + UserNews.FIELD_FAVORITE + " from BDL_NEW left join BDL_USER_VISITED_NEW on " + New.FIELD_ID + "=" + UserNews.FIELD_IDNEW + " where " + FIELD_IDUSER + "=? AND " + New.FIELD_HOST + "  =  ? order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idHost, offset, limit});

    }

    public static List<New> searchByHost(String idUser, List<String> hosts, Integer limit, Integer offset) throws SQLException  {
        String query = "select BDL_NEW.*, CASE WHEN " + FIELD_IDUSER + "= ? THEN 1 ELSE 0 END as " + New.FIELD_VISITED + ", " + UserNews.FIELD_FAVORITE + " from BDL_NEW left join BDL_USER_VISITED_NEW on " + New.FIELD_ID + "=" + UserNews.FIELD_IDNEW + " where " + FIELD_IDUSER + "=? AND " + New.FIELD_HOST + " IN ('" + Util.join(hosts, "','") + "')" + " order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idUser, offset, limit});
    }

    public static List<New> searchByTag(String idUser, Integer tag, Integer limit, Integer offset) throws SQLException  {
        String query = "select BDL_NEW.*, CASE WHEN " + FIELD_IDUSER + "= ? THEN 1 ELSE 0 END as " + New.FIELD_VISITED + ", " + UserNews.FIELD_FAVORITE + " from BDL_NEW left join BDL_USER_VISITED_NEW on " + New.FIELD_ID + "=" + UserNews.FIELD_IDNEW + " where " + FIELD_IDUSER + "=? AND " + New.FIELD_TAG + "  =  ? order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idUser, tag, offset, limit});
    }

    public static List<New> searchByTag(String idUser, List<Integer> tags, Integer limit, Integer offset) throws SQLException  {
        String query = "select BDL_NEW.*, CASE WHEN " + FIELD_IDUSER + "= ? THEN 1 ELSE 0 END as " + New.FIELD_VISITED + ", " + UserNews.FIELD_FAVORITE + " from BDL_NEW left join BDL_USER_VISITED_NEW on " + New.FIELD_ID + "=" + UserNews.FIELD_IDNEW + " where " + FIELD_IDUSER + "=? AND " + New.FIELD_TAG + " IN ('" + Util.join(tags, "','") + "')" + " order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idUser, offset, limit});
    }

    public static List<New> searchNewsByUserAndTag(String idUser, Integer tag, Integer limit, Integer offset) throws SQLException  {
        String query = "select *, CASE WHEN BDL_USER_VISITED_NEW.idUser = ? THEN 1 ELSE 0 END as visited from BDL_USER_HAS_HOST left join BDL_NEW on BDL_NEW.id_Host=BDL_USER_HAS_HOST.idHost left join BDL_USER_VISITED_NEW on BDL_NEW.id=BDL_USER_VISITED_NEW.idNew where BDL_USER_HAS_HOST." + UserNews.FIELD_IDUSER + "=? AND " + New.FIELD_TAG + "  =  ? order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idUser, tag, offset, limit});
    }

    public static List<New> searchNewsByUserAndTag(String idUser, List<Integer> tags, Integer limit, Integer offset) throws SQLException  {
        String query = "select *, CASE WHEN BDL_USER_VISITED_NEW.idUser =  ? THEN 1 ELSE 0 END as visited from BDL_USER_HAS_HOST left join BDL_NEW on BDL_NEW.id_Host=BDL_USER_HAS_HOST.idHost left join BDL_USER_VISITED_NEW on BDL_NEW.id=BDL_USER_VISITED_NEW.idNew where BDL_USER_HAS_HOST." + UserNews.FIELD_IDUSER + "=? AND " + New.FIELD_TAG + " IN ('" + Util.join(tags, "','") + "')" + " order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idUser, offset, limit});
    }

    public static List<New> searchFavoriteNews(String idUser, Integer limit, Integer offset) throws SQLException  {
        String query = "select BDL_NEW.*, CASE WHEN " + FIELD_IDUSER + " =  ? THEN 1 ELSE 0 END as " + New.FIELD_VISITED + ", " + UserNews.FIELD_FAVORITE + " from BDL_NEW left join BDL_USER_VISITED_NEW on " + New.FIELD_ID + " = " + UserNews.FIELD_IDNEW + " where " + UserNews.FIELD_IDUSER + "  =  ? and " + UserNews.FIELD_FAVORITE + "=1 order by visited, moment DESC limit ?,?";
        return executeMyQuery(query, new Object[]{idUser, idUser, offset, limit});
    }

    public static New convertModel(ResultSet modelNew) throws SQLException {
        Host host = Host.search(modelNew.getString(New.FIELD_HOST));//Obtenemos la parte de host
        List<Photo> photos = Photo.search(host.getId(), modelNew.getString(New.FIELD_ID));
        return new New(modelNew.getString(New.FIELD_HASH), modelNew.getString(New.FIELD_COMPLETE), modelNew.getString(New.FIELD_SUMMER), modelNew.getString(New.FIELD_URL), host, modelNew.getDate(New.FIELD_MOMENT), modelNew.getString(New.FIELD_TITLE), modelNew.getString(New.FIELD_ID), modelNew.getInt(New.FIELD_TAG), photos, modelNew.getInt(New.FIELD_SENTIMENT), modelNew.getInt(New.FIELD_VISITED) == 1, modelNew.getInt(UserNews.FIELD_FAVORITE) == 1);

    }

    public static List<UserNews> convertModel(List<Model> modelNew) {
        List<UserNews> news = new ArrayList<>();
        for (Model found : modelNew) {
            news.add(convertModel(found));
        }
        return news;
    }

    public static UserNews convertModel(Model modelNew) {
        try {
            return new UserNews(modelNew.getString(FIELD_IDUSER), modelNew.getString(FIELD_IDNEW), modelNew.getInteger(FIELD_FAVORITE) == 1);
        } catch (NullPointerException ex) {
            //Not found
            return null;
        }
    }

    /**
     * @return the idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * @return the idNew
     */
    public String getIdNew() {
        return idNew;
    }

    /**
     * @param idNew the idNew to set
     */
    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    /**
     * @return the favorite
     */
    public Boolean getFavorite() {
        return favorite;
    }

    /**
     * @param favorite the favorite to set
     */
    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

}
