/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.helper;

import java.util.Date;
import java.util.UUID;


public class MyUUID {
    
    public static synchronized String generateUUID(String url) {
        UUID nameUUIDFromBytes = java.util.UUID.nameUUIDFromBytes(url.getBytes());
        return nameUUIDFromBytes.toString().replaceAll("-", "");
    }
    
    public static synchronized String generateUUID() {
        return java.util.UUID.randomUUID().toString().replaceAll("-", "");
        
    }
}
    
