/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */

package com.boudly.orm.config;

import com.boudly.orm.exceptions.LoadFileException;

public class LoaderConfiguration {
    

    public static final String JNI_NAME_DB = "com.boudly.ara.orm.config.db.jni";
    public static final String HOST_DB = "com.boudly.ara.orm.config.db.host";
    public static final String PORT_DB = "com.boudly.ara.orm.config.db.port";
    public static final String USER_DB = "com.boudly.ara.orm.config.db.user";
    public static final String PASS_DB = "com.boudly.ara.orm.config.db.pass";
    public static final String DRIVER_DB = "com.boudly.ara.orm.config.db.driver";
    public static final String DATABASE_DB = "com.boudly.ara.orm.config.db.database";
    

    /**
     * 
     * @param param
     * @return param
     * @throws LoadFileException 
     */
    public static String getParam(String param) throws LoadFileException{
        return PropertiesConfig.getInstance().getProperty(param);
    }
    
}
