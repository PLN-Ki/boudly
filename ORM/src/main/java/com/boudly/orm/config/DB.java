package com.boudly.orm.config;

import com.boudly.orm.exceptions.LoadFileException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.javalite.activejdbc.Base;

public class DB {

    public static synchronized void init() throws Exception {

//            Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/prueba", "chorizo", "chorizo");
        Base.open(LoaderConfiguration.getParam(LoaderConfiguration.DRIVER_DB),
                LoaderConfiguration.getParam(LoaderConfiguration.HOST_DB) + ":" + LoaderConfiguration.getParam(LoaderConfiguration.PORT_DB) + "/" + LoaderConfiguration.getParam(LoaderConfiguration.DATABASE_DB),
                LoaderConfiguration.getParam(LoaderConfiguration.USER_DB),
                LoaderConfiguration.getParam(LoaderConfiguration.PASS_DB));

    }

    public static synchronized void initWithJniName(Context ctx) throws Exception {
        DataSource ds = (DataSource) ctx.lookup(LoaderConfiguration.getParam(LoaderConfiguration.JNI_NAME_DB));
        Base.open(ds);

    }
    
    
//    public static synchronized void initWithJniName() throws Exception {
//        Base.open(LoaderConfiguration.getParam(LoaderConfiguration.JNI_NAME_DB));
//    }

    public static void close() {
        Base.close();
    }
}

    

