/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.orm.config;

import com.boudly.orm.exceptions.LoadFileException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PropertiesConfig {

    private static Properties properties;
    private static final String PROPERTIES_FILE = "ara_orm_properties.properties";

    public static Properties getInstance() throws LoadFileException {
        if (properties == null) {
            properties = openProperties();
        }
        return properties;

    }

    private static Properties openProperties() throws LoadFileException {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(obtainPath());

            prop.load(input);

        } catch (IOException io) {
            //Como la biblioteca se puede usar tambien des
            throw new LoadFileException("Imposible read file properties", LoadFileException.IMPOSIBLE_READ_FILE);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new LoadFileException("Imposible close file properties", LoadFileException.IMPOSIBLE_READ_FILE);
                }
            }

        }
        return prop;
    }

    private static boolean executeHowLibrary() {

        String path = PropertiesConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        Integer pathLeng = path.length();

        Integer pathExtensionInit = path.length() - 3;

        if (pathExtensionInit > 0) {

            return path.substring(pathExtensionInit, pathLeng).equals("jar");

        } else {

            return false;

        }

    }

    private static String obtainPath() {

        String path = "";

        if (executeHowLibrary()) {

            path = PropertiesConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath();

            List<String> parts = Arrays.asList(path.split("/"));

            int posToRemove = parts.size() - 1;

            parts = parts.subList(0, posToRemove);

            path = PropertiesConfig.join("/", parts) + "/";

        }

        return path + PROPERTIES_FILE;

    }

    /**
     * @param union parameter to join the elements
     * @param list elements joinned
     * @return text joined
     */
    private static String join(String union, List<String> list) {
        StringBuilder join = new StringBuilder();
        for (String element : list) {
            join.append(element).append(union);
        }
        return join.length() > 0 ? join.substring(0, join.length() - union.length()) : join.toString();
    }

}
