/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.classifier;

import java.util.HashMap;


public class TrainingObject {
    
    private HashMap<String, Integer> freqs;
    private String tag;
    private Integer id;

    public HashMap<String, Integer> getFreqs() {
        return freqs;
    }

    public TrainingObject setFreqs(HashMap<String, Integer> freqs) {
        this.freqs = freqs;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public TrainingObject setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public TrainingObject setId(Integer id) {
        this.id = id;
        return this;
    }
    

}
