/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.classifier;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Model implements java.io.Serializable {

    //HASH KEYWORD -> TAG -> PROB
    private HashMap<String, HashMap<String, Double>> model;
    private HashMap<String, Double> probTag;
    private HashMap<String, Double> probKeyword;
    private Integer numberOfPairs = 0;

    protected List<String> tags;

    public Double probabilityOfTag(String tag) {

        Double prob = 0.0;

        if (this.probTag.keySet().contains(tag)) {

            prob = this.probTag.get(tag);
        }

        return prob;
    }

    public boolean serialize(String path) {
        Boolean wasOk = false;

        try {
            Kryo kryo = new Kryo();
            Output output = new Output(new FileOutputStream(path + "model.ser"));
            kryo.writeObject(output, this);
            output.close();
            wasOk = true;
        } catch (Exception ex) {
            wasOk = false;
        }

        /*try {
            FileOutputStream fileOut = new FileOutputStream(path + "model.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
            wasOk = true;
        } catch (Exception ex) {
            wasOk = false;
        }*/
        return wasOk;

    }

    public static Model unserialize(String path) {

        Model model = null;

        try {
            Kryo kryo = new Kryo();
            Input input = new Input(new FileInputStream("file.bin"));
            model= kryo.readObject(input, Model.class);
            input.close();

        } catch (Exception i) {

        }

        /*Model model = null;
        try {
            FileInputStream fileIn = new FileInputStream(path + "model.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            model = (Model) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {

        }*/

        return model;

    }

    public Double probabilityOfPair(String keyword, String tag) {

        Double prob = 1 / this.numberOfPairs.doubleValue();

        if (this.model.keySet().contains(keyword)) {

            if (this.model.get(keyword).keySet().contains(tag)) {

                prob = this.model.get(keyword).get(tag);
            }
        }

        return prob;
    }

    public Model(List<TrainingObject> training) {

        this.model = new HashMap<>();
        this.probTag = new HashMap<>();
        this.probKeyword = new HashMap<>();
        this.tags = new ArrayList<>();

        //FREQ
        for (TrainingObject obj : training) {

            this.tags.add(obj.getTag());

            for (String keyword : obj.getFreqs().keySet()) {

                addPair(keyword, obj.getTag(), obj.getFreqs().get(keyword));
                addTag(obj.getTag(), obj.getFreqs().get(keyword));
                addKeyword(keyword);
            }
        }

        calculateTagProbability();

        //MODEL WITH TFIDF
        for (String keyword : this.model.keySet()) {

            for (String tag : this.model.get(keyword).keySet()) {

                Double idf = this.numberOfPairs / this.probKeyword.get(keyword);
                Double tfidf = this.model.get(keyword).get(tag) * idf;
                Double pairProb = tfidf / this.probTag.get(tag);

                this.model.get(keyword).put(tag, pairProb);
            }
        }

        this.probKeyword.clear();
        this.probKeyword = null;

    }

    private void addPair(String keyword, String tag, Integer freq) {

        if (!this.model.keySet().contains(keyword)) {

            this.model.put(keyword, new HashMap<String, Double>());
        }
        if (!this.model.get(keyword).keySet().contains(tag)) {

            this.model.get(keyword).put(tag, 0.0);
            this.numberOfPairs++;
        }

        this.model.get(keyword).put(tag, this.model.get(keyword).get(tag) + freq);
    }

    private void addTag(String tag, Integer freq) {

        if (!this.probTag.keySet().contains(tag)) {

            this.probTag.put(tag, 0.0);
        }

        this.probTag.put(tag, this.probTag.get(tag) + freq);
    }

    private void addKeyword(String keyword) {

        if (!this.probKeyword.keySet().contains(keyword)) {

            this.probKeyword.put(keyword, 0.0);
        }

        this.probKeyword.put(keyword, this.probKeyword.get(keyword) + 1);
    }

    private void calculateTagProbability() {

        for (String tag : probTag.keySet()) {

            this.probTag.put(tag, probTag.get(tag) / this.numberOfPairs);
        }
    }
}
