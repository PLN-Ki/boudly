/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.classifier;

import com.boudly.keyword.KWExtractor;
import com.boudly.keyword.Keyword;
import com.boudly.tagger.Tag;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Target {
    
    private String text;
    private List<Keyword> keywords;
    private List<Tag> tags;
    private Date classificationDate;
    private Long classificationTime;

    public Target() {        
        this.keywords = new ArrayList<>();
        this.tags = new ArrayList<>();
    }

    public Target(String text, String lang) {

        long start = System.currentTimeMillis();

        this.text = text;
        this.keywords = new KWExtractor().extract(text, lang);
        this.tags = new ArrayList<>();

        long end = System.currentTimeMillis();

        this.setClassificationTime(end-start);
    }
    
    public String getText() {
        return text;
    }

    public Target setText(String text) {
        this.text = text;
        return this;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public Target setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
        return this;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Target setTags(List<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Date getClassificationDate() {
        return classificationDate;
    }

    public Target setClassificationDate(Date classificationDate) {
        this.classificationDate = classificationDate;
        return this;
    }

    public Long getClassificationTime() {
        return classificationTime;
    }

    public Target setClassificationTime(Long classificationTime) {
        this.classificationTime = classificationTime;
        return this;
    }
    
    
}
