/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */

package com.boudly.classifier;

import com.boudly.keyword.Keyword;
import com.boudly.tagger.Tag;
import com.boudly.training.Parser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Classifier_V1 extends Classifier {

    private static final int VALID_TAG_THRESHOLD = 5;

    @Override
    public Target execute(Target target) {

        try {

            long start = System.currentTimeMillis();

            HashMap<String, Double> targetTags = new HashMap<>();
            Double probability;

            for (String tag : model.tags) {

                probability = model.probabilityOfTag(tag);

                for (Keyword keyword : target.getKeywords()) {

                    probability = probability * model.probabilityOfPair(keyword.getWord(), tag);
                }

                targetTags.put(tag, probability);
            }

            Map<String, Double> ordered = sortByComparator(targetTags);
            List<Tag> tags = new ArrayList<>();
            int accepted = 0;

            for (String tagStr : ordered.keySet()) {

                tags.add(new Tag().setTag(tagStr).setProbability(ordered.get(tagStr)).setId(Parser.searchId(tagStr)));
                accepted++;

                if (accepted == VALID_TAG_THRESHOLD) {

                    break;
                }
            }

            long end = System.currentTimeMillis();

            target.setClassificationDate(new Date(end));
            target.setClassificationTime(target.getClassificationTime() + (end - start));
            
            target.setTags(tags);
            
        } catch (Exception ex) {

            String hola = "";
        }
        
        return target;
    }

    private static Map<String, Double> sortByComparator(Map<String, Double> unsortMap) {

        // Convert Map to List
        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1,
                    Map.Entry<String, Double> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
