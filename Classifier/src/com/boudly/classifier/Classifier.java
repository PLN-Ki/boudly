/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */

package com.boudly.classifier;


public abstract class Classifier {
    
    public enum Version{
    
        V1
    }
    
    public Model model;
    
    public static Classifier getInstance(Version version, Model model){
    
        Classifier classifier;
        
        switch(version){
        
            case V1:
                classifier = new Classifier_V1();
                break;
            default:
                classifier = new Classifier_V1();
                break;
        }
        
        classifier.model = model;
        
        return classifier;
    }
    
    public abstract Target execute(Target target);
}
