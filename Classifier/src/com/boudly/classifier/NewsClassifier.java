/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.classifier;


import com.boudly.tagger.Tag;
import com.boudly.training.Parser;
import com.tekstly.linguist.Language;
import java.util.List;


public class NewsClassifier {
    
    Classifier classifier;

    public NewsClassifier(String pathTrainer) {
        Model model = Model.unserialize(pathTrainer);
        if(model==null){
            List<TrainingObject> training = new Parser().parse(pathTrainer);
            model = new Model(training);
            System.out.println(model.serialize(pathTrainer));
        }
        classifier = Classifier.getInstance(Classifier.Version.V1, model);
    }
    
    public Tag clasificar(String texto){
        
        Target target = new Target(texto, Language.ES);
        target = classifier.execute(target);
    
        return target.getTags().get(0);
    }
    
}
