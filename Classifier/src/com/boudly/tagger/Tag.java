/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.tagger;

public class Tag {
    
    private String tag;
    private Integer id;
    private Double probability;

    public String getTag() {
        return tag;
    }

    public Tag setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public Double getProbability() {
        return probability;
    }

    public Tag setProbability(Double probability) {
        this.probability = probability;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Tag setId(Integer id) {
        this.id = id;
        return this;
    }
    
        
}
