/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.training;

import com.boudly.classifier.TrainingObject;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Parser {
    public static final Integer NOT_FOUND_ID = -1;
    private static final String[] topicsName = {"POLITICA", "CIENCIA", "ARTE_Y_CULTURA", "DEPORTES", "ECONOMIA", "EDUCACION", "SALUD", "SOCIEDAD", "TECNOLOGIA"};
    private static final Integer[] topicsId = {1, 3, 2, 4, 5, 6, 7, 8, 9};
    
    public static Integer searchId(String tagStr) {
        for(int pos = 0;pos<topicsName.length;pos++){
            if(topicsName[pos].equals(tagStr)){
                return topicsId[pos];
            }
        }
        return NOT_FOUND_ID;
    }

    public List<TrainingObject> parse(String basePath) {

        List<TrainingObject> list = new ArrayList<>();

        for(int pos=0;pos<topicsId.length;pos++){
            TrainingObject obj = new TrainingObject();
            HashMap<String, Integer> freqs = new HashMap<>();

            FileReader fr = null;
            BufferedReader br = null;
            
            try {
                fr = new FileReader(basePath+topicsName[pos]+"/words_"+topicsName[pos]+".txt");
                br = new BufferedReader(fr);
            
                String line;
                br.readLine();
                while ((line = br.readLine()) != null) {

                    String[] splitted = line.split(";");

                    freqs.put(splitted[0], Integer.parseInt(splitted[1]));

                }
                
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                
                try {
                    if(br != null)
                        br.close();
                    
                    if(fr != null)
                        fr.close();
                } catch (IOException ex) {
                    Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            obj.setTag(topicsName[pos]);
            obj.setId(topicsId[pos]);
            obj.setFreqs(freqs);

            list.add(obj);
        }
        

        return list;
    }
}
