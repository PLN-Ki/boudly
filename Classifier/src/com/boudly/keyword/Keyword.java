/* 
 * 
 *  Boudly
 * 
 *  @author boudly.com
 *  @since v1.0
 * 
 *  Copyright 2015
 * 
 */
package com.boudly.keyword;

public class Keyword {
    
    private String word;

    public String getWord() {
        return word;
    }

    public Keyword setWord(String word) {
        this.word = word;
        return this;
    }
    
    
    
}
