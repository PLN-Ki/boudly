/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.boudly.keyword;

import com.tekstly.exceptions.LoadFileException;
import com.tekstly.exceptions.NotFoundLanguageException;
import com.tekstly.linguist.Analyzer;
import com.tekstly.linguist.KeywordsExtractor;
import com.tekstly.linguist.MultiwordsExtractor;
import com.tekstly.logic.keyword.Multiword;
import com.tekstly.logic.summarizer.Sentence;
import com.tekstly.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sergiovilas
 */
public class KWExtractor {
    
    public List<Keyword> extract(String text, String lang){
    
        List<Keyword> list = new ArrayList<>();
        
        try {
            
            
            List<Sentence> sentences = StringUtils.tokenizerBySentences(lang, text, StringUtils.USE_PUNCTUATION_MARKS);
            sentences = Analyzer.getInstance(lang).obtainWordsBySentence(sentences);
            //List<Entity> searchEntities = EntitiesExtractor.getInstance(lang).searchEntities(sentences);
            List<com.tekstly.logic.keyword.Keyword> keywords = KeywordsExtractor.getInstance(lang).getKeywords(10, sentences);
            List<Multiword> multiwords = MultiwordsExtractor.getInstance(lang).getMultiwords(10, sentences);
            
            for(com.tekstly.logic.keyword.Keyword kw: keywords){
            
                list.add(new Keyword().setWord(kw.getWord().getFirstLema().getLema()));
            }
            
            for(Multiword mw: multiwords){
            
                list.add(new Keyword().setWord(mw.wordsToLemaString()));
            }
            
        } catch (LoadFileException ex) {
            Logger.getLogger(KWExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotFoundLanguageException ex) {
            Logger.getLogger(KWExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return list;
    }
    
}
