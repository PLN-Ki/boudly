/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.actions;

import com.boudly.exceptions.AraApiException;
import com.boudly.api.logic.NewResponse;
import com.boudly.orm.config.DB;
import com.boudly.orm.logic.New;
import static com.boudly.orm.logic.New.convertModel;
import com.boudly.orm.logic.UserHosts;
import com.boudly.orm.logic.UserNews;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.common.Util;

public class NewsAction {

    private static final Integer DEFAULT_INITIAL_POSITION = 0;
    private static final Integer DEFAULT_STEP = 20;

    /**
     * @since 1.0
     * @param  idUser
     * @param idsHost
     * @param to
     * @param from
     * @return list news to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<NewResponse> getNewsByHost(Integer idUser, List<Integer> idsHost, Integer from, Integer to) throws AraApiException {
        if(idUser==null){
            throw new AraApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        if (from == null) {
            from = DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = DEFAULT_STEP;
        }
        DB.initWithJniName();
        List<New> noticias = UserNews.searchByHost(idUser, idsHost, to, from);
        
        DB.close();

        return NewResponse.convert(noticias);
    }
    
    
    /**
     * @param idUser
     * @since 1.0
     * @param idsTag
     * @param to
     * @param from
     * @return list news to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<NewResponse> getNewsByTag(Integer idUser, List<Integer> idsTag, Integer from, Integer to) throws AraApiException {
        if (from == null) {
            from = DEFAULT_INITIAL_POSITION;
        }
        if (to == null || to > DEFAULT_STEP) {
            to = DEFAULT_STEP;
        }
        DB.initWithJniName();
        List<New> noticias = new ArrayList<>();
        if(idUser!=null){
            noticias = New.searchByTag(idsTag, to, from);
        }else{
            noticias = UserNews.searchByTag(idUser, idsTag, to, from);
        }
        DB.close();
        return NewResponse.convert(noticias);
    }
    
    
    
    
    /**
     * @since 1.0
     * @param idNew
     * @return boolean if is removed
     */
    public static boolean removeNew(Integer idNew){
        DB.initWithJniName();
        boolean isCorrect = New.search(idNew).delete();
        DB.close();
        return isCorrect;
    }
    
    
    /**
     * @throws com.boudly.exceptions.AraApiException
     * @since 1.0
     * @param idUser
     * @param news
     * @return boolean if is saved
     */
    public static boolean markRead(Integer idUser, List<NewResponse> news) throws AraApiException{
        if(idUser==null){
            throw new AraApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        boolean saveIt = false;
        DB.initWithJniName();
        for(NewResponse n:news){
            UserNews userNews = new UserNews();
            userNews.setIdNew(n.getId());
            userNews.setIdUser(idUser);
            saveIt = saveIt && userNews.saveNow();
        }
        DB.close();
        
        return saveIt;
        
    }
    
    
    /**
     * @since 1.0
     * @param idUser
     * @param idsRequest
     * @throws com.boudly.exceptions.AraApiException
     */
    public static void checkIfCanObtain(Integer idUser, List<Integer> idsRequest)  throws AraApiException{
        if(idsRequest==null){
            throw new AraApiException("The ids parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }else if(idUser==null){
            throw new AraApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        String query = UserHosts.FIELD_IDUSER+" = ? AND "+UserHosts.FIELD_IDHOST + " IN ('" + Util.join(idsRequest, "','") + "')";
        LazyList<Model> hostsFind = UserHosts.find(query, idUser);
        if(hostsFind.size()!=idsRequest.size()){
            throw new AraApiException("Requested ids invalid for this User", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
    }
    
}
