/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.actions;

import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.TagResponse;
import com.boudly.exceptions.AraApiException;
import com.boudly.orm.config.DB;
import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.Tag;
import java.util.List;


public class TagsAction {
    
    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<TagResponse> getTagList() throws AraApiException {
        DB.initWithJniName();
        List<Tag> tags = Tag.getAll();

        DB.close();

        return TagResponse.convert(tags);
    }
}
