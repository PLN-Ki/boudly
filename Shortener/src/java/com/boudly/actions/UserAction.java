package com.boudly.actions;

import com.boudly.api.logic.TokenResponse;
import com.boudly.crypto.Cipher;
import com.boudly.crypto.SHAHelper;
import com.boudly.exceptions.AraApiException;
import com.boudly.exceptions.NotAuthorizedException;
import com.boudly.orm.config.DB;
import com.boudly.orm.logic.Token;
import com.boudly.orm.logic.User;
import java.util.Date;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class UserAction {
    
    public static void createUser(ServletContext ctx, String accesToken, User user) throws AraApiException{
        if(accesToken==null){
            throw new AraApiException("The accesstoken parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
            String[] decodeAccessToken = Cipher.decodeAccessToken(accesToken, ctx);

            String password = SHAHelper.encodeSHA256(decodeAccessToken[0]);//En DB esta dos veces hasheada y una ya viene del cliente
            String mail = decodeAccessToken[1];
            DB.initWithJniName();
            
            user.setPassword(password);
            user.setMail(mail);
            user.setRole(User.ROLE_VALUE_USER);
            user.saveNow();
            
            DB.close();
    }

    public static TokenResponse login(ServletContext ctx, String accesToken) throws AraApiException {
        if(accesToken==null){
            throw new AraApiException("The accesstoken parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        try {
            DB.initWithJniName();
            String[] decodeAccessToken = Cipher.decodeAccessToken(accesToken, ctx);

            String password = SHAHelper.encodeSHA256(decodeAccessToken[0]);//En DB esta dos veces hasheada y una ya viene del cliente
            String mail = decodeAccessToken[1];

            User user = User.search(mail, password);
            if (user == null) {
                throw new NotAuthorizedException(accesToken);
            }
            String token = generateToken(user.getIdUser());

            if (!saveToken(user.getIdUser(), token)) {
                throw new AraApiException("Imposible generate token");
            }

            String accessToken = Cipher.generateAuthToken(token, ctx);
            DB.close();
            return new TokenResponse(accessToken);
        } catch (Exception ex){
            throw new AraApiException(ex.getMessage());
        }
    }
    /* @param ctx
     * @param authToken
     * @param roles
     * @return idUSer
     *
     * @since 1.0.0
     */
    public static Integer authorized(ServletContext ctx, String authToken, String[] roles) throws AraApiException {
        Integer idUser = null;
        if(authToken==null){
            throw new AraApiException("The auth_token parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        boolean authorized = false;
        try {
            DB.initWithJniName();
            String token = Cipher.decodeAuthToken(authToken, ctx);
            Token searchByToken = Token.searchByToken(token);
            idUser = Integer.parseInt(token.split("::")[0]);

            if (searchByToken != null && searchByToken.getIdUser().equals(idUser)) {
                User user = User.searchById(idUser);
                for(String role:roles){
                    if (user.getRole().equals(role)) {
                        authorized = true;
                        break;
                    }
                }
            }

            DB.close();
        } catch (Exception ex) {
            throw new AraApiException(ex.getMessage());

        }

        if (!authorized) {
            throw new NotAuthorizedException("token invalid");
        }
        return idUser;
    }

    private static boolean saveToken(Integer idUsuario, String token) {
        Token tokenDB = new Token(idUsuario, null, new Date(), token);
        return tokenDB.saveNow();
    }

    private static String generateToken(Integer idUsuario) {
        //Total de elementos 11+19+5+9
        Random rand = new Random();
        Integer randomNumber = rand.nextInt((1000000000 - 1000) + 1) + 1000;
        long timeStamp = new java.util.Date().getTime();
        Boolean booleanRand = rand.nextBoolean();
        return idUsuario + "::" + timeStamp + booleanRand + randomNumber;
    }

}
