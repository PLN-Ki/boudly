/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.actions;

import com.boudly.exceptions.AraApiException;
import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.RuleResponse;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public class HostsAction {

    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<HostResponse> getHostList() throws AraApiException {
        
        return HostResponse.convert("hosts");
    }
    
    /**
     * @since 1.0
     * @param idUSer
     * @return list host to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<HostResponse> getHostList(Integer idUSer) throws AraApiException {
        if(idUSer==null){
            throw new AraApiException("The idUser parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
        }
        DB.initWithJniName();
        List<Host> hostsUser = UserHosts.searchHostDetail(idUSer);
        
        DB.close();

        return HostResponse.convert(hostsUser);
    }
    
    
    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<HostResponse> getAllHostListWithRules() throws AraApiException {
        DB.initWithJniName();
        List<HostResponse> hosts = HostResponse.convert(Host.getAll());
        
        for(HostResponse h:hosts){
            h.setRules(RuleResponse.convert(Rule.searchByHost(h.getId())));
        }

        DB.close();

        return hosts;
    }
    
    
    /**
     * @since 1.0
     * @return list host to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<HostResponse> getVisibleHostListWithRules() throws AraApiException {
        DB.initWithJniName();
        List<HostResponse> hosts = HostResponse.convert(Host.getAllVisible());
        
        for(HostResponse h:hosts){
            h.setRules(RuleResponse.convert(Rule.searchByHost(h.getId())));
        }

        DB.close();

        return hosts;
    }

    /**
     * @since 1.0
     * @param idHost
     * @return boolean if is removed
     */
    public static boolean setInvisibleHost(Integer idHost) {
        DB.initWithJniName();
        Host h = Host.search(idHost);   
        h.setVisible(0);
        boolean isCorrect = h.saveNow();
        DB.close();
        return isCorrect;
    }

    /**
     * @since 1.0
     * @param idHost
     * @return boolean if is removed
     */
    public static boolean setVisibleHost(Integer idHost) {
        DB.initWithJniName();
        Host h = Host.search(idHost);   
        h.setVisible(1);
        boolean isCorrect = h.saveNow();
        DB.close();
        return isCorrect;
    }

    /**
     * @see modify host
     * @since 1.0
     * @param host
     */
    public static boolean modifyHost(Host host) {
        DB.initWithJniName();
        host.setVisible(host.getVisible()==null?Host.search(host.getId()).getVisible():host.getVisible());
        boolean isCorrect = host.saveNow();
        DB.close();
        return isCorrect;
    }
    
    /**
     * @see If exist host modify It, if not exist create It
     * @since 1.0
     * @param host
     */
    public static boolean addHost(Host host) {
        DB.initWithJniName();
        boolean isCorrect = host.saveNow();
        DB.close();
        return isCorrect;
    }

    /**
     * @see If exist rule modify It, if not exist create It
     * @since 1.0
     * @param rules
     */
    public static void addRules(List<Rule> rules) {
        DB.initWithJniName();
        for (Rule rule : rules) {
            rule.saveNow();
        }
        DB.close();
    }

    /**
     * @see If exist rule modify It, if not exist create It
     * @since 1.0
     * @param rule
     * @return save it
     */
    public static boolean addRule(Rule rule) {
        DB.initWithJniName();
        boolean isCorrect = rule.saveNow();
        DB.close();
        return isCorrect;
    }

    /**
     * @since 1.0
     * @param idRule
     * @return boolean if is removed
     */
    public static boolean removeRule(Integer idRule) {
        DB.initWithJniName();
        Rule rule = Rule.search(idRule);
        boolean isCorrect = rule.delete();
        DB.close();
        return isCorrect;
    }

    /**
     * @since 1.0
     * @param idHost
     * @return list rules to response
     * @throws com.boudly.exceptions.AraApiException
     */
    public static List<RuleResponse> getRulesList(Integer idHost) throws AraApiException {
        DB.initWithJniName();
        List<Rule> rules = Rule.searchByHost(idHost);
        DB.close();
        return RuleResponse.convert(rules);
    }
    
    /**
     * @param idUser
     * @param idHost
     * @return wasSave
     * @throws com.boudly.exceptions.AraApiException
     * @since 1.0
     */
    public static boolean addHost(Integer idUser, Integer idHost) throws AraApiException {
        if(idUser==null){
            throw new AraApiException("The id_user parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
            
        }else if(idHost ==null){
            throw new AraApiException("The id_host parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
            
        }
        DB.initWithJniName();
        boolean isCorrect = false;
        try {
            isCorrect = new UserHosts(idUser, idHost, true).saveNow();
        }catch(Exception ex){
            throw new AraApiException(ex.getMessage(), AraApiException.DEFAULT_CODE);
        }finally{
            DB.close();
        }
        return isCorrect;
    }
    
    /**
     * @param idUser
     * @param idHost
     * @throws com.boudly.exceptions.AraApiException
     * @since 1.0
     */
    public static void removeHost(Integer idUser, Integer idHost) throws AraApiException {
        if(idUser==null){
            throw new AraApiException("The id_user parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
            
        }else if(idHost ==null){
            throw new AraApiException("The id_host parameter is necessary", HttpServletResponse.SC_PRECONDITION_FAILED);
            
        }
        DB.initWithJniName();
        try{
            String query = UserHosts.FIELD_IDHOST+" = ? AND "+UserHosts.FIELD_IDUSER+" = ?";
            UserHosts.delete(query, idHost, idUser);
        }catch(Exception ex){
            throw new AraApiException(ex.getMessage(), AraApiException.DEFAULT_CODE);
        }finally{
            DB.close();
        }
    }

}
