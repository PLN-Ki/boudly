/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.actions;

import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.RuleResponse;
import com.boudly.api.logic.TagResponse;
import com.boudly.exceptions.AraApiException;
import com.boudly.exceptions.NotModifyParamsException;
import com.boudly.jobs.logic.Url;
import com.boudly.jobs.noticiascomplejo.ObtainUrlsJob;
import com.boudly.orm.config.DB;
import com.boudly.orm.logic.Host;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletAdminAction {

    /**
     * Send error msg
     *
     * @param request
     * @param response
     * @param msg
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public static void sendError(HttpServletRequest request, HttpServletResponse response, String msg) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("error", msg);
        response.sendRedirect("../error");
    }

    /**
     * Refresh or add necesary data from view
     *
     * @param request
     * @throws com.boudly.exceptions.AraApiException
     */
    public static void refreshDataAtributtes(HttpServletRequest request) throws AraApiException {
        HttpSession session = request.getSession();
        List<HostResponse> hosts = HostsAction.getAllHostListWithRules();
        session.setAttribute("hosts", hosts);
        List<TagResponse> tags = TagsAction.getTagList();
        session.setAttribute("tags", tags);
    }

    /**
     * Check in session if have user loged
     *
     * @param request
     * @return
     */
    public static boolean checkIsLogged(HttpServletRequest request) {
        return true;
    }

    public static void modifyHost(String option, HostResponse host) throws NotModifyParamsException {
        switch (option) {
            case "delete":
                if (!HostsAction.setInvisibleHost(host.getId())) {
                    throw new NotModifyParamsException("Delete host " + host.getId());
                }
                break;
            case "update":
                if (!HostsAction.modifyHost(host.host())) {
                    throw new NotModifyParamsException("Update host " + host.getId());
                }
                break;
            case "enable":
                if (!HostsAction.setVisibleHost(host.getId())) {
                    throw new NotModifyParamsException("Enable host " + host.getId());
                }
                break;
            case "insert":
                if (!HostsAction.addHost(host.host())) {
                    throw new NotModifyParamsException("Insert host " + host.getId());
                }
                break;

        }

    }

    public static void modifyRule(String option, RuleResponse rule) throws NotModifyParamsException {
        switch (option) {
            case "delete":
                if (!HostsAction.removeRule(rule.getId())) {
                    throw new NotModifyParamsException("Delete host " + rule.getId());
                }
                break;
            case "update":
                if (!HostsAction.addRule(rule.getRule())) {
                    throw new NotModifyParamsException("Update host " + rule.getId());
                }
                break;
            case "insert":
                if (!HostsAction.addRule(rule.getRule())) {
                    throw new NotModifyParamsException("Insert host " + rule.getId());
                }
                break;
        }

    }

    public static void testHosts(Integer idHost, HttpServletRequest request) {
        //Este metodo es un poco tochon pero hace todo lo que tendría que hacer una carga de urls en un solo método
        try {
            DB.initWithJniName();
            List<Host> noticias = new ArrayList<>();
            noticias.add(Host.search(idHost));

            ObtainUrlsJob urlsJob = new ObtainUrlsJob();
            urlsJob.execute((List<Object>) (Object) noticias);
            List<Url> result = (List<Url>) urlsJob.getResult();
            DB.close();
            
            
            HttpSession session = request.getSession();
            if(result.isEmpty()){
                session.setAttribute("host", "No hay elementos");
            }else{
                session.setAttribute("host", "Elementos de "+result.get(0).getHost().getTitle());
            }
            
            session.setAttribute("test", result);
        } catch (Exception webs) {

        }
    }
}
