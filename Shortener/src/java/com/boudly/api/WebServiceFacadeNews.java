package com.boudly.api;

import com.boudly.actions.HostsAction;
import com.boudly.actions.NewsAction;
import com.boudly.actions.TagsAction;
import com.boudly.actions.UserAction;
import com.boudly.api.logic.Erro;
import com.boudly.exceptions.AraApiException;
import com.boudly.api.logic.NewResponse;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.JSONP;
import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.TagResponse;
import com.boudly.orm.logic.User;
import javax.ws.rs.HeaderParam;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@Path("")
public class WebServiceFacadeNews {

    @Context ServletContext ctx;
    

    /*
     * GET api/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        return Response.ok("pong").build();

    }
    
    /*
     * GET api/jsonp/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/jsonp/ping")
    @Produces("application/javascript" + ";charset=UTF-8")
    @JSONP(callback = "callback", queryParam = "callback")
    public Response pingJSONP() {
        return Response.ok("pong").build();

    }
    
    
    
    /*
     * GET api/hosts
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/hosts")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getHosts(@QueryParam("ids") List<Integer> ids, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try {
            List<HostResponse> news = HostsAction.getHostList();
            return Response.ok(news).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }

 

    
    /*
     * GET api/tags
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/tags")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getTags() {
        try {
            List<TagResponse> news = TagsAction.getTagList();
            return Response.ok(news).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    /*
     * GET api/news
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/hosts/news")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getNewsByHost(@HeaderParam("user_token") String userToken, @QueryParam("ids") List<Integer> ids, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try {
            Integer idUSer = UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            NewsAction.checkIfCanObtain(idUSer, ids);
            List<NewResponse> news = NewsAction.getNewsByHost(idUSer, ids, from, to);
            NewsAction.markRead(idUSer, news);
            return Response.ok(news).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }


    
    /*
     * GET api/tags/news
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/tags/news")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getNewsbyTag(@HeaderParam("user_token") String userToken, @QueryParam("ids") List<Integer> ids, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try {
            Integer idUSer = userToken==null?null:UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            List<NewResponse> news = NewsAction.getNewsByTag(idUSer, ids, from, to);
            if(idUSer!=null){
                NewsAction.markRead(idUSer, news);
            }
            
            return Response.ok(news).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    



}
