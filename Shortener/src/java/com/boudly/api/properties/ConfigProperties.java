/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.properties;

import com.boudly.exceptions.LoadFileException;
import java.util.Properties;
import javax.servlet.ServletContext;


public class ConfigProperties {
    private static final String PROPERTIES_FILE = "/WEB-INF/config.properties";
    public static final String AES_KEY = "com.boudly.ara.api.properties.aes.key";
    
    private static Properties properties;
    
    public static Properties getInstance(ServletContext ctx) throws LoadFileException{
        if(properties == null){
            properties = PropertiesLoader.getInstance(ctx, PROPERTIES_FILE);
        }
        return properties;
    }
}
