/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.api.properties;

import com.boudly.exceptions.LoadFileException;

import java.io.IOException;

import java.io.InputStream;

import java.util.Properties;
import javax.servlet.ServletContext;

public class PropertiesLoader {


    public static Properties getInstance(ServletContext ctx, String file) throws LoadFileException {

        return new PropertiesLoader().openProperties(ctx, file);

    }

    private Properties openProperties(ServletContext ctx, String file) throws LoadFileException {

        Properties prop = new Properties();

        InputStream input = null;

        try {

            input = ctx.getResourceAsStream(file);

            prop.load(input);

        } catch (IOException io) {

            //Como la biblioteca se puede usar tambien des
            throw new LoadFileException("Imposible read file properties", LoadFileException.IMPOSIBLE_READ_FILE, file);

        } finally {

            if (input != null) {

                try {

                    input.close();

                } catch (IOException e) {

                }

            }

        }

        return prop;

    }

}
