package com.boudly.api;

import com.boudly.actions.SearchAction;
import com.boudly.actions.UserAction;
import com.boudly.api.logic.Erro;
import com.boudly.api.logic.HostResponse;
import com.boudly.api.logic.NewResponse;
import com.boudly.api.logic.SearcherHostResponse;
import com.boudly.api.logic.SearcherNewResponse;
import com.boudly.exceptions.AraApiException;
import com.boudly.orm.logic.User;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.JSONP;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@Path("search")
public class WebServiceFacadeSearcher extends org.glassfish.jersey.servlet.ServletContainer{

    @Context ServletContext ctx;
    

    /*
     * GET api/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        return Response.ok("pong").build();

    }
    
    /*
     * GET api/jsonp/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/jsonp/ping")
    @Produces("application/javascript" + ";charset=UTF-8")
    @JSONP(callback = "callback", queryParam = "callback")
    public Response pingJSONP() {
        return Response.ok("pong").build();

    }
    
    
    /*
     * GET api/{lang}/hosts
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/hosts")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response searchHosts(@HeaderParam("user_token") String userToken, @QueryParam("q") String q, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try{
            UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            SearcherHostResponse hosts = SearchAction.searchHost(q, from, to);
            return Response.ok(hosts).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }

    
    /*
     * GET api/{lang}/news
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/{lang}/news")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response searchNews(@PathParam("lang") String lang, @QueryParam("q") String q, @QueryParam("from") Integer from, @QueryParam("to") Integer to) {
        try{
            SearcherNewResponse news = SearchAction.searchNew(lang, q, from, to);
            return Response.ok(news).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }

    }


}
