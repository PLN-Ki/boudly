package com.boudly.api;

import com.boudly.actions.HostsAction;
import com.boudly.actions.UserAction;
import com.boudly.api.logic.Erro;
import com.boudly.api.logic.TokenResponse;
import com.boudly.exceptions.AraApiException;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.JSONP;
import com.boudly.crypto.SHAHelper;
import com.boudly.orm.logic.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@Path("user")
public class WebServiceFacadeUser extends org.glassfish.jersey.servlet.ServletContainer{

    @Context ServletContext ctx;
    

    /*
     * GET api/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response ping() {
        return Response.ok("pong").build();

    }
    
    /*
     * GET api/jsonp/ping
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/jsonp/ping")
    @Produces("application/javascript" + ";charset=UTF-8")
    @JSONP(callback = "callback", queryParam = "callback")
    public Response pingJSONP() {
        return Response.ok("pong").build();

    }
    

    /*
     * POST api/user
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response createUser(@HeaderParam("access_token") String accesstoken, @FormParam("name") String name) {
        try {
            User user = new User();
            user.setName(name);
            UserAction.createUser(ctx, accesstoken, user);
            return Response.ok().build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    /*
     * GET api/test/login
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */

    @GET
    @Path("/test/login")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response testLogin() {
        try {
//            new RSAHelper().init(ConfigProperties.getInstance(ctx).getProperty(ConfigProperties.RSA_API_PRIVATE_KEY), ConfigProperties.getInstance(ctx).getProperty(ConfigProperties.RSA_API_PUBLIC_KEY));
            
            String password = SHAHelper.encodeSHA256("chorizo");//"79e82f84f57a169c800a9d657a1a43b53f1e17a573584b05de68af5e27096904";
            String login = password + "::" + "fernando.blanco.dosil@gmail.com";
//            byte[] encryptData = RSAHelper.encryptData(login, ConfigProperties.getInstance(ctx).getProperty(ConfigProperties.RSA_API_PUBLIC_KEY));
//            String acces_token = Base64.encodeBase64String(encryptData);
            String acces_token = Base64.encodeBase64String(login.getBytes());
            
            
            UserAction.createUser(ctx, acces_token, new User());
            
//            return Response.ok(acces_token).build();

            TokenResponse loginToken = UserAction.login(ctx, acces_token);
            
            UserAction.authorized(ctx, loginToken.getToken(), new String[]{User.ROLE_VALUE_ADMIN});
//            String tokenDecrypt = AESHelper.decrypt(secretAES, Base64.decodeBase64(loginToken.getToken()));
//            String authToken = Base64.encodeBase64String(RSAHelper.encryptData(tokenDecrypt, ConfigProperties.getInstance(ctx).getProperty(ConfigProperties.RSA_API_PUBLIC_KEY)));
//            SecurityAction.authorized(ctx, authToken, new String[]{User.ROLE_VALUE_USER});
//
            return Response.ok(loginToken).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        } catch (Exception ex) {
            Logger.getLogger(WebServiceFacadeUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }


    /*
     * GET api/user/login
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response login(@HeaderParam("access_token") String accesstoken) {
        try {
            return Response.ok(UserAction.login(ctx, accesstoken)).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }

    
    /*
     * GET api/user/hosts
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/hosts")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getHosts(@HeaderParam("user_token") String userToken) {
        try {
            Integer idUSer = UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            
            return Response.ok(HostsAction.getHostList(idUSer)).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    /*
     * GET api/user/validate
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @GET
    @Path("/validate")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response validateToken(@HeaderParam("user_token") String userToken) {
        try {
            UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
             return Response.ok().build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    /*
     * POST api/user/host
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @POST
    @Path("/host/{id_host}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response addHost(@HeaderParam("user_token") String userToken, @PathParam("id_host") Integer idHost) {
        try {
            Integer idUSer = UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            HostsAction.addHost(idUSer, idHost);
            
            return Response.ok(HostsAction.getHostList(idUSer)).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    /*
     * DELETE api/user/host
     * 
     * WS that check server.
     *
     * @since 1.0.0
     */
    @DELETE
    @Path("/host/{id_host}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response removeHost(@HeaderParam("user_token") String userToken, @PathParam("id_host") Integer idHost) {
        try {
            Integer idUSer = UserAction.authorized(ctx, userToken, new String[]{User.ROLE_VALUE_USER, User.ROLE_VALUE_ADMIN});
            HostsAction.removeHost(idUSer, idHost);
            
            return Response.ok(HostsAction.getHostList(idUSer)).build();
        } catch (AraApiException ex) {
            return Response.status(ex.getCode()).entity(new Erro(ex.getCode(), ex.getMessage())).build();
        }
    }
    
    
    


}
