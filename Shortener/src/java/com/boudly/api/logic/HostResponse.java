package com.boudly.api.logic;

import com.boudly.orm.logic.Host;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

//Descargar library: http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22activejdbc%22
/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@XmlRootElement
public class HostResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String url;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String value;
    @JsonIgnore
    private Integer visible;
    @JsonIgnore
    private Integer defaultTag;
    @JsonIgnore
    private Integer order;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<RuleResponse> rules;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private boolean showed;

    public static List<HostResponse> convert(List<Host> hosts) {
        List<HostResponse> devolver = new ArrayList<>();
        for (Host h : hosts) {
            devolver.add(new HostResponse(h));
        }
        return devolver;
    }

    public HostResponse(Host host) {
        this(host.getUrl(), host.getTitle(), host.getId(), host.getVisible());
        this.defaultTag = host.getIdDefaultTag();
        this.order = host.getOrder();
        this.showed = host.getShowed()!=null?host.getShowed():true;
    }

    public HostResponse(String url, String title) {
        this.url = url;
        this.value = title;
    }
    
    
    public HostResponse(String url, String title, Integer id, Integer visible) {
        this(url, title, id);
        this.visible = visible;
    }

    public HostResponse(String url, String title, Integer id) {
        this(url, title);
        this.id = id;
    }

    public HostResponse() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the title to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the rules
     */
    public List<RuleResponse> getRules() {
        return rules;
    }

    /**
     * @param rules the rules to set
     */
    public void setRules(List<RuleResponse> rules) {
        this.rules = rules;
    }

    /**
     * @return host that implement this host
     */
    public Host host() {
        Host host = new Host();

        host.setId(this.getId());
        host.setTitle(this.getValue());
        host.setUrl(this.getUrl());
        host.setIdDefaultTag(this.getDefaultTag());
        host.setOrder(this.order);
        return host;
    }

    /**
     * @return the visible
     */
    public Integer getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    /**
     * @return the defaultTag
     */
    public Integer getDefaultTag() {
        return defaultTag;
    }

    /**
     * @param defaultTag the defaultTag to set
     */
    public void setDefaultTag(Integer defaultTag) {
        this.defaultTag = defaultTag;
    }

    /**
     * @return the order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * @return the showed
     */
    public boolean getShowed() {
        return showed;
    }

    /**
     * @param showed the showed to set
     */
    public void setShowed(boolean showed) {
        this.showed = showed;
    }

}
