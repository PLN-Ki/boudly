package com.boudly.api.logic;

//Descargar library: http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22activejdbc%22

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@XmlRootElement
public class SearcherHostResponse {
    private String query;
    private List<HostResponse> found;
    
    public SearcherHostResponse(){
        this.found = new ArrayList<>();
    }
    
    public SearcherHostResponse(String query, List<HostResponse> found){
        this.query = query;
        this.found = found;
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return the found
     */
    public List<HostResponse> getFound() {
        return found;
    }

    /**
     * @param found the found to set
     */
    public void setFound(List<HostResponse> found) {
        this.found = found;
    }
}
