package com.boudly.api.logic;

import com.boudly.orm.logic.Rule;
import java.util.ArrayList;
import java.util.List;

/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
public class RuleResponse {
    private String value;
    private Integer id;
    private HostResponse host;
    private String description;
    private Integer type;
    
    public static List<RuleResponse> convert(List<Rule> rules){
        List<RuleResponse> devolver = new ArrayList<>();
        for(Rule r:rules){
            devolver.add(new RuleResponse(r));
        }
        return devolver;
    }

    private RuleResponse(Rule r) {
        this.value = r.getValue();
        this.id = r.getId();
        this.host = new HostResponse(r.getIdHost());
        this.description = r.getDescription();
        this.type = r.getType();
    }
    
    public RuleResponse(Integer id, String value, String description, Integer type){
        this.id=id;
        this.value = value;
        this.description = description;
        this.type = type;
    }
    
    public RuleResponse(Integer id, String value, String description, Integer type, Integer idHost){
        this.id=id;
        this.value = value;
        this.description = description;
        this.type = type;
        this.host = new HostResponse();
        this.host.setId(idHost);
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the host
     */
    public HostResponse getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(HostResponse host) {
        this.host = host;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    public Rule getRule() {
        Rule rule = new Rule();
        rule.setValue(this.getValue());
        rule.setId(this.getId());
        rule.setIdHost(this.getHost().host());
        rule.setDescription(this.getDescription());
        rule.setType(this.getType());
        return rule;
    }
}
