package com.boudly.api.logic;

import com.boudly.orm.logic.Host;
import com.boudly.orm.logic.Photo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

//Descargar library: http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22activejdbc%22
/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
@XmlRootElement
public class PhotoResponse {
    private String url;
    private Integer id;
    @JsonIgnore
    private Integer idNew;    
    @JsonIgnore
    private Integer idHost;


    public static List<PhotoResponse> convert(List<Photo> photos){
        List<PhotoResponse> devolver = new ArrayList<>();
        for(Photo p:photos){
            devolver.add(new PhotoResponse(p));
        }
        return devolver;
    }
    public PhotoResponse(Photo photo){
        this(photo.getUrl(),photo.getIdNew(), photo.getIdHost(),photo.getId());
    }
    
    public PhotoResponse(String url) {
        this.url = url;
    }
    
    public PhotoResponse(String url, Integer idNew, Integer idHost) {
        this(url);
        this.idNew = idNew;
        this.idHost = idHost;
    }
    
    public PhotoResponse(String url, Integer idNew, Integer idHost, Integer id) {
        this(url, idNew, idHost);
        this.id = id;
    }

    public PhotoResponse() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    
    /**
     * @return the idNew
     */
    public Integer getIdNew() {
        return idNew;
    }

    /**
     * @param idNew the idNew to set
     */
    public void setIdNew(Integer idNew) {
        this.idNew = idNew;
    }

    /**
     * @return the idHost
     */
    public Integer getIdHost() {
        return idHost;
    }

    /**
     * @param idHost the idHost to set
     */
    public void setIdHost(Integer idHost) {
        this.idHost = idHost;
    }



}
