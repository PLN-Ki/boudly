/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.crypto;

import com.boudly.api.properties.ConfigProperties;
import com.boudly.exceptions.AraApiException;
import com.boudly.exceptions.LoadFileException;
import java.security.GeneralSecurityException;
import javax.servlet.ServletContext;
import org.apache.tomcat.util.codec.binary.Base64;

public class Cipher {

    public static String generateAuthToken(String authToken, ServletContext ctx) throws Exception {
        String secret = ConfigProperties.getInstance(ctx).getProperty(ConfigProperties.AES_KEY);
        return Base64.encodeBase64String(AESHelper.encrypt(secret, authToken));
    }

    public static String decodeAuthToken(String authToken, ServletContext ctx) throws Exception {
        String secret = ConfigProperties.getInstance(ctx).getProperty(ConfigProperties.AES_KEY);
        return AESHelper.decrypt(secret, Base64.decodeBase64(authToken));
                
    }


    private static String decodeToken(String token, ServletContext ctx) throws AraApiException {
        //return RSAHelper.decryptData(Base64.decodeBase64(token),getPrivateFilePath(ctx));
        return new String(Base64.decodeBase64(token));

    }

    public static String[] decodeAccessToken(String accessToken, ServletContext ctx) throws AraApiException {
        String[] split = null;
        split = decodeToken(accessToken, ctx).split("::");
        return split;
    }


}
