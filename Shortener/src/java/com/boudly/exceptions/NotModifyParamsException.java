/**
 * Project ARA a Boudly concept
 *
 * @author fblanco.eu
 *
 * Copyright 2015
 */
package com.boudly.exceptions;

public class NotModifyParamsException extends Exception {
    private String param;
    public NotModifyParamsException(String param){
        this.param = param;
    }
    @Override
    public String getMessage() {
        return "Imposible modidy param: "+param;
    }
    

}
